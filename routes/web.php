<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
header('Access-Control-Allow-Origin: *');

Route::get('/backend/login', 'LoginController@index')->name('login');
Route::post('/backend/login','Auth\LoginController@login');
Route::get('/backend/logout', 'LogoutController@index')->name('logout');
Route::group(['middleware' => 'backendauth', 'prefix' => 'backend'], function () {

	Route::get('dashboard','HomeController@index')->name('dashboard');

	Route::any('users/data', 'UserController@getData')->name('users.getData');
	Route::resource('users', 'UserController');


	Route::any('userlist/data', 'UserListController@getData')->name('userlist.getData');
	Route::resource('userlist','UserListController');
	Route::any('luckyitems/data', 'LuckyItemController@getData')->name('luckyitems.getData');
	Route::resource('luckyitems', 'LuckyItemController');
	//Gallery route
	Route::any('galleries/data', 'GalleryController@getData')->name('galleries.getData');
	Route::resource('galleries', 'GalleryController');

	Route::any('footballTeamList/data', 'FootballTeamListController@getData')->name('footballTeamList.getData');
	Route::resource('footballTeamList','FootballTeamListController');

	Route::any('map/data', 'MapController@getData')->name('map.getData');
	Route::resource('map','MapController');

	Route::any('sliders/data', 'SliderController@getData')->name('sliders.getData');
	Route::resource('sliders','SliderController');

	Route::any('matches/data', 'MatchController@getData')->name('matches.getData');
	Route::resource('matches','MatchController');

	Route::any('videos/data', 'VideoController@getData')->name('videos.getData');
	Route::resource('videos','VideoController');
	
	Route::resource('settings','SettingController');
	
	Route::any('teamList/data', 'TeamListController@getData')->name('teamList.getData');
	Route::resource('teamList','TeamListController');

	Route::any('winning/data', 'WinningController@getData')->name('winning.getData');
	Route::resource('winning','WinningController');
});
Route::get('/','HomePageController@promo');
Route::post('/confirm_yes','HomePageController@confirm');
Route::get('/sess_destroy','HomePageController@destory_session');
Route::get('/home','HomePageController@index')->name('homepage');
Route::resource('userReg','UserReqRegController');
Route::get('/customerlogin','UserReqRegController@showLogin')->name('customer_login');
Route::post('/customerlogin','UserReqRegController@login')->name('customer_login');

Route::get('/customerlogout','UserReqRegController@logout')->name('customer_logout');
Route::group(['middleware' => 'forntendauth:customer'], function () {
	Route::get('/teamedit','HomePageController@teamEdit')->name('teamedit');
	Route::get('/playerlistedit/{id}','HomePageController@playerlistedit')->name('playeredit');
	Route::post('/teamupdate/{id}','HomePageController@teamupdate')->name('teamupdate');
Route::post('/playerlistupdate/{id}','HomePageController@playerlistupdate')->name('playerupdate');
Route::resource('/footballTeamReg','FootballTeamRegController');
Route::get('/teamplayerlist','HomePageController@playerlist')->name('playerlist');
});
Route::get('/luckydraw','HomePageController@luckydraw')->name('luckydraw');

Route::get('/gallery','HomePageController@gallery')->name('gallery');
Route::get('/gallery/{id}','HomePageController@gallery_detail')->name('gallery_detial');

//video gallery
Route::get('/videogallery','HomePageController@videoGallery')->name('videogallery');
Route::get('/videogallery/{id}','HomePageController@videoGallery_detail')->name('videoGallery_detail');
Route::get('/getLuckyItem','HomePageController@getLuckyItem')->name('getluckyitem');
Route::get('/checkLogin','PromotionController@checkLogin')->name('checkLogin');
Route::post('/winningStore','PromotionController@winningStore')->name('winningStore');


Route::resource('/map','MapController');

Route::get('/promotions','HomePageController@promotions')->name('promotions');
Route::get('/term_condition','HomePageController@term_condition')->name('term_condition');
Route::get('/eventlist','HomePageController@eventList')->name('eventlist');
Route::get('/eventdetail/{id}','HomePageController@eventDetail')->name('eventdetail');
Route::get('/matchlist/{id}','HomePageController@matchList')->name('matchList');
//facebook loing

Route::get('facebook_login', 'UserReqRegController@redirectToProvider')->name('fb_login');
Route::get('facebook_login/callback', 'UserReqRegController@handleProviderCallback');

