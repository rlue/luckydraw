<?php

use Illuminate\Database\Seeder;
use App\User;
class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         User::create([
            'name'  => 'Super Admin',
            'email' => 'admin@gmail.com',
            'password'  => bcrypt("minminoo123")
            ]);
    }
}
