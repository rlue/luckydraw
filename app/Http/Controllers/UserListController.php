<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Repositories\CustomerRepo;
use App\Repositories\TeamRegRepo;
use Illuminate\Validation\Rule; 
use Session;


class UserListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $customerRepo;
    protected $teamRegRepo;

    public function __construct(CustomerRepo $customerRepo,TeamRegRepo $teamRegRepo) {
        $this->customerRepo = $customerRepo;
        $this->teamRegRepo = $teamRegRepo;
    }
    public function index()
    {
       return view('UserList.index');
    }

    public function getData(Request $request){
        return $this->customerRepo->getUsers($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	
        $team = $this->teamRegRepo->getTeamNames($id);
	      
if(isset($team->user_id)){
            $userid = $team->user_id;
        }else{
            $userid = '';
        }
        $player = $this->teamRegRepo->getPlayerList($userid);
	//dd($player);
        return view('UserList.view', compact('team','player'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->customerRepo->getUser($id);
        return view('UserList.edit', compact('user')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'username'  => 'required|max:45',
            'phone_no'  => 'required|numeric',
            'address'   => 'required',
        ]);

        $user = $this->customerRepo->save($request,$id);
        
        Session::flash('message', 'You have successfully Update User.');
        return redirect()->route('userlist.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleted = $this->customerRepo->deleteUser($id);

            
        if($deleted){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
