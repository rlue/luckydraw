<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\TeamRegRepo;
use Illuminate\Validation\Rule; 
use Session;

class TeamListController extends Controller
{
    protected $teamRegRepo;

    public function __construct(TeamRegRepo $teamRegRepo) {
        $this->teamRegRepo = $teamRegRepo;
    }
    public function index(){
    	return view('TeamList.index');     
    }
    public function getData(Request $request){
        return $this->teamRegRepo->getTeamNameList($request);
    }
    public function show($id){
    	$team = $this->teamRegRepo->getTeamName($id);
    	$playerList = $this->teamRegRepo->getPlayerLists($id);
    	return view('TeamList.show', compact('playerList','team'));
    }

}