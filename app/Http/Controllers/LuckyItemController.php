<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RequestItems;
use App\Repositories\LuckyItemRepo;
use Illuminate\Validation\Rule; 
use Session;

class LuckyItemController extends Controller
{
    protected $luckyItemRepo;

    public function __construct(LuckyItemRepo $luckyItemRepo) {
        $this->luckyItemRepo = $luckyItemRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        
            return view('luckyitem.index');     
       
    }

    public function getData(Request $request)
    {
        return $this->luckyItemRepo->getLuckyItems($request);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('luckyitem.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestItems $request)
    {      
           
        $user = $this->luckyItemRepo->save($request);

        Session::flash('message', 'You have successfully Insert LuckyItems.');
        return redirect()->route('luckyitems.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
        $item = $this->luckyItemRepo->getLuckyItem($id);
        return view('luckyitem.edit', compact('item')); 
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
        $item = $this->luckyItemRepo->save($request,$id);
        
        Session::flash('message', 'You have successfully Update Item.');
        return redirect()->route('luckyitems.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $deleted = $this->luckyItemRepo->deleteLuckyItem($id);

            
        if($deleted){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
    
}
