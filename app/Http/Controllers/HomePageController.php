<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\customerReg;
use App\Repositories\GalleryRepo;
use App\Repositories\TeamRegRepo;
use App\Repositories\LuckyItemRepo;
use App\Repositories\MapRepo;
use App\Repositories\SliderRepo;
use App\Repositories\MatchRepo;
use App\Repositories\VideoRepo;
use App\Repositories\MatchListRepo;
use App\Repositories\PromoRepo;
use Auth;
use Session;
//Myat Thiri Swe
class HomePageController extends Controller
{
	protected $galleryRepo;
	protected $luckyItemRepo;
	protected $teamRegRepo;
	protected $mapRepo;
	protected $videoRepo;
	protected $sliderRepo;
	protected $matchRepo;
	protected $matchListRepo;
	protected $promoRepo;
    public function __construct(GalleryRepo $galleryRepo,LuckyItemRepo $luckyItemRepo, TeamRegRepo $teamRegRepo, MapRepo $mapRepo,SliderRepo $sliderRepo,MatchRepo $matchRepo,MatchListRepo $matchListRepo,VideoRepo $videoRepo,PromoRepo $promoRepo) {
        $this->galleryRepo 		= $galleryRepo;
        $this->luckyItemRepo 	= $luckyItemRepo;
        $this->teamRegRepo 		= $teamRegRepo;
        $this->mapRepo 			= $mapRepo;
        $this->sliderRepo 		= $sliderRepo;
        $this->matchRepo 		= $matchRepo;
        $this->videoRepo 		= $videoRepo;
        $this->matchListRepo 	= $matchListRepo;
        $this->promoRepo 	= $promoRepo;
    }

	public function index(){
		$slide = $this->sliderRepo->getSliderList();
		$match = $this->matchRepo->getMatchList();
		$yangon = $this->matchRepo->getYgnEvent();
		$mandalay= $this->matchRepo->getMdyEvent();
		$grand = $this->matchRepo->getGrandEvent();
    	return view('HomePage.index',compact('slide','yangon','match','mandalay','grand'));
	}
	public function luckyDraw()
	{
		if(Auth::guard('customer')->check()){
			$id = Auth::guard('customer')->user()->id;
		}else{
			$id='';
		}
		$clickCount = $this->promoRepo->checkCount($id);
		$winning = $this->promoRepo->getWinning($id);
		//dd($winning->luckyitem);
		return view('forntend.luckydraw',compact('clickCount','winning'));
	}
	public function getLuckyItem()
	{
		$lucky = $this->luckyItemRepo->getItems();
		$items = [];
		foreach($lucky as $t){
			$items[] = [
				'text' => $t->name,
				'value' => $t->id,
				'message' => $t->message,
				'background' => $t->background_color
			];
		}
		return response()->json($items);
	}
	public function gallery()
	{
		
		$gallery = $this->galleryRepo->getGalleryList();
		return view('forntend.gallery',compact('gallery'));
	}
	public  function videoGallery()
	{
		$video = $this->videoRepo->getVideoList();
		
		return view('forntend.video_gallery',compact('video'));
	}
	public function videoGallery_detail($id)
	{
		$video = $this->videoRepo->getVideo($id);
		return view('forntend.video_detail',compact('video'));
	
	}
	public function gallery_detail($id)
	{
		$gallery = $this->galleryRepo->getGallery($id);
		return view('forntend.gallery_detail',compact('gallery'));
	}	
	public function playerlist(){
		if (Auth::guard('customer')) {
			$user_id = auth()->guard('customer')->user()->id;
		}else{
			return redirect()->route('customer_login');
		}		
		
		$teamName 	= $this->teamRegRepo->getTeamNames($user_id);
		if(isset($teamName->id)){
			$teamid= $teamName->id;
		}else{
			$teamid='';
		}
		$playerlist = $this->teamRegRepo->getPlayerLists($teamid);
		
		if(isset($teamName->id)){
			return view('forntend.playerlist',compact('playerlist','teamName'));
		}else{

			return redirect()->route('footballTeamReg.index');
		}
		
	}

	public function playerlistedit($id){
		$player = $this->teamRegRepo->getPlayData($id);
		return view('forntend.playerlistedit',compact('player'));
	}	

	public function playerlistupdate(Request $request, $id){
		$user = $this->teamRegRepo->updatePlayList($request,$id);
        Session::flash('message', 'You have successfully Update User.');
        return redirect()->route('playerlist');
	}

	public function eventList(){
		$eventlists = $this->mapRepo->geteventList();
		return view('forntend.eventlist',compact('eventlists'));
	}

	public function eventDetail($event_id){
		$events = $this->mapRepo->eventDetail($event_id);
		return view('forntend.eventdetail',compact('events'));
	}

	public function matchList($match)
	{
		$event = $this->mapRepo->geteventData($match);
		$matchlist = $this->matchRepo->getMatchDetailList($match);
		
		return view('forntend.matchlist',compact('matchlist','event'));
	}

	public function promo(){
		$sess = Session::get('CONFIRM');

		if($sess){
			return redirect()->route('homepage');
			
		}else{
			return view('forntend.promo_packs');
		}
		
	}

	public function confirm(Request $request){
		$validator = Validator::make($request->all(), [
            'request_day' => 'required|max:31|digits:2',
		    'request_month' => 'required|max:12|digits:2',
		    'request_year' => 'required|digits:4',
        ]);
		 
		if ($validator->fails()) {
			$message = 'Please enter a valid "DD-MM-YYYY" date of birth';
			return view('forntend.promo_packs',compact('message'));
		}
		$input = $request->all();
		$ege = $input['request_year'].''.$input['request_month'].''.$input['request_day'];
		$dbDate = \Carbon\Carbon::parse($ege);
		$diffYears = \Carbon\Carbon::now()->diffInYears($dbDate);
		$year = date('Y',strtotime(\Carbon\Carbon::now()));
		if($year > $input['request_year']){
			if($diffYears > 18){
				$getvariable = 'YES';
				$request->session()->put('CONFIRM', $getvariable);
				return redirect('/home');
			}else{
				$message = 'You are age under 18';
				return view('forntend.promo_packs',compact('message'));
			}
		}else{
			$message = 'Invaide year';
			return view('forntend.promo_packs',compact('message'));
		}
			
		
	}

	public function destory_session(){
		$sess = Session::forget('CONFIRM');
		return $sess;
	}

	public function teamEdit()
	{
		
		$user_id = auth()->guard('customer')->user()->id;

		$teamName 	= $this->teamRegRepo->getTeamNames($user_id);
		if($teamName){
			return view('forntend.team_edit',compact('teamName'));
		}else{
			return redirect()->route('footballTeamReg.index');
		}
		
	}

	public function teamupdate(Request $request,$id)
	{
		$user = $this->teamRegRepo->updateTeamList($request,$id);
        Session::flash('message', 'You have successfully Update Team.');
        return redirect()->route('playerlist');
	}

	public function promotions()
	{
		return view('forntend.promotion');
	}
	public function term_condition()
	{
		return view('forntend.term_condition');
	}
}
