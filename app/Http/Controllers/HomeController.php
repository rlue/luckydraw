<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\GalleryRepo;
use App\Repositories\TeamRegRepo;
use App\Repositories\LuckyItemRepo;
use App\Repositories\MapRepo;
use App\Repositories\SliderRepo;
use App\Repositories\MatchRepo;
use App\Repositories\VideoRepo;
use App\Repositories\MatchListRepo;
use App\Repositories\PromoRepo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $galleryRepo;
    protected $luckyItemRepo;
    protected $teamRegRepo;
    protected $mapRepo;
    protected $videoRepo;
    protected $sliderRepo;
    protected $matchRepo;
    protected $matchListRepo;
    protected $promoRepo;
    
    public function __construct(GalleryRepo $galleryRepo,LuckyItemRepo $luckyItemRepo, TeamRegRepo $teamRegRepo, MapRepo $mapRepo,SliderRepo $sliderRepo,MatchRepo $matchRepo,MatchListRepo $matchListRepo,VideoRepo $videoRepo,PromoRepo $promoRepo) {
        $this->galleryRepo      = $galleryRepo;
        $this->luckyItemRepo    = $luckyItemRepo;
        $this->teamRegRepo      = $teamRegRepo;
        $this->mapRepo          = $mapRepo;
        $this->sliderRepo       = $sliderRepo;
        $this->matchRepo        = $matchRepo;
        $this->videoRepo        = $videoRepo;
        $this->matchListRepo    = $matchListRepo;
        $this->promoRepo    = $promoRepo;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total = $this->promoRepo->getTotalWinning();
        $itemcount = $this->promoRepo->getTodayWinning();
        $footballcount = $this->luckyItemRepo->getItemLeftFootball();
        $tshirtcount = $this->luckyItemRepo->getItemLeftTshirt();

        return view('home',compact('total','itemcount','footballcount','tshirtcount'));
    }
}
