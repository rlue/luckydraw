<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\FootballTeamRequest;
use App\Repositories\TeamRegRepo;
use App\FootballTeam;
use Illuminate\Validation\Rule; 
use Session;
use Auth;
class FootballTeamRegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $teamRegRepo;

    public function __construct(TeamRegRepo $teamRegRepo) {
        $this->teamRegRepo = $teamRegRepo;
    }

    public function index()
    {
        $id = Auth::guard('customer')->user()->id;
        $check = FootballTeam::where('user_id',$id)->first();
        
        return view('FootballTeamReg.index',compact('check'));
        
        
    }

    /**
     * Show the f orm for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FootballTeamRequest $request)
    {
        
        $team = $this->teamRegRepo->save($request);
        Session::flash('message', 'You have successfully Registered.');
        return redirect()->route('footballTeamReg.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $players = $this->teamRegRepo->getPlayer($id);
        
        return view('footballTeamList.edit', compact('players')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
