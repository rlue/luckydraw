<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Winning;
use App\LuckyItem;
use App\Setting;
use App\Repositories\PromoRepo;
class PromotionController extends Controller
{
    //

    protected $promoRepo;

    public function __construct(PromoRepo $promoRepo) {
        $this->promoRepo = $promoRepo;
    }
    public function checkLogin()
    {
    	if(Auth::guard('customer')->check()){
    		$id = Auth::guard('customer')->user()->id;
    		$checkWin = Winning::where('userid',$id)->first();
    		if($checkWin){
    			$data=[
	    		 'status' => 401
	    		];
    		}else{
                //$winn = Winning::get();
                //$count = $winn->count();
                $item = LuckyItem::all()->random();
                $condi = Setting::first();
                if($condi->setting_value){
                    $count = $item->id;
                }else{
                    $count = 1;
                }
                
                
    			$data=[
	    		'status' => 200,
                'count' => $count
	    		];	
    		}
    		
    		return response()->json($data);
    	}else{
    		$data=[
    		'status' => 400
    		];
    		return response()->json($data);
    	}
    }

    public function winningStore(Request $request)
    {
        $id = Auth::guard('customer')->user()->id;
        $winid = $request->get('winid');
        $promo = $this->promoRepo->save($winid,$id);
        $data=[
                'status' => 200
                ];
        return response()->json($data);
    }
}
