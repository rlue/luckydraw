<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RequestGallery;
use App\Repositories\GalleryRepo;
use Illuminate\Validation\Rule; 
use Session;

class GalleryController extends Controller
{
    protected $galleryRepo;

    public function __construct(GalleryRepo $galleryRepo) {
        $this->galleryRepo = $galleryRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        
            return view('gallery.index');     
       
    }

    public function getData(Request $request)
    {
        return $this->galleryRepo->getGalleries($request);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestGallery $request)
    {      
           
        $gallery = $this->galleryRepo->save($request);

        Session::flash('message', 'You have successfully Insert Gallery.');
        return redirect()->route('galleries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
        $gallery = $this->galleryRepo->getGallery($id);
        return view('gallery.edit', compact('gallery')); 
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
        $gallery = $this->galleryRepo->save($request,$id);
        
        Session::flash('message', 'You have successfully Update Gallery.');
        return redirect()->route('galleries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $deleted = $this->galleryRepo->deleteGallery($id);

            
        if($deleted){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
    
}
