<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RequestSlider;
use App\Repositories\SliderRepo;
use Illuminate\Validation\Rule; 
use Session;

class SliderController extends Controller
{
    protected $sliderRepo;

    public function __construct(SliderRepo $sliderRepo) {
        $this->sliderRepo = $sliderRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        
            return view('slider.index');     
       
    }

    public function getData(Request $request)
    {
        return $this->sliderRepo->getSliders($request);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestSlider $request)
    {      
           
        $slider = $this->sliderRepo->save($request);

        Session::flash('message', 'You have successfully Insert Slider.');
        return redirect()->route('sliders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
        $slider = $this->sliderRepo->getSlider($id);
        return view('slider.edit', compact('slider')); 
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
        $slider = $this->sliderRepo->save($request,$id);
        
        Session::flash('message', 'You have successfully Update Slider.');
        return redirect()->route('sliders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $deleted = $this->sliderRepo->deleteSlider($id);

            
        if($deleted){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
    
}
