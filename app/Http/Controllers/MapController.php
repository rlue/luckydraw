<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\MapRepo;
use Session;


class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $mapRepo;

    public function __construct(MapRepo $mapRepo) {
        $this->mapRepo = $mapRepo;
    }

    public function index()
    {
        return view('Map/index');
    }
    public function getData(Request $request){
    	$data =  $this->mapRepo->getevent($request);
    	return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Map/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
        	'title'		=> 'required',
        	'date'		=> 'required',
            'event_time' => 'required',
            'event_place' => 'required',
			'latitude' 	=> 'required',
			'longitude' => 'required',
    	]);
		$map = $this->mapRepo->save($request);
		Session::flash('message', 'You have successfully Created Event.');
        return redirect()->route('map.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = $this->mapRepo->geteventData($id);
        return view('Map.edit', compact('event')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = $this->mapRepo->save($request,$id);
        
        Session::flash('message', 'You have successfully Update Event.');
        return redirect()->route('map.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->mapRepo->deleteEvent($id);

            
        if($deleted){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
