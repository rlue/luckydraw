<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Session;

class LogoutController extends Controller
{
    public function index()
    {   
        //$userName = session()->get('name');
        // activity()->log("{$userName} is logout");

        Auth::logout();
        Session::flush();
        return redirect('/backend/login');
    }
}
