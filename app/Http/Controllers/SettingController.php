<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Validation\Rule; 
use Session;
use App\Setting;

class SettingController extends Controller
{
 

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        
        	$setting = Setting::first();
            return view('setting.index',compact('setting'));     
       
    }

  
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
           
       		$input = $request->all();
       		if($input['settingid']){
       			$setting = Setting::find($input['settingid']);
       			
       		}else{
       			$setting = new Setting();

       		}
       		$setting->fill($input)->save();
        Session::flash('message', 'You have successfully Setting.');
        return redirect()->route('settings.index');
    }

  
    
}
