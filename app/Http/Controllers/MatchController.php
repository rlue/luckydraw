<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\MatchRequest;
use App\Repositories\MatchRepo;
use App\Repositories\MatchListRepo;
use App\Repositories\MapRepo;
use App\Repositories\TeamRegRepo;
use Illuminate\Validation\Rule; 
use Session;

class MatchController extends Controller
{
    protected $matchRepo;
    protected $matchListRepo;
    protected $mapRepo;
    protected $teamRegRepo;

    public function __construct(MatchRepo $matchRepo,MatchListRepo $matchListRepo,MapRepo $mapRepo,TeamRegRepo $teamRegRepo) {
        $this->matchRepo = $matchRepo;
        $this->mapRepo = $mapRepo;
        $this->teamRegRepo = $teamRegRepo;
        $this->matchListRepo = $matchListRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        
            return view('match.index');     
       
    }

    public function getData(Request $request)
    {
        return $this->matchRepo->getMatches($request);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    	$event = $this->mapRepo->getEventLists();
    	$team = $this->teamRegRepo->getTeamLists();
        return view('match.create',compact('event','team'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatchRequest $request)
    {      
           
        $user = $this->matchRepo->save($request);

        Session::flash('message', 'You have successfully Insert Match.');
        return redirect()->route('matches.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $event = $this->mapRepo->getEventLists();
    	$team = $this->teamRegRepo->getTeamLists();
        $match = $this->matchRepo->getMatch($id);

        $matchlist = $this->matchListRepo->getMatchList($id);
       
        return view('match.edit', compact('match','event','team','matchlist')); 
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
        $item = $this->matchRepo->save($request,$id);
        
        Session::flash('message', 'You have successfully Update Match.');
        return redirect()->route('matches.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $deleted = $this->matchRepo->deleteMatch($id);

            
        if($deleted){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
    
}
