<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Auth;
use Session;
use Socialite;
use App\Paren;
use Activity;

use App\Repositories\UserRepo;

class LoginController extends Controller
{
    protected $userRepo;

    public function __construct(UserRepo $userRepo) {
        $this->userRepo = $userRepo;
    }

    public function index()
    {  
        
        if (!Auth::check()) {   
            return view('layouts.login');
        }
        else { 
            return redirect()->intended('users');
        }
    }

    public function loginValidate(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'login'   => 'required',
            'password' => 'required',
        ],[],[
            'login'    => 'Email or Phone'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $ret = array(
                'status' => 'fail',
                'msg'    => implode('<br/>', $messages->all()),
            );
        }
        else {
            //login with email or phone
            $field = 'email';
            if (is_numeric($request->input('login'))) {
                $field = 'phone';
            } elseif (filter_var($request->input('login'), FILTER_VALIDATE_EMAIL)) {
                $field = 'email';
            }

            $request->merge([$field => $request->input('login')]);

            if (Auth::attempt($request->only($field,'password'))) {
               
                if(auth()->user()->hasRole('admin')) { // For System Admin
                    $ret = $this->loginSuccess();  
                }
                else {
                    $centers = $this->userRepo->getUserCenterByUserID(Auth::user()->user_id);
                    if (empty($centers)) {
                        $ret = $this->loginFail();
                    } else {
                        $center_id = (count($centers) == 1) ? $centers[0]['center_id'] : NULL;
                        $ret = $this->loginSuccess($center_id);
                    }
                }
        	}
            else{
                $ret = array(
                    'status' => 'fail',
                    'msg'    => 'Invalid Email/Phone or Password'
                );     
            }
        }
        echo json_encode($ret);
    }

    private function loginSuccess($center_id = NULL) {
       $userName =  Auth::user()->name;
        session([
            'user_id'   => Auth::user()->user_id,
            'name'      => $userName,
            'role_id'   => Auth::user()->roles()->first()->id, 
            'center_id' => $center_id
        ]);
        $time = date('d-m-Y h:m:s');
        activity()->log("{$userName} is login");
        //Activity::log("{$userName} is Login in {$time}");
        if(hasRole('parent')) {
            $parent = Paren::where('user_id', Auth::user()->user_id)->first();
            $student_ids = $parent->students()->pluck('student.student_id')->toArray();
            session(['student_ids' => $student_ids]);
        }
        $ret = array(
            'status' => 'success',
        );     
        return $ret; 
    }

    private function loginFail() {
        Auth::logout();
        Session::flush();
        $ret = array(
            'status' => 'fail',
            'msg'    => "Sorry, currently you can't login..."
        );  
        return $ret;             
    }

    public function forgotPassword(Request $request) 
    {
        $input = $request->all();

        Validator::extend('check_email', function($field,$value,$parameters){
            $user = $this->userRepo->getUserByEmail($parameters[0]); 
            return ($user) ? TRUE : FALSE;
        });

        $validator = Validator::make($input, [
            'email'         => 'required|email|check_email:' . $input['email'],
        ], ['check_email'   => 'Sorry, there is no account related to given email.']);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $ret = array(
                'status' => 'fail',
                'msg'    => implode('<br/>', $messages->all()),
            );
        }
        else {
            $updated = $this->userRepo->resetPassword($input['email']);
            if( $updated ) {
                $ret = array(
                    'status' => 'success',
                    'msg'    => "Password successfully reset. Please check your email.",
                );
            }
        }
        echo json_encode($ret);
    }


}
