<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\WinningRepo;
use Illuminate\Validation\Rule; 
use Session;

class WinningController extends Controller
{
    protected $winningRepo;

    public function __construct(WinningRepo $winningRepo) {
        $this->winningRepo = $winningRepo;
    }
    public function index(){
    	return view('winning.index');     
    }
    public function getData(Request $request){
        return $this->winningRepo->getWinningList($request);
    }
    

}