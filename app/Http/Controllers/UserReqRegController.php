<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Repositories\CustomerRepo;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule; 
use Session;
use Auth;
use Socialite;
use App\customerReg;
use Illuminate\Support\Facades\URL;

//Myat Thiri Swe
class UserReqRegController extends Controller
{
	protected $customerRepo;

	public function __construct(CustomerRepo $customerRepo) {
        $this->customerRepo = $customerRepo;
    }
	public function index(){
		return view('UserReqReg/index');
	}
	public function store(Request $request){
		
		$validatedData = $request->validate([
			'username' 	=> 'required|max:45',
			'email' 	=> 'required|email|unique:customer_regs,email,',
			'password' => 'min:6|required_with:password_confirmation|same:password_confirm',
			'password_confirm' => 'min:6',
			'phone_no'	=> 'required|numeric',
			'address'	=> 'required',
            'chk_accept'=> 'required',
    	]);
		$user = $this->customerRepo->save($request);

        Session::flash('message', 'You have successfully Registered.');
        return redirect()->route('userReg.index');
	}

	public function showLogin()
	{
        
        session(['url.intended' => url()->previous()]);
        
		return view('forntend.customer_login');
	}
	public function login(Request $request)
	{
		$input = $request->all();
		$validator = Validator::make($request->all(), [
            'email'   => 'required',
            'password' => 'required',
        ],[],[
            'email'    => 'Email or Phone Invalide
            '
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            
            return view('forntend.customer_login',compact('messages'));
        }
        else {
			if (Auth::guard('customer')->attempt(['email' => $input['email'], 'password' => $input['password']])) {
					return redirect(session('url.intended'));
			}else{
				Session::flash('message', 'You have Invalide login.');
				return view('forntend.customer_login');
			}
		}
	}

	public function logout()
	{
		Auth::guard('customer')->logout();
  		return redirect('/customerlogin');
	}

	public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        try {
            $fbuser = Socialite::driver('facebook')->stateless()->user();
        } catch (InvalidStateException $e) {
            $fbuser = Socialite::driver('facebook')->stateless()->user();
        }
        

        $email = customerReg::where('email',$fbuser->email)->first();

        if($email){
            $check = customerReg::where('social_id',$fbuser->id)->first();
           
            if (!$check) {
               Session::flash('error', 'Sorry you social account incorrect'); 

                
                return view('frontend.customer_login');
            }else{
               Auth::guard('customer')->login($check);
                
            }
        }else{
            //$meta = SeoMeta::where('page','register')->first();
            return view('UserReqReg.index',['username' => $fbuser->getName(), 'email' => $fbuser->getEmail(),'social_id'=>$fbuser->getId()]);
        }
            
        
       return redirect()->route('homepage'); 
    }
}
