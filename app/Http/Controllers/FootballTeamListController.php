<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\FootballTeamRequest;
use App\Repositories\TeamRegRepo;
use Illuminate\Validation\Rule; 
use Session;


class FootballTeamListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $teamRegRepo;

    public function __construct(TeamRegRepo $teamRegRepo) {
        $this->teamRegRepo = $teamRegRepo;
    }
    public function index()
    {
       return view('footballTeamList.index');
    }

    public function getData(Request $request){

        return $this->teamRegRepo->getTeamList($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $players = $this->teamRegRepo->getPlayer($id);
        return view('footballTeamList.show', compact('players')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = $this->teamRegRepo->getTeam($id);

        return view('footballTeamList.edit', compact('team')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'team_name'  => 'required',
            'region'     => 'required',
        ]);
        $user = $this->teamRegRepo->TeamSave($request,$id);
        Session::flash('message', 'You have successfully Update User.');
        return redirect()->route('footballTeamList.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleted = $this->teamRegRepo->deleteTeam($id);
        if($deleted){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
