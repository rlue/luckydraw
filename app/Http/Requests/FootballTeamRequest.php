<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FootballTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'teamname'          => 'required',
            'region'            => 'required',
            'captainName'       => 'required',
            'captainemail'      => 'required',
            'captainphone_no'   => 'required',
            'captainnrc_no'     => 'required',
            'player1Name'       => 'required',
            'player1email'      => 'required',
            'player1phone_no'   => 'required',
            'player1nrc_no'     => 'required',
            'player2Name'       => 'required',
            'player2email'      => 'required',
            'player2phone_no'   => 'required',
            'player2nrc_no'     => 'required',
            'player3Name'       => 'required',
            'player3email'      => 'required',
            'player3phone_no'   => 'required',
            'player3nrc_no'     => 'required',
            'player4Name'       => 'required',
            'player4email'      => 'required',
            'player4phone_no'   => 'required',
            'player4nrc_no'     => 'required',
            'player5Name'       => 'required',
            'player5email'      => 'required',
            'player5phone_no'   => 'required',
            'player5nrc_no'     => 'required',
        ];
    }
}
