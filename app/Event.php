<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class Event extends Model implements HasMedia
{
    use SoftDeletes,HasMediaTrait;

    protected $fillable = ['title','description','date','event_time','event_place','latitude','longitude'];

    public function getMediaUrl()
    {
        if ($this->hasMedia('event')) {
            return $this->getFirstMediaUrl('event');
        }
        
    }

}
