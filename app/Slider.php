<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class Slider extends Model implements HasMedia
{
    //
    use SoftDeletes,HasMediaTrait;

    protected $fillable = [
            'title','url','btn_name'
    ];


    public function getMediaUrl()
    {
        if ($this->hasMedia('slider')) {
            return $this->getFirstMediaUrl('slider');
        }
        
    }
}
