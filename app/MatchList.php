<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class MatchList extends Model
{
    //
     use SoftDeletes;

     protected $fillable = [
            'match_id','home_team','away_team','match_time','result'
    ];

    public function team()
    {
        return $this->belongsTo(Event::class,'eventid');
    }
}
