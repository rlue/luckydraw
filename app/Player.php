<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    use SoftDeletes;

    protected $fillable = [
            'name','team_id','email','phone_no','nrc_no'
    ];
    public function footballteam(){
        return $this->belongsTo('App\FootballTeam','team_id');
    }
}
