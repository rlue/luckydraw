<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FootballTeam extends Model
{
    use SoftDeletes;

    protected $fillable = [ 'name','user_id','region' ];

    public function player(){
        return $this->belongsTo('App\Player');
    }
}
