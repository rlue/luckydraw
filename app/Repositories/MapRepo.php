<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;
use App\Event;

class MapRepo
{
    public function getevent($request)
    {
        $event = Event::query();
        $datatables = DataTables::of($event)
            ->addColumn('no', function ($event) {
                return '';
            })
           
            ->addColumn('action', function ($event) {
                $btn = '<a href="'. route('map.edit', $event) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> ';

                // $btn .= '<a href="#" data-id="'.$event->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
           
            return "<div class='action-column'>" . $btn . "</div>";
                        
                    })
            ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', function($query , $keyword) {
                $sql = "event.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }
    public function geteventData($id)
    {
        $event = Event::where('id', '=', $id)->first();
        return $event;
    }
     public function geteventList()
    {
        $date = date("Y-m-d");
        $eventlist = Event::paginate(10);
        return $eventlist;
    }
     public function getEventLists()
    {
        
        $eventlist = Event::get();
        return $eventlist;
    }
    public function eventDetail($event_id){
        $event = Event::where('id',$event_id)->first();
        return $event;
    }

    public function save(Request $request, $id = null)
    {
        $input = $request->all();
        
        $input['title']      = $request->get("title");
        $input['description']       = $request->get("description");
        $input['date']       = $request->get("date");
        $input['event_time']       = $request->get("event_time");
        $input['event_place']       = $request->get("event_place");
        $input['latitude']   = $request->get("latitude");
        $input['longitude']  = $request->get("longitude");
        
        if ($id === NULL) {
          $map = new Event();
        }
        else {
          $map = Event::find($id);
        }
        
        if($request->file('files')){
            DB::table('media')->where('model_id','=',$map->id)->where('collection_name','=','event')->delete();
            $map->addMedia($request->file('files'))->toMediaCollection('event');
           
        }
        $saved = $map->fill($input)->save();

        return ($saved) ? $map : FALSE;
    }

    public function deleteEvent($id)
    {
        $event = Event::find($id);
        $event->delete();
        return ($event) ? $event : false;
    }

}
