<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\customerReg;
class CustomerRepo
{
    public function getUsers($request)
    {
        $users = customerReg::query();
        $datatables = DataTables::of($users)
            ->addColumn('no', function ($user) {
                return '';
            })
           
            ->addColumn('action', function ($user) {
                $btn = '<a href="'. route('userlist.show', $user) .'" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
                $btn .= ' <a href="'. route('userlist.edit', $user) .'" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= ' <a href="#" data-id="'.$user->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
           
            return "<div class='action-column'>" . $btn . "</div>";
                        
                    })
            ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', function($query , $keyword) {
                $sql = "userlist.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }

    

    public function getUser($user_id)
    {
        $user = customerReg::where('id', '=', $user_id)->first();
        return $user;
    }

    public function save(Request $request, $id = null)
    {
        $input = $request->all();  
        $input['username']  = $request->get("username");
        $input['email']     = $request->get("email");
        $input['password']  = bcrypt($request->get("password"));
        $input['phone_no']  = $request->get("phone_no");
        $input['address']   = $request->get("address");
       
        
        if ($id === NULL) {
          $user = new customerReg();
        }
        else {
           if(!empty($request->get("password"))){
            $input = ['username' => $request->get("username"),'email' => $request->get("email"),'password'=>bcrypt($request->get("password")),'phone_no' => $request->get("phone_no"),'address' => $request->get("address")];

            }else{
             $input = ['username' => $request->get("username"),'email' => $request->get("email"),'phone_no' => $request->get("phone_no"),'address' => $request->get("address")];
            } 
            
            
          $user = customerReg::find($id);
        }

        $saved = $user->fill($input)->save();

        return ($saved) ? $user : FALSE;
    }

    public function deleteUser($id)
    {
        $user = customerReg::find($id);
        $user->delete();
        return ($user) ? $user : false;
    }

}
