<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\Player;
use App\FootballTeam;
class TeamRegRepo
{
    public function getPlayers($request)
    {
        $player = Player::query();
        $datatables = DataTables::of($player)
                        ->addColumn('no', function ($player) {
                            return '';
                        })
                        
                         ->addColumn('name', function ($player) {
                            return $player->name;
                        })
                        ->addColumn('action', function ($player) {
                            $btn = '<a href="'. route('footballTeamList.edit', $player) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= '  <a href="#" data-id="'.$player->team_id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
                            
                        })
                        ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', function($query , $keyword) {
                $sql = "players.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }
    public function getTeamList($request)
    {
        $team = FootballTeam::query();
        $datatables = DataTables::of($team)
                        ->addColumn('no', function ($team) {
                            return '';
                        })
                        ->addColumn('action', function ($team) {
                            $btn = '<a href="'. route('footballTeamList.show', $team) .'" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>';
                            $btn .= ' <a href="'. route('footballTeamList.edit', $team) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                            $btn .= ' <a href="#" data-id="'.$team->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                       
                            return "<div class='action-column'>" . $btn . "</div>";
                        
                        })
                        ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', function($query , $keyword) {
                $sql = "football_teams.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }

    public function getPlayer($team_id)
    {
        $players = Player::where('team_id',$team_id)->get();
        return $players;
    }
    public function getPlayData($id)
    {
        $players = Player::where('id',$id)->first();
        return $players;
    }
    public function getPlayerList($user_id)
    {
	//return $user_id;
        $playerlists = FootballTeam::leftJoin('players','players.team_id','=','football_teams.id')->where('football_teams.user_id','=',$user_id)->orderBy('players.updated_at','desc')->paginate(10);
        return $playerlists;
    }
    public function getPlayerLists($user_id)
    {
        //return $user_id;
        $playerlists = FootballTeam::leftJoin('players','players.team_id','=','football_teams.id')->where('football_teams.id','=',$user_id)->orderBy('players.updated_at','desc')->get();
        return $playerlists;
    }

    public function getTeamName($team_id)
    {
        $team = FootballTeam::where('id',$team_id)->first();
        return $team;
    }
    public function getTeamNames($user){
	$team  = FootballTeam::where('user_id',$user)->first();
	return $team;
    }
    public function getTeam($team_id)
    {
        $teamName = FootballTeam::where('id',$team_id)->first();
        return $teamName;
    }

    public function TeamSave(Request $request, $id = null){
        $input = $request->all(); 
        $team = new FootballTeam();
        $input['name']      = $request->get("team_name");
        $input['region']    = $request->get("region");

        $team = FootballTeam::find($id);
        $saved = $team->fill($input)->save();

        return ($saved) ? $team : FALSE;
    }


    public function save(Request $request, $id = null)
    {
        $input = $request->all(); 
        $user_id = auth()->guard('customer')->user()->id;
        if(empty($user_id)){
            $user_id = 0;
        }
        $input['name']      = $request->get("teamname");
        $input['user_id']   = $user_id;
        $input['region']    = $request->get("region");
       
        $updatedata = array(
            array('name'=> $request->get("captainName"), 'email'=>  $request->get("captainemail"), 'phone_no'=>  $request->get("captainphone_no"), 'nrc_no'=>  $request->get("captainnrc_no")),
            array('name'=> $request->get("player1Name"), 'email'=>  $request->get("player1email"), 'phone_no'=>  $request->get("player1phone_no"), 'nrc_no'=>  $request->get("player1nrc_no")),
            array('name'=> $request->get("player2Name"), 'email'=>  $request->get("player2email"), 'phone_no'=>  $request->get("player2phone_no"), 'nrc_no'=>  $request->get("player2nrc_no")),
            array('name'=> $request->get("player3Name"), 'email'=>  $request->get("player3email"), 'phone_no'=>  $request->get("player3phone_no"), 'nrc_no'=>  $request->get("player3nrc_no")),
            array('name'=> $request->get("player4Name"), 'email'=>  $request->get("player4email"), 'phone_no'=>  $request->get("player4phone_no"), 'nrc_no'=>  $request->get("player4nrc_no")),
            array('name'=> $request->get("player5Name"), 'email'=>  $request->get("player5email"), 'phone_no'=>  $request->get("player5phone_no"), 'nrc_no'=>  $request->get("player5nrc_no"))
        );
        if ($id === NULL) {
            $team = new Player();
            $team_id = DB::table('football_teams')->insertGetId(['name' => $request->get("teamname"),'user_id' => $user_id,'region' => $request->get("region")]);
            if(!empty($team_id)){
                $updatedata = array(
                array('team_id'=>$team_id,'name'=> $request->get("captainName"), 'email'=>  $request->get("captainemail"), 'phone_no'=>  $request->get("captainphone_no"), 'nrc_no'=>  $request->get("captainnrc_no")),
                array('team_id'=>$team_id, 'name'=> $request->get("player1Name"), 'email'=>  $request->get("player1email"), 'phone_no'=>  $request->get("player1phone_no"), 'nrc_no'=>  $request->get("player1nrc_no")),
                array('team_id'=>$team_id,'name'=> $request->get("player2Name"), 'email'=>  $request->get("player2email"), 'phone_no'=>  $request->get("player2phone_no"), 'nrc_no'=>  $request->get("player2nrc_no")),
                array('team_id'=>$team_id,'name'=> $request->get("player3Name"), 'email'=>  $request->get("player3email"), 'phone_no'=>  $request->get("player3phone_no"), 'nrc_no'=>  $request->get("player3nrc_no")),
                array('team_id'=>$team_id,'name'=> $request->get("player4Name"), 'email'=>  $request->get("player4email"), 'phone_no'=>  $request->get("player4phone_no"), 'nrc_no'=>  $request->get("player4nrc_no")),
                array('team_id'=>$team_id,'name'=> $request->get("player5Name"), 'email'=>  $request->get("player5email"), 'phone_no'=>  $request->get("player5phone_no"), 'nrc_no'=>  $request->get("player5nrc_no"))
                );
                 $saved = DB::table('players')->insert($updatedata); 
            }

        }
        else {
          $team = Player::find($id);
          $saved = $team->fill($updatedata)->save();
        }

        return ($saved) ? $team : FALSE;
    }
    public function updatePlayList(Request $request,$id){
        $player = Player::find($id);
        $input['name']      = $request->get("name");
        $input['email']     = $request->get('email');
        $input['phone_no']  = $request->get('phone_no');
        $input['nrc_no']    = $request->get("nrc_no");

        $saved = $player->fill($input)->save();
        return ($saved) ? $player : FALSE;
    }
    public function updateTeamList($request,$id)
    {
        $team = FootballTeam::find($id);
        $input['name'] = $request->get("name");

        $saved = $team->fill($input)->save();
        return ($saved) ? $team : FALSE;
    }
    public function getTeamLists()
    {
        $teamlist = FootballTeam::get();
        return $teamlist;
    }
    public function deleteTeam($id)
    {
        $team = FootballTeam::find($id);
        $team->delete();
        return ($team) ? $team : false;
    }

     public function getTeamNameList($request)
    {
        $team = FootballTeam::query();
        $datatables = DataTables::of($team)
                        ->addColumn('no', function ($slider) {
                            return '';
                        })
                        
                        ->addColumn('name', function ($team) {
                            return $team->name;
                        })
                        ->addColumn('region', function ($team) {
                            return $team->region;
                        })
                        ->addColumn('action', function ($team) {
                            $btn = '<a href="'. route('teamList.show', $team) .'" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';

                // $btn .= '  <a href="#" data-id="'.$team->team_id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
                            
                        })
                        ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', function($query , $keyword) {
                $sql = "football_teams.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }

}
