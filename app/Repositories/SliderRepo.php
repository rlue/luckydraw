<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\Slider;
class SliderRepo
{
    public function getSliders($request)
    {
        $slider = Slider::query();
        $datatables = DataTables::of($slider)
                        ->addColumn('no', function ($slider) {
                            return '';
                        })
                       
                        ->addColumn('action', function ($slider) {
                            $btn = '<a href="'. route('sliders.edit', $slider) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= ' <a href="#" data-id="'.$slider->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
                            
                        })
                        ->rawColumns([ 'action']);

        
        return $datatables->make(true);
    }

    

    public function getSlider($item_id)
    {
        $slider = Slider::where('id', '=', $item_id)->first();
        return $slider;
    }

    public function save(Request $request, $id = null)
    {
        $input = $request->all();  
        $input['title'] = $request->get("title");
        $input['url'] = $request->get("url");
        $input['btn_name'] = $request->get("btn_name");
       
        
        if ($id === NULL) {
          $slider = new Slider();
        }
        else {
           
          $slider = Slider::find($id);
        }
            
            if($request->file('files')){
                DB::table('media')->where('model_id','=',$slider->id)->where('collection_name','=','slider')->delete();
               $slider->addMedia($request->file('files'))->toMediaCollection('slider'); 
            }
            
          
        $saved = $slider->fill($input)->save();

        return ($saved) ? $slider : FALSE;
    }

    public function deleteSlider($id)
    {
        $slider = Slider::find($id);
        $slider->delete();
        return ($slider) ? $slider : false;
    }

    public function getSliderList()
    {
        $slider = Slider::orderBy('id','desc')->get();
        return $slider;
    }
}
