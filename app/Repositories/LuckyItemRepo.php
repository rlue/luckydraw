<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\LuckyItem;
class LuckyItemRepo
{
    public function getLuckyItems($request)
    {
        $items = LuckyItem::query();
        $datatables = DataTables::of($items)
                        ->addColumn('no', function ($item) {
                            return '';
                        })
                       
                        ->addColumn('action', function ($item) {
                            $btn = '<a href="'. route('luckyitems.edit', $item) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                // $btn .= ' <a href="#" data-id="'.$item->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
                            
                        })
                        ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', function($query , $keyword) {
                $sql = "luckyt_items.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }

    

    public function getLuckyItem($item_id)
    {
        $item = LuckyItem::where('id', '=', $item_id)->first();
        return $item;
    }

    public function save(Request $request, $id = null)
    {
        $input = $request->all();  
        $input['name'] = $request->get("name");
        $input['message'] = $request->get("message");
        $input['item_count'] = $request->get("item_count");
        $input['status'] = 0;
        $input['background_color'] = $request->get("background_color");
       
        
        if ($id === NULL) {
          $item = new LuckyItem();
        }
        else {
           
          $item = LuckyItem::find($id);
        }

        $saved = $item->fill($input)->save();

        return ($saved) ? $item : FALSE;
    }

    public function deleteLuckyItem($id)
    {
        $item = LuckyItem::find($id);
        $item->delete();
        return ($item) ? $item : false;
    }

    public function getItems()
    {
        $item = LuckyItem::where('status',0)->get();
        return $item;
    }

    public function getItemLeftFootball()
    {
        $item = LuckyItem::selectraw('sum(item_count) as item_count')->whereIn('id',['2','5','9'])->get();
        foreach($item as $key=>$val){
            $count = $val->item_count;
        }
        return $count;
    }
    public function getItemLeftTshirt()
    {
        $item = LuckyItem::selectraw('sum(item_count) as item_count')->whereIn('id',['3','8'])->get();
        foreach($item as $key=>$val){
            $count = $val->item_count;
        }
        return $count;
    }
}
