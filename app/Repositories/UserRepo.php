<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\User;
class UserRepo
{
    public function getUsers($request)
    {
        $users = User::query();
        $datatables = DataTables::of($users)
                        ->addColumn('no', function ($user) {
                            return '';
                        })
                       
                        ->addColumn('action', function ($user) {
                            $btn = '<a href="'. route('users.edit', $user) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= '  <a href="#" data-id="'.$user->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
                            
                        })
                        ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', function($query , $keyword) {
                $sql = "users.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }

    

    public function getUser($user_id)
    {
        $user = User::where('id', '=', $user_id)->first();
        return $user;
    }

    public function save(Request $request, $id = null)
    {
        $input = $request->all();  
        $input['name'] = $request->get("name");
        $input['email'] = $request->get("email");
        $input['password'] = bcrypt($request->get("password"));
       
        
        if ($id === NULL) {
          $user = new User();
        }
        else {
           if(!empty($request->get("password"))){
            $input = ['name' => $request->get("name"),'email' => $request->get("email"),'password'=>bcrypt($request->get("password"))];

            }else{
             $input = ['name' => $request->get("name"),'email' => $request->get("email")];
            } 
            
            
          $user = User::find($id);
        }

        $saved = $user->fill($input)->save();

        return ($saved) ? $user : FALSE;
    }

    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();
        return ($user) ? $user : false;
    }

}
