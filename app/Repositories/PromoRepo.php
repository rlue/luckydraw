<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\Winning;
use App\LuckyItem;
class PromoRepo
{
   
    public function save($winid,$id)
    {
       
        $input['itemid']  = $winid;
        $input['userid']     = $id;
        $user = new Winning();
        $saved = $user->fill($input)->save();
        $item = LuckyItem::where('id','=',$winid)->decrement('item_count',1);
        $upitem = LuckyItem::where('item_count','=',0)->where('id','=',$winid)->update(['status'=>1]);
        return ($saved) ? $user : FALSE;
    }

    public function checkCount($id)
    {
        $check = Winning::where('userid',$id)->first();
        if($check){
            $no = 0;
        }else{
            $no=1;
        }
        return $no;
    }
   
    public function getWinning($id)
    {
        $check = Winning::with('luckyitem')->where('userid',$id)->get();
        return $check;
    }

    public function getTodayWinning()
    {
        $date = Date('Y-m-d');
        $count = Winning::whereIn('itemid',['2','3','5','8','9'])->whereDate('created_at','=',$date)->count();
        return $count;
    }
    public function getTotalWinning()
    {
        
        $count = Winning::whereIn('itemid',['2','3','5','8','9'])->count();
        return $count;
    }
}
