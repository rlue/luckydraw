<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;
use App\Winning;

class WinningRepo
{
     public function getWinningList($request)
    {
        $team = Winning::with(['luckyitem','customer'])->whereIn('itemid',['2','3','5','8','9']);
        $datatables = DataTables::of($team)
                        ->addColumn('no', function ($slider) {
                            return '';
                        })
                        
                        ->editColumn('luckyitem.name', function ($team) {
                            return $team->luckyitem->name;
                        })
                        ->editColumn('customer.username', function ($team) {
                            return $team->customer->username;
                        })
                        ->editColumn('customer.email', function ($team) {
                            return $team->customer->email;
                        })
                        ->editColumn('customer.phone_no', function ($team) {
                            return $team->customer->phone_no;
                        });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', function($query , $keyword) {
                $sql = "football_teams.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }



}
