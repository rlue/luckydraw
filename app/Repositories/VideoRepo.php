<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\Video;
class VideoRepo
{
    public function getVideos($request)
    {
        $videos = Video::query();
        $datatables = DataTables::of($videos)
                        ->addColumn('no', function ($video) {
                            return '';
                        })
                       
                        ->addColumn('action', function ($video) {
                            $btn = '<a href="'. route('videos.edit', $video) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= ' <a href="#" data-id="'.$video->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
                            
                        })
                        ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('title', function($query , $keyword) {
                $sql = "videos.title like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }

    

    public function getVideo($id)
    {
        $video = Video::where('id', '=', $id)->first();
        return $video;
    }

    public function save(Request $request, $id = null)
    {
        $input = $request->all();  
        $input['title'] = $request->get("title");
        $input['description'] = $request->get("description");
       
        
        if ($id === NULL) {
          $video = new Video();
        }
        else {
           
          $video = Video::find($id);
        }
            if($request->file('images')){
                $video->addMedia($request->file('images'))->toMediaCollection('videoimage');
            }
            if($request->file('files')){
                $video->addMedia($request->file('files'))->toMediaCollection('video');
            }
            
          
        $saved = $video->fill($input)->save();

        return ($saved) ? $video : FALSE;
    }

    public function deleteVideo($id)
    {
        $video = Video::find($id);
        $video->delete();
        return ($video) ? $video : false;
    }

    public function getVideoList()
    {
        $video = Video::orderBy('id','desc')->paginate(20);
        return $video;
    }
}
