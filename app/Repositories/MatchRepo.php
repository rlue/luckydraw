<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\Match;
use App\MatchList;
class MatchRepo
{
    public function getMatches($request)
    {
        $match = Match::query();
        $datatables = DataTables::of($match)
                        ->addColumn('no', function ($match) {
                            return '';
                        })
                       ->editColumn('event', function ($match) {
                            if(isset($match->event->title)){
                                return $match->event->title;
                            }else{
                                return "";
                            }
                            
                        })
                        ->addColumn('action', function ($match) {
                            $btn = '<a href="'. route('matches.edit', $match) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= ' <a href="#" data-id="'.$match->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
                            
                        })
                        ->rawColumns([ 'action']);

        // if ($keyword = $request->get('search')['value']) {
        //     $datatables->filterColumn('name', function($query , $keyword) {
        //         $sql = "luckyt_items.name like ? ";
        //         $query->whereRaw($sql, ["%{$keyword}%"]);
        //     });
        // }
        return $datatables->make(true);
    }

    

    public function getMatch($id)
    {
        $match = Match::with('event')->where('id', '=', $id)->first();
        return $match;
    }

    public function save(Request $request, $id = null)
    {
        $input = $request->all();  

        $input['title'] = $request->get("title");
        $input['description'] = $request->get("description");
        $input['eventid'] = $request->get("eventid");
        $input['event_date'] = $request->get("event_date");
        
        
         
        
        if ($id === NULL) {
          $item = new Match();

        }
        else {
           
          $item = Match::find($id);
          MatchList::where('match_id',$id)->delete();
        }

        $saved = $item->fill($input)->save();
        $condition = $input['home_team'];
         foreach ($condition as $key => $condition) {
            $list = new MatchList;
            $list->home_team = $input['home_team'][$key];
            $list->away_team = $input['away_team'][$key];
            $list->match_time = $input['match_time'][$key];
            $list->result = $input['result'][$key];
            $list->match_id = $item->id;
            $list->save();
         }
        return ($saved) ? $item : FALSE;
    }

    public function deleteMatch($id)
    {
        $item = Match::find($id);
        $item->delete();
        return ($item) ? $item : false;
    }

    public function getMatchList()
    {
        $match = Match::with('event','matchlist')->limit(2)->orderBy('id','desc')->get();
        return $match;
    }
    public function getMdyEvent()
    {
        $match = Match::where('eventid','=',5)->with('event','matchlist')->limit(1)->orderBy('id','desc')->get();
        return $match;
    }
     public function getYgnEvent()
    {
        $match = Match::where('eventid','=',6)->with('event','matchlist')->limit(1)->orderBy('id','desc')->get();
        return $match;
    }
     public function getGrandEvent()
    {
        $match = Match::where('eventid','=',7)->with('event','matchlist')->limit(1)->orderBy('id','desc')->get();
        return $match;
    }
    public function getMatchDetailList($id)
    {
        $match = Match::with('event','matchlist')->where('eventid','=',$id)->orderBy('id','desc')->get();
        return $match;
    }

    public function getMatchWithEvent($id)
    {
        $match = Match::with('event','matchlist')->where('eventid','=',$id)->get();
        return $match;
    }
}
