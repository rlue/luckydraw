<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use DB;
use DataTables;
use Session;
use Hash;
use Mail;

use App\Gallery;
class GalleryRepo
{
    public function getGalleries($request)
    {
        $galleries = Gallery::query();
        $datatables = DataTables::of($galleries)
                        ->addColumn('no', function ($gallery) {
                            return '';
                        })
                       
                        ->addColumn('action', function ($gallery) {
                            $btn = '<a href="'. route('galleries.edit', $gallery) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= ' <a href="#" data-id="'.$gallery->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
                            
                        })
                        ->rawColumns([ 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('title', function($query , $keyword) {
                $sql = "galleries.name like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }
        return $datatables->make(true);
    }

    

    public function getGallery($item_id)
    {
        $gallery = Gallery::where('id', '=', $item_id)->first();
        return $gallery;
    }

    public function save(Request $request, $id = null)
    {
        $input = $request->all();  
        $input['title'] = $request->get("title");
        $input['description'] = $request->get("description");
       
        
        if ($id === NULL) {
          $gallery = new Gallery();
        }
        else {
           
          $gallery = Gallery::find($id);
        }
            
             
            if($request->file('files')){
                DB::table('media')->where('model_id','=',$gallery->id)->where('collection_name','=','gallery')->delete();
            $gallery->addMedia($request->file('files'))->toMediaCollection('gallery');
            }
            if($request->file('video_files')){
                DB::table('media')->where('model_id','=',$gallery->id)->where('collection_name','=','gallery_video')->delete();
                $gallery->addMedia($request->file('video_files'))->toMediaCollection('gallery_video');
            }
        $saved = $gallery->fill($input)->save();

        return ($saved) ? $gallery : FALSE;
    }

    public function deleteGallery($id)
    {
        $gallery = Gallery::find($id);
        $gallery->delete();
        return ($gallery) ? $gallery : false;
    }

    public function getGalleryList()
    {
        $gallery = Gallery::orderBy('id','desc')->paginate(20);
        return $gallery;
    }
}
