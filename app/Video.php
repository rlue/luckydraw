<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class Video extends Model implements HasMedia
{
    //
    use SoftDeletes,HasMediaTrait;

     protected $fillable = [
            'title','description'
    ];


    public function getMediaUrl()
    {
        if ($this->hasMedia('video')) {
            return $this->getFirstMediaUrl('video');
        }
        
    }
    public function getMediaImageUrl()
    {
        if ($this->hasMedia('videoimage')) {
            return $this->getFirstMediaUrl('videoimage');
        }
        
    }
}
