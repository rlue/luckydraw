<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winning extends Model
{
    //
     protected $fillable = [
            'itemid','userid'
    ];

    public function luckyitem()
    {
        return $this->belongsTo(LuckyItem::class,'itemid');
    }
    public function customer()
    {
        return $this->belongsTo(customerReg::class,'userid');
    }
}
