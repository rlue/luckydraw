<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\FootballTeam;
use DB;
class Match extends Model
{
    //
    use SoftDeletes;

     protected $fillable = [
            'title','description','eventid','event_date'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class,'eventid');
    }

    public function matchlist()
    {
        return $this->hasMany(MatchList::class);
    }

    public function matchdetail($id)
    {
    		$result = DB::table('match_lists as m')
    		->join('football_teams as h','h.id','=','m.home_team')
    		->join('football_teams as a','a.id','=','m.away_team')
    		->select('m.*','a.name as awayTeam','h.name as homeTeam')
    		->where('m.match_id','=',$id)
    		->where('m.deleted_at','=',null)
			->get();
			return $result;
    }
}
