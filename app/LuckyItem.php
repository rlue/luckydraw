<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LuckyItem extends Model
{
    //
    use SoftDeletes;

     protected $fillable = [
            'name','message','item_count','background_color','status'
    ];
}
