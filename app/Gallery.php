<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class Gallery extends Model implements HasMedia
{
    //
    use SoftDeletes,HasMediaTrait;

     protected $fillable = [
            'title','description'
    ];


    public function getMediaUrl()
    {
        if ($this->hasMedia('gallery')) {
            return $this->getFirstMediaUrl('gallery');
        }
        
    }
    public function getVideoUrl()
    {
        if ($this->hasMedia('gallery_video')) {
            return $this->getFirstMediaUrl('gallery_video');
        }
        
    }
}
