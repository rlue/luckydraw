<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class customerReg  extends Authenticatable
{
    //
    
    protected $guard = 'customer';

    protected $fillable = ['username', 'email', 'password','phone_no','address','social_id'];

    protected $hidden = [
    'password', 'remember_token',
];
}
