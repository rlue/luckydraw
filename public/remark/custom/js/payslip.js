$(function() {
  var tbl = $('#payslips-table').DataTable({
      processing: true,
      serverSide: true,
      iDisplayLength: 25,
      responsive: true,
      language: {
        "sSearchPlaceholder": "Search..",
        "lengthMenu": "_MENU_",
        "search": "_INPUT_",
        "paginate": {
          "previous": '<i class="icon md-chevron-left"></i>',
          "next": '<i class="icon md-chevron-right"></i>'
        }
      }, 
      order: [[ 1, "desc" ]],
      ajax: {
          url  : baseURL + '/index', // '{!! URL::to("payslip/index/dt") !!}',
          type : "POST",
          data : function (d) {
              d.is_dt = 1;
              d.from = $('#search-from').val();
              d.to = $('#search-to').val();
              d.start_date = $('#search-start-date').val();
              d.end_date = $('#search-end-date').val();
              d.employee_id = $('#search-employee-id').val();
              d.company_id = $('#search-company-id').val();
          }
      },
      columns: [
          { data: 'no', orderable: false, bSearchable: false },
          { data: 'date', name: 'date' },
          { data: 'date_hidden', name: 'date_hidden', "visible": false},
          { data: 'employee', name: 'employee' },
          { data: 'company', name: 'company', className: 'name-col', visible: companyVisiable },
          { data: 'from', name: 'from' },
          { data: 'from_hidden', name: 'from_hidden', "visible": false},
          { data: 'to', name: 'to' },
          { data: 'to_hidden', name: 'to_hidden', "visible": false},
          { data: 'net_pay', name: 'net_pay' },
          { data: 'action', name: 'action', 'bSortable': false, 'searchable': false}
      ],
      "fnRowCallback" : function(nRow, aData, iDisplayIndex){
          // For auto numbering at 'No' column
          var start = tbl.page.info().start;
          $('td:eq(0)',nRow).html(start + iDisplayIndex + 1);
      },
  });

  $('#btn-submit').click(function(e){
    e.preventDefault();
    tbl.ajax.reload( null, false );
  });

  $("#date").datepicker({
    format: "dd-mm-yyyy",
    autoclose :true,
    orientation: "bottom auto"
  }); 

  $("#from").datepicker({
    format: "dd-mm-yyyy",
    autoclose :true,
  }); 

  $("#to").datepicker({
    format: "dd-mm-yyyy",
    autoclose :true,
  }); 

  $("#search-from").datepicker({
    format: "dd-mm-yyyy",
    autoclose :true,
  }); 

  $("#search-to").datepicker({
    format: "dd-mm-yyyy",
    autoclose :true,
  }); 

  $("#search-start-date").datepicker({
    format: "dd-mm-yyyy",
    autoclose :true,
  }); 

  $("#search-end-date").datepicker({
    format: "dd-mm-yyyy",
    autoclose :true,
  }); 

  // Quick Add 
  $("#quick-add").click(function(e){
    e.preventDefault();
    var url = baseURL; //'{!! URL::to("payslip") !!}';
    $('#submiturl').val(url);
    $('.quick-action').html('Add');
    $("#quickModal").modal('show');
  });

  $('#tbl-payslip').on('click', '.view-link', function(e) { 
    e.preventDefault();
    var id = getIdfromid($(this).attr('id')); 
    $.ajax({
      type :  "GET",
      url  :  baseURL + '/' + id, // '{!! URL::to("payslip") !!}' + '/' + id,
      data :  {},
      success: function(data){ 
        $('#view-payslip-detail').html(data); 
        $("#viewModal").modal('show');
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) { 
        alert("ERROR!!!");     
        alert(errorThrown);      
      } 
    }); 
  });

  // Quick Edit
  $('#tbl-payslip').on('click', '.edit-link', function(e) { 
    e.preventDefault();
    var id = getIdfromid($(this).attr('id')); 
    $.ajax({
      type :  "GET",
      url  :  baseURL + '/' + id + '/edit', // '{!! URL::to("payslip") !!}' + '/' + id + '/edit',
      data :  {},
      success: function(data){ 
        var result = $.parseJSON(data);
        if(result['status'] == 'success') {
          $.each(result, function( index, value ) {
            if ($("[name='"+index+"']").length) {
              if($("[name='"+index+"']").is("input")) {
                if( $("[name='"+index+"']").attr('type') == 'text' || $("[name='"+index+"']").attr('type') == 'hidden') {
                    $("input[name='"+index+"']").val(value);    
                }
                else if ($("[name='"+index+"']").attr('type') == 'radio') {
                  $("input[name='"+index+"'][value=" + value + "]").prop('checked', true);
                }
              }
              else if ($("[name='"+index+"']").is("textarea")) {
                $("textarea[name='"+index+"']").val(value);   
              }
              else if ($("[name='"+index+"']").is("select")) {
                $("select[name='"+index+"']").val(value);   
              }
            }
          });
          // Allowances, Deductions, Additionals, Otherpays
          if( result['allowances'].length > 0) {
            $.each(result['allowances'], function( index, value ) {  
              $("#add-allowance-area").before( getAllowanceelement(value) );
            });
          }
          if( result['deductions'].length > 0) {
            $.each(result['deductions'], function( index, value ) {  
              $("#add-deduction-area").before( getDeductionelement(value) );
            });
          }
          if( result['additionals'].length > 0) {
            $.each(result['additionals'], function( index, value ) {  
              $("#add-additional-area").before( getAdditionalelement(value) );
            });
          }
          if( result['otherpays'].length > 0) {
            $.each(result['otherpays'], function( index, value ) {  
              $("#add-otherpay-area").before( getOtherpayelement(value) );
            });
          }
          
          var updateurl =  baseURL + '/' + id;
          $('#submiturl').val(updateurl);
          $('.quick-action').html('Edit');
          $("#quickModal").modal('show');  
        }
        else {
           alert(result['msg']);
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) { 
        alert("Error in getting payslip information. Please contact to administrator"); // alert("ERROR!!!"); alert(errorThrown);         
      } 
    });
  });

  // Quick Submit
  $("#btn-q-submit").click(function(e){
      e.preventDefault();
      var valid = validateForm('quick-form');
      if(valid) { 
        var formdata = $("#quick-form").serializeObject();
        var url = $('#submiturl').val();
        var method = (  $('.quick-action').html() == 'Add' ) ? 'POST' : 'PUT';
        quickAjaxsubmit(url, formdata, tbl, method); 
      }   
  });

  $('#btn-save-n-new').click(function(e){
    e.preventDefault();
    var valid = validateForm('quick-form');
    if(valid) { 
      var formdata = $("#quick-form").serializeObject();
      $.ajax({
        type : "POST",
        url  : $('#submiturl').val(), //'{!! URL::to("general/calculateOT") !!}',
        data : formdata,
        success: function(data){ 
          var result = $.parseJSON(data);
          if(result['status'] == 'success') {
            tbl.ajax.reload( null, false );
            resetForm();
            $(".modal-alert-area").empty().append("<div class='alert alert-success success-display'><a href='#' class='close' data-dismiss='alert'>&times;</a>"+result['msg']+"</div>");
          }
          else {
            $('.err-display').html(result['msg']);
            $('.err-display').show();
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          alert('Error in OT & CPF Calculation. Please contact to administrator.');     
        } 
      });
    }   
  });

  //Quick Delete
  $('#tbl-payslip').on('click', '.del-link', function(e) {
    e.preventDefault();
    var url =  $(this).attr("href");
    var del_id = getIdfromid($(this).attr('id')); //url.substring(url.lastIndexOf("/") + 1, url.length);
    $('#hid-delete-id').val(del_id);
    $("#confirmModel").modal('show');
  });

  //Quick Delete Confirm
  $("#btn-confirm-yes").click(function(){
    var del_id= $('#hid-delete-id').val();
    $("#confirmModel").modal('hide');  
    var url =  baseURL + '/' + del_id;  //'{!! URL::to("payslip") !!}' + '/' + del_id;
    deleteAjax(url, tbl);
  });   

  $('#tbl-payslip').on('click', '.print-link', function(e) {
    e.preventDefault();
    var url =  $(this).attr("href");
    var id = getIdfromid($(this).attr('id')); //url.substring(url.lastIndexOf("/") + 1, url.length);
    window.open(baseURL + '/printPayslip/' + id, '_blank');
    // window.location.href =  baseURL + '/printPayslip/' + id ;
  });

  $('#btn-print-all').click(function(e) {
    e.preventDefault();
    var start_date = ($('#search-start-date').val() != '') ? $('#search-start-date').val() : 'NULL';
    var end_date = ($('#search-end-date').val() != '') ? $('#search-end-date').val() : 'NULL'; 
    var from = ($('#search-from').val() != '') ? $('#search-from').val() : 'NULL';
    var to = ($('#search-to').val() != '') ? $('#search-to').val() : 'NULL'; 
    var employee_id = ($('#search-employee-id').val() != '') ? $('#search-employee-id').val() : 'NULL'; 
    var company_id = ($('#search-company-id').val() != '') ? $('#search-company-id').val() : 'NULL'; 
    window.open(baseURL + '/printallpayslip/' + start_date + '/' + end_date + '/' + from + '/' + to + '/' + employee_id + '/' + company_id, '_blank');
  });

  $('#quickModal').on('hidden.bs.modal', function (e) {  
    resetForm(); 
  }); 

  $('#quickModal').on('shown.bs.modal', function (e) {  
    if($('.quick-action').html() == 'Add') {
      $('#btn-save-n-new').show();
    }
    else {
      $('#btn-save-n-new').hide();
    }
  }); 

  $('#employee-id').change(function() {   
    var id = $(this).val();
    changeEmployee();
    if(id != '') {
      getEmployeeinfo(id); 
    }
  });

  $('#btn-add-allowance').click(function(e) {
    var form_ele = getAllowanceelement();
    $("#add-allowance-area").before( form_ele );
  });

  $('#btn-add-additional').click(function(e) {
    var form_ele = getAdditionalelement();
    $("#add-additional-area").before( form_ele );
  });

  $('#btn-add-deduction').click(function(e) {
    var form_ele = getDeductionelement();
    $("#add-deduction-area").before( form_ele );
  });

  $('#btn-add-otherpay').click(function(e) {
    var form_ele = getOtherpayelement();
    $("#add-otherpay-area").before( form_ele );
  });

  $('#allowance-deduction-area').on('click', '.allowance-remove, .deduction-remove', function(e) { 
    e.preventDefault();
    $(this).parent().parent().remove();
    $('#total-allowance').val(calculateAllowance());
    $('#total-deduction').val(calculateDeduction());
    calculateCPF();
  });

  $('#additional-area').on('click', '.additional-remove', function(e) { 
    e.preventDefault();
    $(this).parent().parent().remove();
    $('#total-additional').val(calculateAdditional());
    calculateCPF();
  });

  $('#otherpay-area').on('click', '.otherpay-remove', function(e) { 
    e.preventDefault();
    $(this).parent().parent().remove();
    $('#total-otherpay').val(calculateOtherpay());
    calculateCPF();
  });

  $('#allowance-deduction-area').on('focusout', '.allowance-value', function(e) { 
    var total_allowance = calculateAllowance(); 
    $('#total-allowance').val(total_allowance);
    calculateCPF();
  });

  $('#allowance-deduction-area').on('focusout', '.deduction-value', function(e) { 
    var total_deduction = calculateDeduction(); 
    $('#total-deduction').val(total_deduction);
    calculateCPF();
  });

  $('#additional-area').on('focusout', '.additional-value', function(e) { 
    var total_additional = calculateAdditional(); 
    $('#total-additional').val(total_additional);
    calculateCPF();
  });

  $('#otherpay-area').on('focusout', '.otherpay-value', function(e) { 
    var total_otherpay = calculateOtherpay(); 
    $('#total-otherpay').val(total_otherpay);
    calculateCPF();
  });

  $('#allowance-deduction-area').on('change', '.allowance-cpf, .deduction-bycomp', function(e) { 
    calculateCPF();
  });

  $('#additional-area').on('change', '.additional-cpf', function(e) { 
    calculateCPF();
  }); 

  $('#basic-pay').focusout(function(e) {
    calculateCPF();  
  });

  $('#employee-cpf').focusout(function(e) {
    var total_deduction = calculateDeduction(); 
    $('#total-deduction').val(total_deduction);  
    calculateNetpayonly();
  });

  $('#employer-cpf').focusout(function(e) {
    var total_otherpay = calculateOtherpay(); 
    $('#total-otherpay').val(total_otherpay);  
    calculateNetpayonly();
  });

  $('#ot-hours, #ot-rate').focusout(function(e) {
    calculateOT();
  });
  
});

function resetForm() {
  $('#quick-form').trigger("reset");
  $('.allowance-remove, .deduction-remove, .otherpay-remove, .additional-remove').parent().parent().remove(); 
}

function changeEmployee() {
  // $('.allowance-remove, .deduction-remove, .otherpay-remove, .additional-remove').parent().parent().remove(); 
  $('#hid-employee-cpf, #hid-employer-cpf, #hid-company-id').val('');  
}

function calculateAllowance() {
  var total_allowance = 0;
  $('input[name^="allowance_value"]').each(function() {
    if( $.isNumeric( $(this).val() ) && $(this).val() > 0) {
      total_allowance += parseFloat($(this).val());
    }  
  });
  return (total_allowance != 0) ? total_allowance : '';
}

function getTotalallowancewithcpfonly() {
  var total_allowance = 0;
  $('input[name^="allowance_value"]').each(function() {
    if( $.isNumeric( $(this).val() ) && $(this).val() > 0) {
      if($(this).parent().next().children().children().prop('checked') == true) {
        total_allowance += parseFloat($(this).val());  
      }
    }  
  });
  return total_allowance;  
}

function calculateDeduction() {
  var total_deduction = 0;
  $('input[name^="deduction_value"]').each(function() {
    if( $.isNumeric( $(this).val() ) && $(this).val() > 0) {
      total_deduction += parseFloat($(this).val());
    }  
  });

  if( $.isNumeric( $('#employee-cpf').val() ) && $('#employee-cpf').val() > 0 ) {
    total_deduction += parseFloat($('#employee-cpf').val());
  }

  return (total_deduction != 0) ? total_deduction : '';
}

function getTotaldeductionbycompany() {
  var total_deduction = 0;
  $('input[name^="deduction_value"]').each(function() {
    if( $.isNumeric( $(this).val() ) && $(this).val() > 0) {
      if($(this).parent().next().children().children().prop('checked') == true) {
        total_deduction += parseFloat($(this).val());
      }
    }  
  });
  return total_deduction;  
}

function calculateAdditional() {
  var total_additional = 0;
  $('input[name^="additional_value"]').each(function() {
    if( $.isNumeric( $(this).val() ) && $(this).val() > 0) {
      total_additional += parseFloat($(this).val());
    }  
  });
  return (total_additional != 0) ? total_additional : '';
}

function getTotaladditionalwithcpfonly() {
  var total_additional = 0;
  $('input[name^="additional_value"]').each(function() {
    if( $.isNumeric( $(this).val() ) && $(this).val() > 0) {
      if($(this).parent().next().children().children().prop('checked') == true) {
        total_additional += parseFloat($(this).val());
      }
    }  
  });
  return total_additional; 
}

function calculateOtherpay() {
  var total_otherpay = 0;
  $('input[name^="otherpay_value"]').each(function() {
    if( $.isNumeric( $(this).val() ) && $(this).val() > 0) {
      total_otherpay += parseFloat($(this).val());
    }  
  });

  if( $.isNumeric( $('#employer-cpf').val() ) && $('#employer-cpf').val() > 0 ) {
    total_otherpay += parseFloat($('#employer-cpf').val());
  }

  return (total_otherpay != 0) ? total_otherpay : '';
}

function getEmployeeinfo(id) {
  $.ajax({
    type : "GET",
    url  : baseURL + '/getEmployeeinfo/' + id, // {!! URL::to("payslip/getEmployeeinfo") !!}' + '/' + id,
    data : {},
    success: function(data){ 
      var result = $.parseJSON(data);
      if(result['status'] == 'success') {
        $('#company').val(result['company']);
        $('#hid-company-id').val(result['company_id']);
        $('#basic-pay').val(result['basic_salary']);
        $('#ot-rate').val(result['ot_rate']);
        $('#hid-employer-cpf').val(result['employer_cpf_rate']);
        $('#hid-employee-cpf').val(result['employee_cpf_rate']);
        calculateOT();
      }
      else {
         alert(result['msg']);
         $('#company, #basic-pay, #ot-rate, #hid-employer-cpf, #hid-employee-cpf').val('');
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { 
      alert('Error in getting employee information. Please contact to administrator.');     
    } 
  });
}

function calculateOT() {
  var ot_hours = $('#ot-hours').val();
  var ot_rate = $('#ot-rate').val();

  if( $.isNumeric(ot_hours) && $.isNumeric(ot_rate) ) {
    $.ajax({
      type : "POST",
      url  : domainURL + '/general/calculateOT', //'{!! URL::to("general/calculateOT") !!}',
      data : {
        ot_hours : ot_hours,
        ot_rate  : ot_rate,
      },
      success: function(data){ 
        var result = $.parseJSON(data);
        if(result['status'] == 'success') {
          $('#ot-pay').val(result['ot_pay']);
          calculateCPF();
        }
        else {
           alert(result['msg']);
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) { 
        alert('Error in OT & CPF Calculation. Please contact to administrator.');     
      } 
    });
  }
  else {
    calculateCPF();
  }
}

function calculateCPF() {
  $.ajax({
    type : "POST",
    url  : domainURL + '/general/calculateCPF', //'{!! URL::to("general/calculateCPF") !!}',
    data : {
      emplyee_id           : $('#employee-id').val(),
      employer_cpf         : $('#hid-employer-cpf').val(),
      employee_cpf         : $('#hid-employee-cpf').val(),
      employer_cpf_amount  : $('#employer-cpf').val(),
      employee_cpf_amount  : $('#employee-cpf').val(),
      basic_pay            : $('#basic-pay').val(),
      ot_pay               : $('#ot-pay').val(),
      total_allowance      : calculateAllowance(), // $('#total-allowance').val(),
      total_allowance_cpf  : getTotalallowancewithcpfonly(),
      total_deduction      : calculateDeduction(),  //$('#total-deduction').val(),
      total_deduction_comp : getTotaldeductionbycompany(),
      total_additional     : calculateAdditional(), //$('#total-additional').val(),
      total_additional_cpf : getTotaladditionalwithcpfonly(),
      total_otherpay       : calculateOtherpay(), //$('#total-otherpay').val(),
    },
    beforeSend : function () {
      $('#btn-q-submit').html('<i class="fa fa-spinner fa-spin"></i>Please Wait...').attr('disabled','disabled');
      $('#btn-save-n-new').html('<i class="fa fa-spinner fa-spin"></i>Please Wait...').attr('disabled','disabled'); 
    },
    success: function(data){ 
      var result = $.parseJSON(data);
      if(result['status'] == 'success') {
        $('#employer-cpf').val(result['employer_cpf_amount']);
        $('#employee-cpf').val(result['employee_cpf_amount']);
        $('#net-pay').val(result['net_pay']);
        $('#total-amount').val(result['payslip_amount']);
        $('#total-deduction').val(calculateDeduction());
        $('#total-otherpay').val(calculateOtherpay());
      }
      else {
         alert(result['msg']);
      }
      $('#btn-q-submit').html('<i class="fa fa-save ico-btn"></i> Save').removeAttr('disabled');
      $('#btn-save-n-new').html('<i class="fa fa-save ico-btn"></i> btn-save-n-new').removeAttr('disabled');

    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { 
      alert('Error in CPF Calculation. Please contact to administrator.');         
    } 
  });  
}

function calculateNetpayonly() {
  $.ajax({
    type : "POST",
    url  :  domainURL + '/general/calculateNetonly', //'{!! URL::to("general/calculateNetonly") !!}',
    data : {
      emplyee_id           : $('#employee-id').val(),
      basic_pay            : $('#basic-pay').val(),
      ot_pay               : $('#ot-pay').val(),
      total_allowance      : $('#total-allowance').val(),
      total_deduction      : $('#total-deduction').val(),
      total_deduction_comp : getTotaldeductionbycompany(),
      total_additional     : $('#total-additional').val(),
      total_otherpay       : $('#total-otherpay').val(),
    },
    success: function(data){ 
      // console.log(data);
      var result = $.parseJSON(data);
      if(result['status'] == 'success') {
        $('#net-pay').val(result['net_pay']);
        $('#total-amount').val(result['payslip_amount']);
      }
      else {
         alert(result['msg']);
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { 
      alert('Error in getting employee information. Please contact to administrator.');     
    } 
  });
}

function getAllowanceelement(allowance) {
  var name = ''; var value = ''; var checked = '';
  if(typeof(allowance)!=='undefined') { //if(allowance != false) {
    var name = allowance.name;
    var value = allowance.amount;
    var checked = ( allowance.cpf_payable == 1 ) ? 'checked' : '';
  }
  var form_ele = '<div class="form-group">' +
                      '<div class="col-xs-6 col-sm-4 col-md-4">' +
                        '<input type="text" name="allowance_name[]" class="form-control input-sm allowance-name" placeholder="Enter Additional Name" value="'+name+'"> ' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-4 col-md-4">' +
                        '<input type="text" name="allowance_value[]" class="form-control input-sm allowance-value" placeholder="Enter Additional Amount" value="'+value+'"> ' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-2 col-md-2 checkbox">' +
                        '<label><input type="checkbox" name="allowance_cpf[]" class="allowance-cpf" value="1" '+ checked +'>CPF</label>' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-2 col-md-2">' +
                        '<a href="#" class="allowance-remove"><i class="icon md-minus-square ico-cont" aria-hidden="true"></i></a>' +
                      '</div>' +
                    '</div>';
  return form_ele;
}

function getAdditionalelement(additional) {
  var name = ''; var value = ''; var checked = '';
  if(typeof(additional)!=='undefined') { // if(additional != false) {
    var name = additional.name;
    var value = additional.amount;
    var checked = ( additional.cpf_payable == 1 ) ? 'checked' : '';
  }
  var form_ele = '<div class="form-group">' +
                      '<div class="col-xs-6 col-sm-4 col-md-4">' +
                        '<input type="text" name="additional_name[]" class="form-control input-sm additional-name" placeholder="Enter Additional Name" value="'+name+'"> ' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-4 col-md-4">' +
                        '<input type="text" name="additional_value[]" class="form-control input-sm additional-value" placeholder="Enter Additional Amount" value="'+value+'"> ' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-2 col-md-2 checkbox">' +
                        '<label><input type="checkbox" name="additional_cpf[]" class="additional-cpf" value="1" '+ checked +'>CPF</label>' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-2 col-md-2">' +
                        '<a href="#" class="additional-remove"><i class="icon md-minus-square ico-cont" aria-hidden="true"></i></a>' +
                      '</div>' +
                    '</div>'; 
  return form_ele;
}

function getDeductionelement(deduction) {
  var name = ''; var value = ''; var checked = '';
  if(typeof(deduction)!=='undefined') {//if(deduction != false) {
    var name = deduction.name;
    var value = deduction.amount;
    var checked = ( deduction.by_company == 1 ) ? 'checked' : '';
  }
  var form_ele = '<div class="form-group">' +
                      '<div class="col-xs-6 col-sm-4 col-md-4">' +
                        '<input type="text" name="deduction_name[]" class="form-control input-sm deduction-name" placeholder="Enter Deduction Name" value="'+name+'"> ' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-4 col-md-4">' +
                        '<input type="text" name="deduction_value[]" class="form-control input-sm deduction-value" placeholder="Enter Deduction Amount" value="'+value+'"> ' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-2 col-md-2 checkbox">' +
                        '<label><input type="checkbox" name="deduction_bycomp[]" class="deduction-bycomp" value="1" '+ checked +'>byComp</label>' +
                      '</div>' +
                      '<div class="col-xs-6 col-sm-2 col-md-2">' +
                        '<a href="#" class="deduction-remove"><i class="icon md-minus-square ico-cont" aria-hidden="true"></i></a>' +
                      '</div>' +
                    '</div>';
  return form_ele;

}

function getOtherpayelement(otherpay) {
  var name = ''; var value = ''; var checked = '';
  if(typeof(otherpay)!=='undefined') {//if(otherpay != false) {
    var name = otherpay.name;
    var value = otherpay.amount;
  }
  var form_ele = '<div class="form-group">' +
                    '<div class="col-xs-6 col-sm-4 col-md-4">' +
                      '<input type="text" name="otherpay_name[]" class="form-control input-sm otherpay-name" placeholder="Enter Other Payment Name" value="'+name+'"> ' +
                    '</div>' +
                    '<div class="col-xs-6 col-sm-6 col-md-6">' +
                      '<input type="text" name="otherpay_value[]" class="form-control input-sm otherpay-value" placeholder="Enter Other Payment Amount" value="'+value+'"> ' +
                    '</div>' +
                    '<div class="col-xs-6 col-sm-2 col-md-2">' +
                      '<a href="#" class="otherpay-remove"><i class="icon md-minus-square ico-cont" aria-hidden="true"></i></a>' +
                    '</div>' +
                  '</div>';
  return form_ele;
}