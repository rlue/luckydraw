function calElmPos(a,e,c){
	var d=_centerX,b=_centerY,g=e+c*_globalVars.config.distanceDeg;c=e+20+_globalVars.config.sliceWidth-_globalVars.config.borderSlice+c*_globalVars.config.distanceDeg;return{x1:d+a*Math.cos(Math.PI*g/e),y1:b+a*Math.sin(Math.PI*g/e),x2:d+a*Math.cos(Math.PI*c/e),y2:b+a*Math.sin(Math.PI*c/e)}}
function spin(a){
	_globalVars.isProcessing=!0;
	turnOffFilters();
	_animation.outerLight.on();
	outerSpin.radius("12%");
	innerSpin.radius("8%");
	spinImage.finish().size(140).move(_centerX/1.23,_centerY/1.22);
	spinLabel.move("43%","44%");
	var e=Math.floor(Math.random()*_globalVars.config.totalSlices+1),c=360/_globalVars.config.totalSlices*e;
	switch(parseInt(_globalVars.config.totalSlices)){
		case 10:c-=3;
		break;
		case 8:c-=8}
		_globalVars.elms.pizza.children()[e-1].attr();
		_globalVars.elms.rotateGroup.animate(15E3,"circInOut").rotate(-3600-
c).afterAll(function(){
	setTimeout(function(){
		_globalVars.elms.pizza.children()[e-1].animate(200).fill("#FFF").loop(15,!0).afterAll(function(){
		_globalVars.elms.pizza.children()[e-1].animate(100).fill(_globalVars.elms.pizzaArr[e-1].fill).afterAll(function(){
			_animation.outerLight.off();
	outerSpin.radius("14%");innerSpin.radius("10%");spinImage.size(160).move(_centerX/1.27,_centerY/1.26).animate().size(140).move(_centerX/1.23,_centerY/1.22).loop(!0,!0);spinLabel.move("42%","44%");_globalVars.isProcessing=!1;
	try{parseInt(e)===_globalVars.jsonData.length&&(e=0);
		var c=_globalVars.jsonData[e].value}catch(b){c=b.toString()
		}
		turnOnFilters();
		a(c)
	})
	})
	},500)}
	)}
function redeem(a,e){var c=JSON.parse(
	localStorage.getItem("ghfjghdsjhf"));
c[a].redeem=!0;
localStorage.setItem("ghfjghdsjhf",JSON.stringify(c));
document.querySelectorAll(".reward-list .items .item")[a].children[0].textContent="Used";
document.querySelectorAll(".reward-list .items .item")[a].children[0].classList.add("disabled");
document.querySelectorAll(".reward-list .items .item")[a].children[0].removeAttribute("onclick");
_globalVars.evt.rewardValue=c[a].price;
document.dispatchEvent(_globalVars.evt)
}
function saveReward(a){
	var e={redeem:!1,price:a};
	localStorage.getItem("ghfjghdsjhf")||localStorage.setItem("ghfjghdsjhf",JSON.stringify([]));a=JSON.parse(localStorage.getItem("ghfjghdsjhf"));a.unshift(e);localStorage.setItem("ghfjghdsjhf",JSON.stringify(a));loadRewardBag()
}
function loadRewardBag(){
	if(localStorage.getItem("ghfjghdsjhf")){
		var a=JSON.parse(localStorage.getItem("ghfjghdsjhf"));
		for(var e=document.querySelector(".reward-list .items"),c="",d=0;d<a.length;d++)c=!0===a[d].redeem?c+('<div class="item"><button class="btn-redeem disabled"><span>Used</span></button><div class="value" data-value="'+a[d].price+'">'+a[d].price+"</div></div>"):c+('<div class="item disabled"><button class="btn-redeem" onclick="redeem('+d+')"><span>Redeem</span></button><div class="value" data-value="'+
a[d].price+'">'+a[d].price+"</div></div>");
			e.innerHTML=c;document.querySelector(".burger-menu .counter").innerHTML=a.length
	}}
function turnOffFilters(){
	outerCircle.attr("filter",null);
	outerSpin.attr("filter",null);
	for(var a=0;a<_lightOuterMem.length;a++)_lightOuterMem[a].element.attr("filter",null);
		for(a=0;a<_globalVars.elms.innerLightArr.length;a++)
			_globalVars.elms.innerLightArr[a].element.attr("filter",null);
		for(a=0;a<_globalVars.elms.textArr.length;a++)
			_globalVars.elms.textArr[a].attr("filter",null);
		shelfTop.attr("filter",null);shelfBot.attr("filter",null);
		outerNeedle.attr("filter",null);
		innerNeedle.attr("filter",null)
	}
function turnOnFilters(){
	"undefined"===typeof _globalVars.config.graphicOption&&(_globalVars.config.graphicOption=0);1<=_globalVars.config.graphicOption&&(outerCircle.filter(setFilter(10,!1)),outerSpin.filter(setFilter(20,!1)));
	if(2<=_globalVars.config.graphicOption)
		for(var a=0;a<_lightOuterMem.length;a++)_lightOuterMem[a].element.filter(setFilter(2,!0));
			if(2<=_globalVars.config.graphicOption)for(a=0;a<_globalVars.elms.innerLightArr.length;a++)_globalVars.elms.innerLightArr[a].element.filter(setFilter(1,
!1));
		if(1<=_globalVars.config.graphicOption)
			for(a=0;a<_globalVars.elms.textArr.length;a++)_globalVars.elms.textArr[a].filter(setFilter(10,!1));2<=_globalVars.config.graphicOption&&(shelfTop.filter(setFilter(3,!1)),shelfBot.filter(setFilter(3,!1)),outerNeedle.filter(setFilter(3,!1)),innerNeedle.filter(setFilter(10,!1)))
		}
function setFilter(a,e){
	var c=new SVG.Filter,d=c.offset(0,0).gaussianBlur(a);
	c.blend(c.source,d);c.size("200%","200%").move("-50%","-50%");e&&d.animate({ease:"<"}).attr({stdDeviation:"9"}).loop(!0,!0);
	_globalVars.elms.filters.push(c);
	return c
}
var _animation={
	outerLight:{
		on:function(){
			for(var a,e=0;e<_lightOuterMem.length;e++)a=.1*e+"s",_lightOuterMem[e].element.animate({ease:"<",delay:a}).fill("#CD0802").loop(!0,!0)},off:function(){for(var a=0;a<_lightOuterMem.length;a++)_lightOuterMem[a].element.finish().fill("#FFFFB0")}}},_globalVars,_width=750,_height=750,_centerX=_width/2,_centerY=_height/2,mFilter=new SVG.Filter;mFilter.offset(0,0).gaussianBlur(10);mFilter.blend(mFilter.source,blur);mFilter.size("200%","200%").move("-50%","-50%");
var lgFilter=new SVG.Filter;lgFilter.offset(0,0).gaussianBlur(20);lgFilter.blend(mFilter.source,blur);lgFilter.size("200%","200%").move("-50%","-50%");
function drawGraphic(){
	var a=SVG("drawing");
	a.viewbox({x:0,y:0,width:_width,height:_height}).attr("id","viewBox");document.getElementById("viewBox").style.height=window.innerHeight+"px";
	_globalVars={isProcessing:!1,coords:{clientX:null,clientY:null},
	config:{totalSlices:12,distanceDeg:45,defaultStartDeg:null,borderSlice:5,sliceWidth:30,
		graphicOption:1,
		brandLogo:"img/brand.png",
		backgroundColor:"#337ab7"},
		elms:{container:a.group(),
			pizza:a.group(),
			pizzaArr:[],
			outerLight:a.group(),
			innerLight:a.group(),
innerLightArr:[],
spin:a.group(),
needle:a.group(),
text:a.group(),
textArr:[],
rotateGroup:a.group(),
filters:[]
},
jsonData:[
{value:"01$"},
{value:"02$"},
{value:"03$"},
{value:"04$"},
{value:"05$"},
{value:"06$"},
{value:"07$"},
{value:"08$"},
{value:"09$"},
{value:"10$"},
{value:"11$"},
{value:"12$"}
]};
0<document.querySelectorAll('[data-type="admin"]').length?(localStorage.getItem("defaultConfig")&&(_globalVars.config=JSON.parse(localStorage.getItem("defaultConfig"))),
	localStorage.getItem("sliceData")&&(_globalVars.jsonData=
JSON.parse(localStorage.getItem("sliceData")))):"undefined"!==typeof _dynamicParams&&(_globalVars.config=_dynamicParams.config,_globalVars.jsonData=_dynamicParams.jsonData);
_globalVars.evt=document.createEvent("Event");
_globalVars.evt.initEvent("onRedeemCompleted",!0,!0);
_globalVars.config.defaultStartDeg=_globalVars.config.totalSlices/2*_globalVars.config.distanceDeg;
var e=a.gradient("linear",function(a){a.at(0,"#3E192A");a.at(1,"#2E0928")});outerCircle=a.circle(_width-30).move(15,15);
outerCircle.fill(e);
e=a.circle("100%").attr({fill:"#666"});
e.radius("45%");outerSpin=a.circle("100%").attr({fill:"#23051D"});
var c=a.gradient("radial",function(a){a.at(0,"#420D39");
	a.at(1,"#23051D")});c.from(.5,0).to(.5,0).radius(.4);
outerSpin.radius("14%");outerSpin.fill(c);
innerSpin=a.circle("100%").attr({fill:"#501245"});
c=a.gradient("linear",function(a){a.at(0,"#23051D");
	a.at(1,"#521246")});
c.from(0,.5).to(0,1);
innerSpin.radius("10%");
innerSpin.fill(c);
spinLabel=a.text("SPIN");
spinLabel.font({size:0,fill:"#fff"});
spinLabel.move("42%","44%");
spinImage=a.image(_globalVars.config.brandLogo,160);
spinImage.move(_centerX/1.27,_centerY/1.26);
_lightOuterMem=[];
if(12>=parseInt(_globalVars.config.totalSlices))
	for(var d=0;d<2*_globalVars.config.totalSlices;d++)
		c=calElmPos(_width/2.08,2*_globalVars.config.defaultStartDeg,d),
	c=a.ellipse(30,30).fill("#FFFFB0").move(c.x1-15,c.y1-15).attr("id",d),_lightOuterMem.push({element:c,filter:blur}),_globalVars.elms.outerLight.add(c);
	for(d=0;d<_globalVars.config.totalSlices;d++){var b=
"M"+_centerX+","+_centerY+" ",g=_width/2.2;
c=calElmPos(g,_globalVars.config.defaultStartDeg,d);
c=a.path(b+"L"+c.x1+","+c.y1+" A"+g+","+g+" 0 0,1 "+c.x2+","+c.y2+" z");var f="";f=0===d||4===d||8===d||12===d||16===d||20===d||24===d||28===d||32===d||36===d?a.gradient("radial",function(a){a.at(0,"#D80001");a.at(1,"#BC1505")}):1===d||5===d||9===d||13===d||17===d||21===d||25===d||29===d||33===d?a.gradient("linear",function(a){a.at(0,"#04756F");a.at(1,"#045E5C")}):2===d||6===d||10===d||14===d||18===d||22===
d||10===d||10===d||10===d?a.gradient("linear",function(a){a.at(0,"#FF8B00");a.at(1,"#D37201")}):36!==parseInt(_globalVars.config.totalSlices)||27!==d&&31!==d&&34!==d?a.gradient("radial",function(a){a.at(0,"#400B35");a.at(1,"#320B28")
}):a.gradient("radial",function(a){a.at(0,"#333333");
a.at(1,"#666666")});c.fill(f);c.stroke({color:"#320E34",width:1,linecap:"round",linejoin:"round"});
_globalVars.elms.pizza.add(c);
_globalVars.elms.pizzaArr.push({element:c,fill:f});b=c=c=c="";
if(12>=parseInt(_globalVars.config.totalSlices))
for(b=
0;5>b;b++)c=_width/2.5-40*b,c=calElmPos(c,_globalVars.config.defaultStartDeg,d),c=a.ellipse(16,16).fill("#FFFFB0").move(c.x1-8,c.y1-8),c.attr("id",b),_globalVars.elms.innerLightArr.push({element:c,filter:blur}),
	_globalVars.elms.innerLight.add(c)
}
_globalVars.elms.container.add(_globalVars.elms.pizza);
for(d=0;d<_globalVars.config.totalSlices;d++){
	c=_width/2.8;c=calElmPos(c,_globalVars.config.defaultStartDeg,d);
	36===parseInt(_globalVars.config.totalSlices)?(c.x1-=15,c.y1-=13):(c.x1=c.x1,c.y1-=20,_globalVars.jsonData[d].value.indexOf(" "));
f=a.gradient("linear",function(a){a.at(0,"#F1F1F1");a.at(1,"#F4F4F4")});
if(12>=parseInt(_globalVars.config.totalSlices))if(-1<_globalVars.jsonData[d].value.indexOf(" ")){
	var h=_globalVars.jsonData[d].value.split(" ");
	b=a.text(function(a){for(var b=0;b<h.length;b++)a.tspan(h[b]).newLine(),
		a.font({size:24,fill:f,anchor:"middle"})});
	4<_globalVars.jsonData[d].value.length&&(c.x1=c.x1,c.y1=c.y1)}
	else b=a.text(function(a){
		a.tspan(_globalVars.jsonData[d].value).font({size:30,fill:f,anchor:"middle"})});
		else b=
a.text(function(a){
	a.tspan(_globalVars.jsonData[d].value).font({
		size:20,fill:f})});
b.rotate(0).move(c.x1,c.y1);
if(36===parseInt(_globalVars.config.totalSlices))
	switch(d){
		case 0:b.rotate(-90);
		break;
		case 1:b.rotate(-80);
		break;
		case 2:b.rotate(-70);
		break;
		case 3:b.rotate(-60);
		break;
		case 4:b.rotate(-50);
		break;
		case 5:b.rotate(-40);
		break;
		case 6:b.rotate(-30);
		break;
		case 7:b.rotate(-20);
		break;
		case 8:b.rotate(-10);
		break;
		case 9:b.rotate(0);
		break;
		case 10:b.rotate(10);
		break;
		case 11:b.rotate(20);
		break;
		case 12:b.rotate(30);
break;
case 13:b.rotate(40);
break;
case 14:b.rotate(50);
break;
case 15:b.rotate(60);
break;
case 16:b.rotate(70);
break;
case 17:b.rotate(80);
break;
case 18:b.rotate(90);
break;
case 19:b.rotate(100);
break;
case 20:b.rotate(110);
break;
case 21:b.rotate(120);
break;
case 22:b.rotate(130);
break;
case 23:b.rotate(140);
break;
case 24:b.rotate(150);
break;
case 25:b.rotate(160);
break;
case 26:b.rotate(170);
break;
case 27:b.rotate(180);
break;
case 28:b.rotate(190);
break;
case 29:b.rotate(200);
break;
case 30:b.rotate(210);
break;
case 31:b.rotate(220);
break;
case 32:b.rotate(230);
break;
case 33:b.rotate(240);
break;
case 34:b.rotate(250);
break;
case 35:b.rotate(260);
break;
default:b.rotate(0)
}
if(12===parseInt(_globalVars.config.totalSlices))
	switch(d){
		case 0:b.rotate(-90);
		break;
		case 1:b.rotate(-60);
		break;
		case 2:b.rotate(-30);
		break;
		case 3:b.rotate(0);
		break;
		case 4:b.rotate(30);
		break;
		case 5:b.rotate(60);
		break;
		case 6:b.rotate(90);
		break;
		case 7:b.rotate(120);
		break;
		case 8:b.rotate(150);
		break;
		case 9:b.rotate(180);
		break;
		case 10:b.rotate(210);
break;
case 11:b.rotate(240);
break;default:b.rotate(0)
}
if(10===parseInt(_globalVars.config.totalSlices))
	switch(d){
		case 0:b.rotate(-90);
		break;
		case 1:b.rotate(-55);
		break;
		case 2:b.rotate(-20);
		break;
		case 3:b.rotate(15);
		break;
		case 4:b.rotate(55);
		break;
		case 5:b.rotate(90);
		break;
		case 6:b.rotate(125);
		break;
		case 7:b.rotate(160);
		break;
		case 8:b.rotate(195);
		break;
		case 9:b.rotate(230);
		break;
		default:b.rotate(0)
	}
		if(8===parseInt(_globalVars.config.totalSlices))switch(d){case 0:b.rotate(-90);
			break;case 1:b.rotate(-45);
break;
case 2:b.rotate(0);
break;
case 3:b.rotate(45);
break;
case 4:b.rotate(90);
break;
case 5:b.rotate(135);
break;
case 6:b.rotate(180);
break;
case 7:b.rotate(225);
break;
default:b.rotate(0)}
_globalVars.elms.text.add(b);
_globalVars.elms.textArr.push(b)
}
shelfTop=a.rect(130,70);
shelfTop.cx(_centerX);
shelfTop.cy(0);
shelfTop.radius(10);
f=a.gradient("linear",function(a){
	a.at(0,"#B3B4B6");
	a.at(.1,"#EAEAEA");
	a.at(.5,"#E6E7E9");
	a.at(.9,"#EAEAEA");
	a.at(1,"#B3B4B6")});
shelfTop.fill(f);
shelfTop.stroke({color:f,width:10,
linecap:"round",linejoin:"round"});
shelfBot=a.path("M 20 0 L 180 0 L 200 80  L 260 110 L -60 110 L 0 80 Z");shelfBot.cx(_centerX);
shelfBot.cy(_height+20);
f=a.gradient("linear",function(a){
	a.at(0,"#BDBDBD");
	a.at(.25,"#E2E1DF");
	a.at(.5,"#C0C0C0");
	a.at(.8,"#5D5D5D");
	a.at(1,"#E2E1DF")});
f.from(0,0).to(0,1);
shelfBot.fill(f);
shelfBot.stroke({color:f,width:10,linecap:"round",linejoin:"round"});outerNeedle=a.polygon("0,0 100,0 50,100");
outerNeedle.cx(_centerX);
outerNeedle.cy(30);
outerNeedle.size(100,70);
f=a.gradient("radial",function(a){
	a.at(0,"#F0F0F0");
	a.at(1,"#A0A0A0")});
outerNeedle.fill(f);
outerNeedle.stroke({color:f,width:10,linecap:"round",linejoin:"round"});innerNeedle=a.polygon("0,0 60,0 30,60");
innerNeedle.cx(_centerX);
innerNeedle.cy(10);
innerNeedle.size(60,50);
innerNeedle.fill("#FFFFFF");
_globalVars.elms.spin.add(outerSpin);
_globalVars.elms.spin.add(innerSpin);
_globalVars.elms.spin.add(spinLabel);
_globalVars.elms.spin.add(spinImage);
_globalVars.elms.needle.add(outerNeedle);
_globalVars.elms.needle.add(innerNeedle);
_globalVars.elms.container.add(shelfTop);
_globalVars.elms.container.add(shelfBot);
_globalVars.elms.container.add(outerCircle);
_globalVars.elms.container.add(e);
_globalVars.elms.container.add(_globalVars.elms.outerLight);
_globalVars.elms.rotateGroup.add(_globalVars.elms.pizza);
_globalVars.elms.rotateGroup.add(_globalVars.elms.innerLight);
_globalVars.elms.rotateGroup.add(_globalVars.elms.text);
_globalVars.elms.container.add(_globalVars.elms.rotateGroup);
_globalVars.elms.container.add(_globalVars.elms.spin);
_globalVars.elms.container.add(_globalVars.elms.needle);
turnOnFilters();
_globalVars.elms.spin.children()[3].animate().size(140).move(_centerX/1.23,_centerY/1.22).loop(!0,!0);
_globalVars.elms.spin.style("cursor","pointer");
_globalVars.elms.pizza.rotate(105);
_globalVars.elms.innerLight.rotate(105);36===parseInt(
	_globalVars.config.totalSlices)&&(_globalVars.elms.pizza.rotate(95),_globalVars.elms.innerLight.rotate(95));switch(parseInt(_globalVars.config.totalSlices)){
	case 36:_globalVars.elms.rotateGroup.rotate(0);
_globalVars.elms.text.rotate(90);
break;
case 12:_globalVars.elms.rotateGroup.rotate(0);
_globalVars.elms.text.rotate(90);
break;
case 10:_globalVars.elms.rotateGroup.rotate(3);
_globalVars.elms.text.rotate(87);
break;
case 8:_globalVars.elms.rotateGroup.rotate(7),
_globalVars.elms.text.rotate(83)
}}
function drawLuckWheel(){
	drawGraphic()
}
var callback=function(){
	setTimeout(function(){
		drawLuckWheel();
		loadEvents()
	},500)};
"complete"===document.readyState||"loading"!==document.readyState&&!document.documentElement.doScroll?callback():document.addEventListener("DOMContentLoaded",callback);