function setDefaultParams(){
	var a=JSON.parse(localStorage.getItem("defaultConfig"));
	document.getElementById("graphic").value=a.graphicOption;
	document.getElementById("slice").value=a.totalSlices;
	"#337ab7"!==a.backgroundColor&&($("body").css("background-color",a.backgroundColor),$("body").css("background-image","none"));
	setSliceDetail();
	updateMemTotalSlices()
}
function updateMemTotalSlices(){
	var a={config:_globalVars.config,jsonData:_globalVars.jsonData};
	$("#config").empty().text("var _dynamicParams = "+JSON.stringify(a))
}
function reload(){
	localStorage.setItem("defaultConfig",
		JSON.stringify(_globalVars.config));
	updateMemTotalSlices();
	document.getElementById("viewBox").remove();drawLuckWheel();
	try{
		loadEvents()}
		catch(a){}}function setGraphicQuality(){
			var a=document.getElementById("graphic").value;_globalVars.config.graphicOption=a;reload()
	}
function setTotalSlices(){
	var a=document.getElementById("slice").value;
	_globalVars.config.totalSlices=a;a=JSON.parse(localStorage.getItem("sliceData"));
	var b=Math.abs(a.length-parseInt(_globalVars.config.totalSlices));
	if(a.length>parseInt(_globalVars.config.totalSlices))
		for(var c=0;c<b;c++)a.splice(a[a.length-1],1);
	else if(a.length<parseInt(_globalVars.config.totalSlices)){c=a.length;
		for(var d=0;d<b;d++)a.push({value:c+d+1+"$"})}
					_globalVars.jsonData=a;localStorage.setItem("sliceData",JSON.stringify(a));
setSliceDetail();
reload()}
function setSliceDetail(){
	var a=JSON.parse(
		localStorage.getItem("sliceData")
		);
	document.getElementById("list-input").innerHTML="";
	for(var b=0;b<a.length;b++){
	var c=document.createElement("input");
	c.setAttribute("id",b);
	c.setAttribute("type","text");
	c.setAttribute("maxlength","15");
	c.setAttribute("placeholder","Enter Slice Value");
	c.setAttribute("class","form-control input-detail");
	c.setAttribute("oninput","updateSliceData(this)");
	c.setAttribute("value",a[b].value);
	document.getElementById("list-input").appendChild(c)
}}
function updateSliceData(a){
	console.log(a.value);for(
		var b=0;
		b<_globalVars.jsonData.length;b++)parseInt(a.id)===b&&(_globalVars.jsonData[b].value=a.value);
	localStorage.setItem("sliceData",JSON.stringify(_globalVars.jsonData));reload()
}
	function setBrandLogo(a){
		a=document.querySelector("input[type=file]").files[0];
		var b=new FileReader;
		b.onloadend=function(){
			_globalVars.config.brandLogo=b.result;
			reload()};
			a&&b.readAsDataURL(a)
		}
		function reset(){
				localStorage.clear();
				location.reload()
			}
var assets={
	css:"",
	fonts:"",
	img:"",
	js:""
},_zip;
function save(){
	updateMemTotalSlices();
	var a=$("html");
	a.find("svg").remove();
	a.find('[data-type="admin"]').remove();
	a.find(".burger-menu .counter").empty().text("...");
	a.find(".reward-list .items").empty();
	a.find(".custom-overlay").removeClass("hide");
	a="<!DOCTYPE html>\n<html>\n"+a.html()+"\n</html>";
	_zip=new JSZip;
	_zip.file("wheel.html",a);
	assets.css=_zip.folder("css");
	assets.fonts=_zip.folder("fonts");
	assets.img=_zip.folder("img");
	assets.js=_zip.folder("js");
	createAssets("js/","svg.min.js","binary",
function(){
	createAssets("js/","layout.js","binary",function(){
	createAssets("css/","global.css","binary",function(){
	createAssets("img/","reward_bg.jpg","image",function(){
	createAssets("img/","daily_lucky.png","image",function(){
	createAssets("img/","brand.png","image",function(){
	_zip.generateAsync({type:"blob"}).then(function(a){
	saveAs(a,"LuckyWheel.zip");
	location.reload()
})
})
})
})
})
})
})
}
function createAssets(a,b,c,d){
	fetch(a+b).then(function(a){
		return 200===a.status||0===a.status?Promise.resolve(a.blob()):Promise.reject(Error(a.statusText))}).then(function(e){console.log(e);"binary"===c&&-1<a.indexOf("js")&&assets.js.file(b,e,{binary:!0});"binary"===c&&-1<a.indexOf("css")&&assets.css.file(b,e,{binary:!0});"image"===c&&-1<a.indexOf("img")&&assets.img.file(b,e,{base64:!0});d(!0)},function(a){d(a.toString()
			)}
		)}
	function quickStar(){
		$(".context").removeClass("hide")}
$("#reward-popup-config").on("change",function(a){
	1===parseInt($(this).val())?$("#daily-lucky").removeAttr("data-type"):$("#daily-lucky").attr("data-type","admin")
});
$("#login-popup-config").on("change",function(a){
	1===parseInt($(this).val())?($(".custom-overlay").removeAttr("data-type"),$(".custom-overlay").removeClass("hide"),$(".custom-overlay .box").removeClass("active"),setTimeout(function(){$(".custom-overlay .box").addClass("active");setTimeout(function(){$(".custom-overlay").addClass("hide");$(".custom-overlay .box").removeClass("active")},1500)},500)):$(".custom-overlay").attr("data-type","admin")});
$("#colorpicker").spectrum({flat:!0,showInput:!0,preferredFormat:"hex",change:function(){var a=$("#colorpicker").spectrum("get").toHexString();$("body").css("background-color",a);$("body").css("background-image","none");var b=JSON.parse(localStorage.getItem("defaultConfig"));b.backgroundColor=a;localStorage.setItem("defaultConfig",JSON.stringify(b))}});$(".context").on("click",function(){$(event.target).hasClass("swiper-container")||0!==$(event.target).parents(".swiper-container").length||$(".context").addClass("hide")});
var callback=function(){setTimeout(function(){localStorage.setItem("defaultConfig",JSON.stringify(_globalVars.config));localStorage.setItem("sliceData",JSON.stringify(_globalVars.jsonData));setDefaultParams()},800)};"complete"===document.readyState||"loading"!==document.readyState&&!document.documentElement.doScroll?callback():document.addEventListener("DOMContentLoaded",callback);