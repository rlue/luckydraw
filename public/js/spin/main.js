



// Super Wheel Script
jQuery(document).ready(function($){
	
	
	var base_url = window.location.origin;
	
	var item=[];
	$.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "GET",

          url  : base_url+'/getLuckyItem',
          cache: false,
          async: false, 
          success: function(data){
            item = data;
            
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
          }
        }); 
	
	$('.wheel-standard').superWheel({
		slices: item ,
	text : {
		color: '#f6933f',
	},
	line: {
		width: 10,
		color: "#111632"
	},
	outer: {
		width: 14,
		color: "#111632"
	},
	inner: {
		width: 15,
		color: "#111632"
	},
	marker: {
		background: "#00BCD4",
		animate: 1
	},
	center:{
	    width: 30,
	    background: '#111632',
	    rotate: false,
	    image:{
	      url: base_url+"/images/logo.png",
	      width: 30
	    },

  	},
	selector: "value",
	
	
	
	});
	
	
	
	var tick = new Audio('media/tick.mp3');
	
	$(document).on('click','.wheel-standard-spin-button',function(e){
		
		$.ajax({
          type : "GET",
          url  : base_url+'/checkLogin',
          cache: false,
          async: false,
          success: function(data){
          	
          	if(data.status == 200){
            var id = data.count;
           
          	$('.wheel-standard').superWheel('start', id);
          		//$('.wheel-standard').superWheel('start','value',Math.floor(Math.random() * 3) );
		
				$(this).prop('disabled',true);

          	}else if(data.status == 401){
          		alert('You can try one time');
          	}else{
          		window.location.replace(base_url+"/customerlogin");
          	}
            
            
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in Lucky Items view. Please contact to administrator');
          }
        }); 
		
	});
	
	
	$('.wheel-standard').superWheel('onStart',function(results){
		
		
		$('.wheel-standard-spin-button').text('Spinning...');
		
	});
	$('.wheel-standard').superWheel('onStep',function(results){
		
		if (typeof tick.currentTime !== 'undefined')
			tick.currentTime = 0;
        
		tick.play();
		
	});
	
	
	$('.wheel-standard').superWheel('onComplete',function(results){
		//console.log(results.value);
		$.ajax({
			headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "POST",
          url  : base_url+'/winningStore',
          cache: false,
          async: false,
          
          data:{winid:results.value},
          success: function(data){
          		
          	if(data.status == 200){
          		
	          	$('.wheel-standard-spin-button:disabled').prop('disabled',false).text('Spin');
				
				swal({
					type: 'success',
					title: results.message, 
					html: ' <b>You got : [ '+ results.text+ ' ]</b><br><br> Note: Please claim the prize at nearest Event Place in Yangon or Mandalay. For more details : reach us on https://www.facebook.com/tigerbeerMM'
				});
				
				//window.location.reload();
				
          	}else{
          		alert('Please login');
          	}
            
            
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in Lucky Items view. Please contact to administrator');
          }
        }); 
		
	});
	
	
	
	
	
});
