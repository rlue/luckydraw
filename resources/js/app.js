
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import { Modal, Tabs ,Tab } from 'uiv'
import Multiselect from 'vue-multiselect'
import swal from 'sweetalert'



Vue.component('modal', Modal)
Vue.component('tabs', Tabs)
Vue.component('tab', Tab)
Vue.component('multiselect', Multiselect)
Vue.mixin({
    methods: {
        route: route
    }
});
