import DatePicker from '../../resources/js/components/DatePicker.vue'
import TimePicker from '../../resources/js/components/TimePicker.vue'
import ApproveForm from '../../resources/js/components/ApproveForm.vue'
import ConfirmModal from '../../resources/js/components/ConfirmModal.vue'
import Helpers from '../../resources/js/mixins/helpers'


const fields = {
        id: '',
        tag_name:'',
        
};


new Vue({
  el: '#tags',
  mixins: [Helpers],
  components: {
    'date-picker': DatePicker,
    'time-picker': TimePicker,
  },
  data: {
    tbl: null,
    form: null,
    errors: '',
    submitting: false,
    editLink: '',
    updateLink: '',
    deleteLink: '',
    detailLink: '',
    models:'',
    isDisabled:[],
    state:'',
    deleteModal: false,
    importFile: null,
    isLoading: false,
    tagModal: false,
  },

  methods: {
    buildForm(attributes = {}) {
      this.form = new Form(attributes);
    },
    toTitleCase(str)
    {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    },
    create () { 
      this.state = 'Add'
      this.buildForm(fields);
      this.tagModal = true;
    },
    edit () {
      this.state = 'Edit'
      axios.get(this.editLink).then((response) => {
        //console.log(response.data);
        this.buildForm(response.data);
        this.tagModal = true;
      })
    },

    submit () {
     
      if (this.state === 'Add') {

          this.toServer(route('tags.store'));
      } else {
          this.toServer(this.updateLink,'put');
      }
    },
    
    submitPermission(){
      this.toServer(this.updateLink);
    },
    toServer (url, method = 'post') {
      this.submitting = true;
      this.form[method](url)
          .then(response => {
            this.submitting = false;
            this.tbl.ajax.reload(null, false)
            this.notify(response.msg)
            this.tagModal = false;
          })
          .catch(error => {
            this.submitting=false;
          });
    },

    deleteModalClose () {
      this.deleteModal = false
    },

    delete () {
      this.handleDelete(this.deleteLink);
    },
    
    
  },
  computed: {
    getTitle () {
      return this.state + ' Tag';
    },
    isEdit() {
      return this.form && this.form.id;
    },
    
  },

  watch: {
    leadModal: function(modal) {
      if (!modal) {
        this.form = null;
      }
    },
  },

  mounted () {
    
   let me = this
   this.tbl = $('#tag-table').DataTable({
    processing: true,
    serverSide: true,
    iDisplayLength: 25,
    'responsive': true,
    'language' : {
      "sSearchPlaceholder": "Search..",
      "lengthMenu": "_MENU_",
      "search": "_INPUT_",
      "paginate": {
        "previous"  : '<i class="icon zmdi zmdi-chevron-left"></i>',
        "next"      : '<i class="icon zmdi zmdi-chevron-right"></i>'
      }
    }, 
    order: [[ 1, "asc" ]],
    ajax: {
      url  :  baseUrl + '/tags/data',
      type : "POST",
      data : function (d) {
          d.is_dt = 1;
      }
    },
    columns: [
      { data: 'no', orderable: false, bSearchable: false },
      { data: 'tag_name', name: 'tag_name' },
      { data: 'action', name: 'action', 'bSortable': false, 'searchable': false}
    ],
    "fnRowCallback" : function(nRow, aData, iDisplayIndex){
        // For auto numbering at 'No' column
        var start = me.tbl.page.info().start;
        $('td:eq(0)',nRow).html(start + iDisplayIndex + 1);
    },
  });

    $('#tag-table tbody').on('mouseover', 'tr td', function (e) {
      $(this).find('.action-column').removeClass('hidden');
    }).on('mouseout', 'td', function (e) {
      $(this).find('.action-column').addClass('hidden');
    });

    $('#tag-table tbody').on('click', 'tr', function (e) {
      e.preventDefault();
      if($(e.target).is("td")){
        var data = $(this).find('td .edit-link').data();
        me.editLink = data.editLink
        me.updateLink = data.updateLink
        me.first = true
        me.edit()
      }
    });

    $('#tag-table').on('click', '.del-link', (e) => {
      e.preventDefault();
      this.deleteLink = e.currentTarget.dataset.deleteLink;
      this.delete();
    });

   

  },
})