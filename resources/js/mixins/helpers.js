module.exports = {
  methods: {
    notify (message, type = 'success') {
      swal({
        text: message,
        icon: type,
        buttons: false,
        timer: 1500
      })
    },
    deleteConfirm () {
      swal({
        title: 'Are you sure?',
        icon: 'warning',
        buttons: true,
        dangerMode: true
      }).then((willDelete) => {
          return willDelete;
      });
    },

    handleDelete (url) {
      swal({
        title: 'Are you sure?',
        icon: 'warning',
        buttons: true,
        dangerMode: true
      }).then((willDelete) => {
        if (willDelete) {
          axios.delete(url)
              .then(response => {
                this.notify(response.msg)
                this.tbl.ajax.reload(null, false);
              })
              .catch(error => {
                
          });
        }
        return false;
      });
    }
    
  }
}