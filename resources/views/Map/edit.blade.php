@extends('layouts.admin')
@section('content')
    <div class="page-header">
        <div class="col-xs-6">
            <h1 class="page-title">Events</h1>
            <ol class="breadcrumb breadcrumb-arrow">
              <li><a href="{{route('dashboard')}}">Home</a></li>
              <li class="active">Events</li>
            </ol>     
        </div>
        <div class="col-xs-6 text-right"></div>
    </div>
    <div class="clearfix"></div>
    <div class="page-content" >
        <div class="panel">
            <div class="panel-body container-fluid" id="tbl-user">
                <div class="success-alert-area"> </div>
                <div class="clearfix sp-margin-sm"></div>
                <div class="card">
                    <div class="card-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                  @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                  @endforeach
                                </ul>
                            </div>
                        @endif
                          <form action="{{ route('map.update',$event->id) }}" method="post" enctype="multipart/form-data" >
                            @method('PATCH')
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Name</label>
                                <input class="form-control" type="text"  name="title" value="{{$event->title}}">
                            </div>
                            <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Description</label>
                                            <textarea class="form-control" name="description">@if($event->description){{$event->description}}@endif</textarea>
                                        </div>
                             <div class="form-group">
                                <label for="files" class="col-form-label">Image</label>
                                <input class="form-control" type="file" name="files" />
                            </div>
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Date</label>
                                <div class="input-group">
                                    <input id="start_date" type="text" class="form-control" value="{{$event->date}}" name="date"required>
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar bigger-110"></i>
                                    </span>
                                </div>   
                                
                            </div>
                             <div class="form-group">
                                <label for="event_time" class="col-form-label">Time</label>
                                <input class="form-control" type="text"  name="event_time" value="{{$event->event_time}}">
                            </div>
                             <div class="form-group">
                                <label for="event_place" class="col-form-label">Event Place</label>
                                <input class="form-control" type="text"  name="event_place" value="{{$event->event_place}}">
                            </div>
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Latitude</label>
                                <input class="form-control" type="text"  name="latitude" value="{{$event->latitude}}" id="us3-lat" readonly>
                            </div>
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Longitude</label>
                                <input class="form-control" type="text"  name="longitude" value="{{$event->longitude}}" id="us3-lon" readonly>
                            </div>
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Location</label>
                                <input class="form-control" type="text"  name="location" value="{{$event->location}}" id="us3-address">
                            </div>
                            <div id="us3" style="width: 550px; height: 400px;"></div>
                            <br>
                            <form action="{{ route('map.store', $event->id) }}">
                                {{ csrf_field() }}
                                {{ method_field("patch") }}
                                 <div class="form-group">
                                    <a href="{{route('map.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                   <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                </div>
                            </form>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script  src='https://maps.google.com/maps/api/js?key=AIzaSyAnuPCfATkGIl7rFif3WzdpiUHyTIvmpeA&libraries=places'></script>
  <script src="{{asset('js/locationpicker.jquery.min.js')}}"></script>
<script>
    $('#start_date').datepicker({
         autoclose: true,
         format: 'yyyy-mm-dd',
         todayHighlight: true,
         defaultDate:false
    })
    var lat = $('#us3-lat').val();
    var long = $('#us3-lon').val();
   
 $('#us3').locationpicker({
    location: {
        latitude: lat,
        longitude:long
    },
    radius: 300,
    inputBinding: {
        latitudeInput: $('#us3-lat'),
        longitudeInput: $('#us3-lon'),
        radiusInput: $('#us3-radius'),
        locationNameInput: $('#us3-address')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        // Uncomment line below to show alert on each Location Changed event
        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
});
</script>
@endpush






