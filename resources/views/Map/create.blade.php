@extends('layouts.admin')
@section('content')
    <div class="page-header">
        <div class="col-xs-6">
            <h1 class="page-title">Users</h1>
            <ol class="breadcrumb breadcrumb-arrow">
              <li><a href="{{route('dashboard')}}">Home</a></li>
              <li class="active">Users</li>
            </ol>     
        </div>
        <div class="col-xs-6 text-right"></div>
    </div>
    <div class="clearfix"></div>
    <div class="page-content" >
        <div class="panel">
            <div class="panel-body container-fluid" id="tbl-user">
                <div class="success-alert-area"> </div>
                <div class="clearfix sp-margin-sm"></div>
                <div class="card">
                    <div class="card-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                  @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                  @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('map.store') }}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Name</label>
                                <input class="form-control" type="text"  name="title" value="{{old('title')}}" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <label for="example-search-input" class="col-form-label">Description</label>
                                <textarea name="description" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="files" class="col-form-label">Image</label>
                                <input class="form-control" type="file" name="files" />
                            </div>
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Date</label>
                                <div class="input-group">
                                    <input id="start_date" type="text" class="form-control" value="{{old('date')}}" name="date" required> 
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar bigger-110"></i>
                                    </span>
                                </div>   
                                
                            </div>
                            <div class="form-group">
                                <label for="event_time" class="col-form-label">Time</label>
                               <input class="form-control" type="text"  name="event_time" value="{{old('event_time')}}" placeholder="12:00 AM" required>  
                                
                            </div>
                            <div class="form-group">
                                <label for="event_place" class="col-form-label">Event Place</label>
                               <input class="form-control" type="text"  name="event_place" value="{{old('event_place')}}" placeholder="Event Place" required>  
                                
                            </div>
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Latitude</label>
                                <input class="form-control" type="text"  name="latitude" value="{{old('latitude')}}" id="us3-lat">
                            </div>
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Longitude</label>
                                <input class="form-control" type="text"  name="longitude" value="{{old('longitude')}}" id="us3-lon">
                            </div>
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Location</label>
                                <input class="form-control" type="text"  name="location" value="{{old('location')}}" id="us3-address">
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-8">
                                    <a href="{{route('playerlist')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                </div>
                            </div>
                            <div id="us3" style="width: 550px; height: 400px;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script  src='http://maps.google.com/maps/api/js?key=AIzaSyAnuPCfATkGIl7rFif3WzdpiUHyTIvmpeA&libraries=places'></script>
  <script src="{{asset('js/locationpicker.jquery.min.js')}}"></script>
<script>
    $('#start_date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        defaultDate:false
    })
 $('#us3').locationpicker({
    location: {
        latitude: 16.80528,
        longitude:96.15611
    },
    radius: 300,
    inputBinding: {
        latitudeInput: $('#us3-lat'),
        longitudeInput: $('#us3-lon'),
        radiusInput: $('#us3-radius'),
        locationNameInput: $('#us3-address')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        // Uncomment line below to show alert on each Location Changed event
        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
});
</script>
@endpush