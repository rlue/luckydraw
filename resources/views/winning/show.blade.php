@extends('layouts.admin')
@section('content')
  <div class="page-header">
    <div class="col-xs-6">
      <h1 class="page-title">Team Player List</h1>
      <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="{!! URL::to('dashboard') !!}">Home</a></li>
        <li class="active">@if(isset($team->name)) {{$team->name}} @else {{"no team"}}@endif</li>
      </ol>     
    </div>
    <div class="col-xs-4">
      @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ Session::get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="page-content" >
    <div class="panel">
      <div class="panel-body container-fluid" id="tbl-user">
        <div class="success-alert-area"> </div>
        <div class="clearfix sp-margin-sm"></div>
        <h5>{{$team->name}} Player List,  {{$team->region}} Region</h5>
        <table class="table table-hover dataTable table-striped width-full dtr-inline" id='userListTable' >
          <thead>
            <tr>
              
              <th>Name</th>
              <th>Email</th>
              <th>Phone No</th>
              <th>Nrc NO</th>
              
            </tr>
          </thead>
          <tbody> 
          @if(!$playerList->isEmpty())
            @foreach($playerList as $p)
              <tr>
                <td>{{$p->name}}</td>
                <td>{{$p->email}}</td>
                <td>{{$p->phone_no}}</td>
                <td>{{$p->nrc_no}}</td>
              </tr>
            @endforeach
          @endif
          </tbody>
        </table> 
      </div>
    </div>
  </div>
@endsection
