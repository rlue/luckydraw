@extends('layouts.admin')
@section('content')
  <div class="page-header">
    <div class="col-xs-6">
      <h1 class="page-title">Winning List</h1>
      <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="{{route('dashboard')}}">Home</a></li>
        <li class="active">Winning List</li>
      </ol>     
    </div>
    <div class="col-xs-4">
      @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ Session::get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="page-content" >
    <div class="panel">
      <div class="panel-body container-fluid" id="tbl-user">
        <div class="success-alert-area"> </div>
        <div class="clearfix sp-margin-sm"></div>
        <table class="table table-hover dataTable table-striped width-full dtr-inline" id='winningTable' >
          <thead>
            <tr>
              <th>No</th>  
              <th>Iteam</th>
              <th>User Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Date</th>
             
            </tr>
          </thead>
          <tbody> 

          </tbody>
        </table> 
      </div>
    </div>
  </div>
@endsection
@push('scripts')
<script>
$(function() {
  var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
   this.tbl = $('#winningTable').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('winning.getData') !!}',
      columns: [
        { data: 'id', name:'id'  },
        { data: 'luckyitem.name',name: 'luckyitem.name' },
        { data: 'customer.username',name: 'customer.username' },
        { data: 'customer.email',name: 'customer.email' },
        { data: 'customer.phone_no',name: 'customer.phone_no' },
        { data: 'created_at',name: 'created_at' },
      ],
  });
});
</script>
@endpush