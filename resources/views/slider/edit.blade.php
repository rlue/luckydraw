@extends('layouts.admin')

@section('content')
    

   <div class="page-header">
  <div class="col-xs-6">
    <h1 class="page-title">Slider</h1>
    <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{{route('dashboard')}}">Home</a></li>
      <li><a href="{{route('sliders.index')}}">Slider</a></li>
      <li class="active">Edit</li>
    </ol>     
  </div>
  <div class="col-xs-6 text-right">
   
    
   
  </div>
</div>
<div class="clearfix"></div>
<div class="page-content" >
  <div class="panel">
    <div class="panel-body container-fluid" id="tbl-user">
      <div class="success-alert-area"> </div>
      <div class="clearfix sp-margin-sm"></div>
      <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('sliders.update',$slider->id)}}" enctype="multipart/form-data" >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Slider</h4>
                                        
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Title</label>
                                            <input class="form-control" type="text"  name="title" value="{{$slider->title}}" placeholder="Title">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Url</label>
                                            <input class="form-control" type="text"  name="url" value="{{$slider->url}}" placeholder="Title">
                                        </div>
                                        <div class="form-group">
                                            <label for="btn_name" class="col-form-label">Button Name</label>
                                            <input class="form-control" type="text"  name="btn_name" value="{{$slider->btn_name}}" placeholder="Button Name">
                                        </div>
                                       <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">File</label>
                                            <input class="form-control" type="file" name="files" />
                                        </div>
                                       <form action="{{ route('sliders.store', $slider->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('sliders.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
    </div>
  </div>
</div>
@endsection






