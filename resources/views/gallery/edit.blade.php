@extends('layouts.admin')

@section('content')
    

   <div class="page-header">
  <div class="col-xs-6">
    <h1 class="page-title">Gallery</h1>
    <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{{route('dashboard')}}">Home</a></li>
      <li><a href="{{route('galleries.index')}}">Gallery</a></li>
      <li class="active">Edit</li>
    </ol>     
  </div>
  <div class="col-xs-6 text-right">
   
    
   
  </div>
</div>
<div class="clearfix"></div>
<div class="page-content" >
  <div class="panel">
    <div class="panel-body container-fluid" id="tbl-user">
      <div class="success-alert-area"> </div>
      <div class="clearfix sp-margin-sm"></div>
      <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('galleries.update',$gallery->id)}}" enctype="multipart/form-data" >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Gallery</h4>
                                        
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Title</label>
                                            <input class="form-control" type="text"  name="title" value="{{$gallery->title}}" placeholder="Title">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Description</label>
                                            <textarea class="form-control" name="description">@if($gallery->description){{$gallery->description}}@endif</textarea>
                                        </div>
                                       <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Image</label>
                                            <input class="form-control" type="file" name="files" />
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Video</label>
                                            <input class="form-control" type="file" name="video_files" />
                                        </div>
                                       <form action="{{ route('galleries.store', $gallery->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('galleries.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
    </div>
  </div>
</div>
@endsection






