@extends('layouts.admin')

@section('content')
    

   <div class="page-header">
  <div class="col-xs-6">
    <h1 class="page-title">Lucky Items</h1>
    <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{{route('dashboard')}}">Home</a></li>
      <li><a href="{{route('luckyitems.index')}}">Lucky Items</a></li>
      <li class="active">Create</li>
    </ol>     
  </div>
  <div class="col-xs-6 text-right">
   
    
   
  </div>
</div>
<div class="clearfix"></div>
<div class="page-content" >
  <div class="panel">
    <div class="panel-body container-fluid" id="tbl-user">
      <div class="success-alert-area"> </div>
      <div class="clearfix sp-margin-sm"></div>
      <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('luckyitems.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create Lucky Items</h4>
                                        
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Name</label>
                                            <input class="form-control" type="text"  name="name" placeholder="Name" >
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Messsage</label>
                                            <textarea name="message" class="form-control"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="item_count" class="col-form-label">Count</label>
                                            <input class="form-control" type="text"  name="item_count" placeholder="Count" >
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email-input" class="col-form-label">Background Color</label>
                                            <input class="form-control" type="color"  name="background_color" />
                                        </div>
                                        
                                        
                                       
                                        <div class="form-group">
                                            <a href="{{route('luckyitems.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
    </div>
  </div>
</div>
@endsection


