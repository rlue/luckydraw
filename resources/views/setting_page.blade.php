
@extends('layouts.admin')

@section('content')
    
  <div class="page-header">
  <div class="col-xs-6">
    <h1 class="page-title">Setting</h1>
    <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{!! URL::to('dashboard') !!}">Home</a></li>
      <li class="active">Setting</li>
    </ol>     
  </div>
  <div class="col-xs-4">
       @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <i class="fa fa-remove"></i>
                                </button>
                            </div>
                        @endif
  </div>
  <div class="col-xs-2 text-right">
   
    
   
  </div>
</div>
<div class="clearfix"></div>
<div class="page-content" >
  <div class="panel">
    <div class="panel-body container-fluid" id="tbl-user">
      <div class="success-alert-area"> </div>
      <div class="clearfix sp-margin-sm"></div>
      @include('app_settings::_settings')
    </div>
  </div>
</div>
@endsection


@push('scripts')

@endpush