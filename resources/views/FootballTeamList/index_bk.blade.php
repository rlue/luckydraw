@extends('layouts.admin')
@section('content')
  <div class="page-header">
    <div class="col-xs-6">
      <h1 class="page-title">Team Player List</h1>
      <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="{!! URL::to('dashboard') !!}">Home</a></li>
        <li class="active">Team Player List</li>
      </ol>     
    </div>
    <div class="col-xs-4">
      @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ Session::get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="page-content" >
    <div class="panel">
      <div class="panel-body container-fluid" id="tbl-user">
        <div class="success-alert-area"> </div>
        <div class="clearfix sp-margin-sm"></div>
        <table class="table table-hover dataTable table-striped width-full dtr-inline" id='playerListTable' >
          <thead>
            <tr>
              <th>No</th>  
              <th>Team Name</th>  
              <th>Email</th>
              <th>Phone No</th>
              <th>NRC No</th>
              <th class="action-col">Action</th>
            </tr>
          </thead>
          <tbody> 

          </tbody>
        </table> 
      </div>
    </div>
  </div>
@endsection
@push('scripts')
<script>
$(function() {
  var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
   this.tbl = $('#playerListTable').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('footballTeamList.getData') !!}',
      columns: [
          { data: 'id', name:'id'  },
          { data: 'name', name: 'name' },
          { data: 'email', name: 'email' },
          { data: 'phone_no', name: 'phone_no' },
          { data: 'nrc_no', name: 'nrc_no' },
          { data: 'action', name: 'action', orderable: false, searchable: false},
      ],
  });
  $('#playerListTable').on('click', '.sub-delete', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var checked = confirm("Sure You want to delete it");
    if(checked){
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type : "DELETE",
        url  : baseUrl + '/backend/footballTeamList/' + id,
        data : {},
        success: function(data){
          var result = $.parseJSON(data);
          if(result['status'] == 'success'){
              me.tbl.ajax.reload( null, false );
          }else{
              alert(result['message']);        
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert('Error in student view. Please contact to administrator');
        }
      });  
    }else{
        return false;
    }  
  });
});
</script>
@endpush