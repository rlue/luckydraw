@extends('layouts.master')
@section('content')
<main role="main" class="container bootstrap snippet">
	<h3>Football Team Player List</h3>
	<hr>
	<div class="row" style="float: right;">
		<div class="col-xs-4" >
	      @if(Session::has('message'))
	        <div class="alert alert-success alert-dismissible fade show" role="alert">
	          {{ Session::get('message') }}
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	      @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-7 offset-md-2">	
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
					    @foreach ($errors->all() as $error)
					      <li>{{ $error }}</li>
					    @endforeach
					</ul>
				</div>
		    @endif
		</div>
	</div>
	<div class="col-md-7 offset-md-2">
		@php $count = 1;@endphp
		@foreach($players as $player)
			<div id="row">
				@if($count == 1)
					<h5>Captain</h5>
				@else
					<h5>Player {{$count}}</h5>
				@endif
				<div class="form-group row">
		            <label for="name" class="col-sm-4 col-form-label">Name</label>
		            <div class="col-sm-8">
		                <span>{{$player->name}}</span>
		            </div>
		            <label for="name" class="col-sm-4 col-form-label">Email</label>
		            <div class="col-sm-8">
		                <span>{{$player->email}}</span>
		            </div>
		            <label for="name" class="col-sm-4 col-form-label">Phone No</label>
		            <div class="col-sm-8">
		                <span>{{$player->phone_no}}</span>
		            </div>
		            <label for="name" class="col-sm-4 col-form-label">NRC No</label>
		            <div class="col-sm-8">
		                <span>{{$player->nrc_no}}</span>
		            </div>
		        </div>
		    </div>  
		    <?php $count++;?>
		@endforeach
		<div class="form-group row">
		 	<label class="col-sm-4 col-form-label"></label>
		 	<div class="col-sm-8">
		        <a href="{{route('footballTeamList.index')}}" class="btn btn-primary"><i class="fa fa-remove"></i> Back</a>                   
		    </div>
		</div>
	</div>
</main>
@endsection
