@extends('layouts.admin')
@section('content')
<main role="main" class="container">
	<h3>Football Team Edit</h3>
	<hr>
	<div class="row">
		<div class="col-md-6 offset-md-2">
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
					    @foreach ($errors->all() as $error)
					      <li>{{ $error }}</li>
					    @endforeach
					</ul>
				</div><br/>
		    @endif
			<form method="POST" action="{{route('footballTeamList.update',$team->id)}}"  >
                @method('PATCH')
		        <div class="form-group row">
		            <label for="team_name" class="col-sm-4 col-form-label">Team Name<b class="highlight">***</b></label>
		            <div class="col-sm-8">
		                <input type="text" class="form-control" id="team_name" name="team_name" value="{{$team->name}}" required>
		            </div>
		        </div>
		        <div class="form-group row">
		            <label for="region" class="col-sm-4 col-form-label">Region<b class="highlight">***</b></label>
		            <div class="col-sm-8">
		                <input type="text" class="form-control" id="region" name="region" value="{{$team->region}}" >
		            </div>
		        </div>
		         <form action="{{ route('footballTeamList.store', $team->id) }}">
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                     <div class="form-group row">
                     	<label class="col-sm-4 col-form-label"></label>
                     	<div class="col-sm-8">
	                        <a href="{{route('footballTeamList.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
	                       <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
	                    </div>
                    </div>
                </form>
			</form>
		</div>	
	</div>
</main>
@endsection