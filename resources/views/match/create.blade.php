@extends('layouts.admin')

@section('content')
    

   <div class="page-header">
  <div class="col-xs-6">
    <h1 class="page-title">Matches</h1>
    <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{{route('dashboard')}}">Home</a></li>
      <li><a href="{{route('matches.index')}}">Matches</a></li>
      <li class="active">Create</li>
    </ol>     
  </div>
  <div class="col-xs-6 text-right">
   
    
   
  </div>
</div>
<div class="clearfix"></div>
<div class="page-content" >
  <div class="panel">
    <div class="panel-body container-fluid" id="tbl-user">
      <div class="success-alert-area"> </div>
      <div class="clearfix sp-margin-sm"></div>
      <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('matches.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create Match</h4>
                                        
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Title</label>
                                            <input class="form-control" type="text"  name="title" placeholder="Name" >
                                        </div>
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Event Date</label>
                                            <input class="form-control" type="text"  name="event_date" id="event_date" placeholder="Event Date" >
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email-input" class="col-form-label">Event</label>
                                            <select class="form-control" id="eventid" name="eventid">
                                              <option value>Select Event</option>
                                              @foreach($event as $e)
                                                <option value="{{$e->id}}">{{$e->title}}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Description</label>
                                            <textarea name="description" class="form-control"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Team Match</label>
                                            <div id="matchList" ></div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="example-email-input" class="col-form-label">Team 1</label>
                                            <select class="form-control" id="team_one">
                                              <option value>Select Team</option>
                                              @foreach($team as $t)
                                                <option value="{{$t->id}}">{{$t->name}}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-5">
                                            <label for="example-email-input" class="col-form-label">Team 2</label>
                                            <select class="form-control" id="team_two">
                                              <option value>Select Team</option>
                                              @foreach($team as $t)
                                                <option value="{{$t->id}}">{{$t->name}}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                        <label for="example-email-input" class="col-form-label"></label>
                                         <a href="#" id="team_match" class="btn btn-primary">Add</a>
                                        </div>
                                       
                                        <div class="form-group">
                                            <a href="{{route('matches.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script>
$('#event_date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        defaultDate:false
    });
$('#eventid').select2();
$('#team_one').select2();
$('#team_two').select2();
$(function() {
    var baseUrl = '{!! URL::to("/") !!}';
  
  $('#team_match').click(function(e){
    e.preventDefault();
     var teamOne = $('#team_one').val();
     var teamOneText = $('#team_one option:selected').text();
     var teamTwo = $('#team_two').val();
     var teamTwoText = $('#team_two option:selected').text();
     
     var teamData = "<div class='match_team'><div class='form-group col-sm-4'><input type='text' class='form-control '  value='"+teamOneText+"' readonly /><input type='hidden' name='home_team[]' value='"+teamOne+"' /></div><div class='form-group col-sm-1'>VS<input type='hidden' class='form-control ' name='result[]' value='?-?' readonly/></div><div class='form-group col-sm-4'><input type='text' class='form-control '  value='"+teamTwoText+"' readonly/><input type='hidden' name='away_team[]' value='"+teamTwo+"' /></div><div class='form-group col-sm-2'><input type='text' class='form-control ' name='match_time[]' value='' placeholder='12:30 AM' required /></div><div class='form-group col-sm-1'><a href='#' class='btn btn-danger  remove_match'><i class='fa fa-remove'></i></a></div></div>";
     $('#matchList').append(teamData);
  });
   $('#matchList').on('click','.remove_match',function(e){
    e.preventDefault();
      
        $(this).parent('div').parent('div').remove();
   });
});
  

</script>
@endpush

