@extends('layouts.master1')
@push('styles')
 
@endpush
@section('content')
<main role="main" class="container" style="padding-top:20px;">
  
   <div class="row evenHeadText">
    <div class="col-md-4 galleryText">
      <h3>Video GALLERY</h3>
      <p><b>&lsquo;&lsquo;</b>Lorem Ipsum is simply dummmytext of the prinitingand typesetting industry.Lorem Ipsum has been the Industry's standard dummy text ever since the 1500s, when an unknown printer took a gallery of type and scrambled it to make a type specimen book.It has survived not o<b>&rsquo;&rsquo;</b></p>
    </div>
    <div class="col-md-8">
      <div class="row">
        @foreach($video as $v)
         <div class="col-sm-4">
          <a href="{{route('videoGallery_detail',$v->id)}}">
            <div class="imageText">
              <h5>{{$v->title}}</h5>
              
            </div>
            <div class="galleryImg">
              <img src="{{$v->getMediaImageUrl()}}"  alt="Responsive image" width="200" />
            </div>  
            </a>
          </div>
        @endforeach
      
      {{ $video->links() }}
      </div>
    </div>
  </div>
</main>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function($) { //noconflict wrapper
    var heights = $(".imageText p").map(function() {
        return $(this).height();
    }).get(),

    maxHeight = Math.max.apply(null, heights);

    $(".imageText p").height(maxHeight);
  });

</script>

@endpush