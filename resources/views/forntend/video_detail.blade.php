@extends('layouts.master1')
@push('styles')

  <link href="https://vjs.zencdn.net/7.6.0/video-js.css" rel="stylesheet">

  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
  <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
@endpush
@section('content')
<main role="main" class="container" style="padding-top:20px;">
  <div class="row">
    <div class="col-md-4 galleryText">
      <h3>{{$video->title}}</h3>
      <p>{{$video->description}}</p>
    </div>
    <div class="col-md-8">
   <!--  <video id='my-video' class='video-js' poster="#" playsinline autoplay loop controls preload='auto' width='640' height='264'
  poster='MY_VIDEO_POSTER.jpg' data-setup='{}'>
    <source src="{{$video->getMediaUrl()}}" type='video/mp4'>
    <source src="{{$video->getMediaUrl()}}" type='video/ogg'>
    <p class='vjs-no-js'>
      To view this video please enable JavaScript, and consider upgrading to a web browser that
      <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
    </p>
  </video> -->

  
      <video width="100%" autoplay loop muted playsinline controls>
          <source src="{{$video->getMediaUrl()}}" type="video/mp4;codecs='avc1.42E01E, mp4a.40.2'">
          <source src="{{$video->getMediaUrl()}}" type="video/ogg">
          Your browser does not support HTML5 video.
      </video>
    </div>
  </div>
   
</main>
@endsection
@push('scripts')
<script src='https://vjs.zencdn.net/7.6.0/video.js'></script>
<script type=”text/javascript” src=”//cdn.jsdelivr.net/afterglow/latest/afterglow.min.js”></script>
@endpush