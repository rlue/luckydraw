@extends('layouts.master')
@section('content')
<main role="main" class="container">
  <div class="row evenHeadText">
    <div class="col-md-12">
      <h3>{{$events->title}}</h3>  
     <!--  <br>    
      <p><b>&lsquo;&lsquo;</b>{{$events->description}}<b>&rsquo;&rsquo;</b></p> -->
    </div>
  </div>
  <br><br/>
  <div class="row eventDetailContent">
    <div class="col-md-6" style="margin-bottom: 25px;">  
      <div id="us3" style="width: 100%; height: 300px;"></div> 
    </div>  
    <div class="col-md-6 align-items-center h-100">
      <div class="col-md-12 locContent mx-auto">   
        <div class="jumbotron">
          <h5>{{$events->event_place}}</h5>
          
          <dl style="display: none;">
            <dt class="col-sm-4"><i>Date:</i></dt>
            <dd class="col-sm-12" ><i>{{$events->date}}</i></dd>
          </dl>  
          <dl style="display: none;">
            <dt class="col-sm-4"><i>Latitude:</i></dt>
            <dd class="col-sm-12" id="lat"><i>{{$events->latitude}}</i></dd>
          </dl>        
          <dl style="display: none;">
            <dt class="col-sm-4"><i>Longitude:</i></dt>
            <dd class="col-sm-12" id="lon"><i>{{$events->longitude}}</i></dd>
          </dl>
          <br>
          <dl>
            <dt class="col-xs-1"><i class="fa fa-map-marker" aria-hidden="true"></i></dt>
            <dd class="col-sm-12" id="location"></dd>
            <input type="hidden" id="hd_location">
          </dl>
          <!-- <dl>
            <dt class="col-sm-1"><i class="fa fa-phone" aria-hidden="true"></i></dt>
            <dd class="col-sm-12"></dd>
          </dl> -->
        </div>
      </div>  
    </div>
    
    <br>
  </div>
</main>
@endsection
@push('scripts')
<script  src='https://maps.google.com/maps/api/js?key=AIzaSyAnuPCfATkGIl7rFif3WzdpiUHyTIvmpeA&libraries=places'></script>
    <script src="{{asset('js/locationpicker.jquery.min.js')}}"></script>
<script>    
  var lat = $('#lat').text();
  var long = $('#lon').text();
  $('#us3').locationpicker({
    location: {
      latitude: lat,
      longitude:long
    },
    radius: 300,
    inputBinding: {
      locationNameInput: $('#hd_location')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        // Uncomment line below to show alert on each Location Changed event
        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
  });
 
  $('#hd_location').change(function() {
    var loc = $('#hd_location').val();
    $('#location').html(loc);
  });
</script>
@endpush
