@extends('layouts.master')
@section('content')
<main role="main" class="container">
	<h3>Player Edit</h3>
	<hr>
	<div class="row">
		<div class="col-md-6 offset-md-2">
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
					    @foreach ($errors->all() as $error)
					      <li>{{ $error }}</li>
					    @endforeach
					</ul>
				</div><br/>
		    @endif
			<form method="POST" action="{{route('teamupdate',$teamName->id)}}" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
		        <div class="form-group row">
		            <label for="username" class="col-sm-4 col-form-label">Team Name<b class="highlight">*</b></label>
		            <div class="col-sm-8">
		                <input type="text" class="form-control" id="name" name="name" value="{{$teamName->name}}" required>
		            </div>
		        </div>
		        
				<div class="form-group row">
					<label class="col-sm-4 col-form-label"></label>
					<div class="col-sm-8">
				    	<a href="{{route('playerlist')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
				   		<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
					</div>
				</div>
			</form>
		</div>	
	</div>
</main>

@endsection