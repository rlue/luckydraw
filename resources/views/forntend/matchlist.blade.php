@extends('layouts.master')
@section('content')
<main role="main" class="container">
  <div class="row evenHeadText">
    <div class="col-md-12">
      <h3>{{$event->title}}</h3>  
      <br>    
      <p><b>&lsquo;&lsquo;</b>{{$event->description}}<b>&rsquo;&rsquo;</b></p>
    </div>
    <div class="col-xs-4" style="float: right;">
      @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ Session::get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
    </div>
  </div>
  <br><br/>
  <div class="row">
    <div class="col-md-12">
     <div class="table-responsive">
    <table class="table table-dark table-hover">
    @foreach($matchlist as $m)
    <thead>
    <tr>
      <th style="width:25%"><span class="float-left">{{$m->title}}</span></th>
      <th></th>
      <th></th>
      <th><span class="float-right">{{date('d-m-Y',strtotime($m->event_date))}}</span></th>
    </tr>
    </thead>
    <tbody></tbody>
    @foreach($m->matchdetail($m->id) as $list)
      <tr>
        <td class="float-left">{{$list->match_time}}</td>
        <td >{{$list->homeTeam}}</td>
        <td >{{$list->result}}</td>
        <td>{{$list->awayTeam}}</td>
      </tr>
    @endforeach
    </tbody>
    @endforeach
    </table>
  </div>
    
  
        
      </div>
    </div>
  </div>
</main>
@endsection
