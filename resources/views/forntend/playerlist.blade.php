@extends('layouts.master')
@section('content')
<main role="main" class="container">
  <div class="row">
    <div class="col-md-12">
      <h3>{{$teamName->name}}</h3>
      <div class="col-xs-4" style="float: right;">
          @if(Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{ Session::get('message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
        </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
          </ul>
        </div><br/>
        @endif
        <table class="table table-hover dataTable table-striped width-full dtr-inline" id='playerListTable' >
          <thead>
            <tr>
              <th>No</th>  
              <th>Player Name</th>  
              <th>Email</th>
              <th>Phone No</th>
              <th>NRC No</th>
              <th class="action-col">Action</th>
            </tr>
          </thead>
          <tbody> 
            @php $count=1;@endphp
            
            @foreach($playerlist as $list)
              <tr>
                <td>{{$count++}}</td>
                <td>{{$list->name}}</td>
                <td>{{$list->email}}</td>
                <td>{{$list->phone_no}}</td>
                <td>{{$list->nrc_no}}</td>
                <td><a href="{{route('playeredit', $list->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a></td>
              </tr>
            @endforeach
           
          </tbody>
        </table>
        
      </div>
    </div>
  </div>
</main>
@endsection
@push('scripts')
<script>
$(function() {
  $('#playerListTable').on('click', '.sub-delete', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var checked = confirm("Sure You want to delete it");
    if(checked){
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type : "DELETE",
        url  : baseUrl + '/backend/footballTeamList/' + id,
        data : {},
        success: function(data){
          var result = $.parseJSON(data);
          if(result['status'] == 'success'){
              me.tbl.ajax.reload( null, false );
          }else{
              alert(result['message']);        
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert('Error in student view. Please contact to administrator');
        }
      });  
    }else{
        return false;
    }  
  });
});
</script>
@endpush
