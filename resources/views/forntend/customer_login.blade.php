@extends('layouts.master1')
@section('content')
<div class="LoginRegContent">
	<main role="main" class="container" style="margin-top: 5px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="LoginContent">
					<figure><img src="{{ asset('images/tg_logo.png') }}"></figure>
					<div class="headText">
						<h3>Sign in</h3>
						
						<div class="row " style="float: right;">
							<div class="col-xs-4" >
						      @if(Session::has('message'))
						        <div class="alert alert-success alert-dismissible fade show" role="alert">
						          {{ Session::get('message') }}
						          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						            <span aria-hidden="true">&times;</span>
						          </button>
						        </div>
						      @endif
						    </div>
						</div>
					</div>
					
					<div class="row" style="margin:0px;">
						<div class="col-md-12">	
							<ul>
							    @foreach ($errors->all() as $error)
								    <div class="alert alert-danger">
								      <li>{{ $errors->first('email') }}</li>
								    </div>
							    @endforeach
							</ul>
						</div>
							<form class="form_login" action="{{ route('customer_login') }}" method="post" >
								@csrf
						        <div class="form-group row input">
						        	@if ($errors->has('email'))
				                    	<div class="alert alert-danger">{{ $errors->first('email') }}</div>
				                	@endif
						            <label for="email" class="col-sm-4 col-3 col-form-label">Email</label>
						            <div class="col-sm-8 col-9">
						                <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}" required >
						            </div>
						        </div>
						        <div class="form-group row input">
						        	@if ($errors->has('password'))
	                              		<div class="alert alert-danger">{{ $errors->first('password') }}</div>
	                        		@endif
						            <label for="password" class="col-sm-4 col-3 col-form-label">Password</label>
						            <div class="col-sm-8 col-9">
						                <input type="password" class="form-control" id="password" name="password" required>
						            </div>
						        </div>
						        <div class="form-group" style="margin-top:15px;color:#fff;">
						        <p class="float-left ">No account?<a href="{{route('userReg.index')}}" style="color:#fff;"> Create One!</a>	</p>
						        
						            <button type="submit" class="btn btn-tiger float-right">Login</button>
						               
						        
						        </div>
							</form>
						<div class="btnSocialContent">
							<h5>Login with Social Media</h5>
			                <a  href="{{route('fb_login')}}" class="btn btn-primary "><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a>
			            </div>	
					</div>
				</div>
			</div>
		</div>
		<!-- <style type="text/css">
			html,body,.container,.main-content,.main-content-inner{
				height: 100% !important;
			}
			
		</style> -->
		
	</main>
</div>
@endsection