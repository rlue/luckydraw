@extends('layouts.master')
@section('content')
<main role="main" class="container">
  
  <div class="row ">
  <?php $count=1; ?>
  @foreach($eventlists as $list)
    <div class="col-sm-6 event_listimg">
       <img src="{{$list->getMediaUrl()}}" class="img-fluid img-thumbnail" alt="Responsive image" width="100%" alt="...">
          
    </div>
    <div class="col-sm-6 event_list">
        <h4>{{$list->title}}</h4>
         <p>
            <strong>Date:</strong>
            {{$list->date}}
          </p> 
          <p>
            <strong>Time:</strong>
            {{$list->event_time}}
          </p>  
          <p style="display: none;">
            <strong>Latitude:</strong>
            <span id="lat{{$count}}">{{$list->latitude}}</span>
            
          </p>        
          <p style="display: none;">
            <strong>Longitude:</strong>
           <span id="lon{{$count}}">{{$list->longitude}}</span>
           
          </p>
          <div id="us{{$count}}" style="display: none;"></div> 
          <p>
            <strong>Event Place :</strong>
            <span >{{$list->event_place}}</span>
            <input type="hidden" id="hd_location{{$count}}">
          </p>
        <p><a href="{{route('eventdetail', $list->id)}}" class="btn btn-tiger"> <i class="fa fa-map-marker" aria-hidden="true"></i> Get Direction</a>
       <!-- <a href="{{route('matchList', $list->id)}}" class="btn btn-tiger float-right"> <i class="fa fa-futbol-o" aria-hidden="true"></i> Match List</a> --></p>
    </div>
  <hr/>    <?php $count++; ?>
  @endforeach
  </div>
  
</main>
@endsection
@push('scripts')
<script  src='https://maps.google.com/maps/api/js?key=AIzaSyAnuPCfATkGIl7rFif3WzdpiUHyTIvmpeA&libraries=places'></script>
    <script src="{{asset('js/locationpicker.jquery.min.js')}}"></script>
<script>    
  var lat = $('#lat1').text();
  var long = $('#lon1').text();
  
  $('#us1').locationpicker({
    location: {
      latitude: lat,
      longitude:long
    },
    radius: 300,
    inputBinding: {
      locationNameInput: $('#hd_location1')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        // Uncomment line below to show alert on each Location Changed event
        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
  });
 
  
   $('#hd_location1').change(function() {
    var loc = $('#hd_location1').val();
    $('#location1').html(loc);
  });
 
</script>
<script>    
  var lat = $('#lat2').text();
  var long = $('#lon2').text();
  
  $('#us2').locationpicker({
    location: {
      latitude: lat,
      longitude:long
    },
    radius: 300,
    inputBinding: {
      locationNameInput: $('#hd_location2')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        // Uncomment line below to show alert on each Location Changed event
        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
  });
 
  
   $('#hd_location2').change(function() {
    var loc = $('#hd_location2').val();
    $('#location2').html(loc);
  });
 
</script>
<script>    
  var lat = $('#lat3').text();
  var long = $('#lon3').text();
  
  $('#us3').locationpicker({
    location: {
      latitude: lat,
      longitude:long
    },
    radius: 300,
    inputBinding: {
      locationNameInput: $('#hd_location3')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        // Uncomment line below to show alert on each Location Changed event
        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
  });
 
  
   $('#hd_location3').change(function() {
    var loc = $('#hd_location3').val();
    $('#location3').html(loc);
  });
 
</script>
@endpush