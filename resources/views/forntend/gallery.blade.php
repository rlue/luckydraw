@extends('layouts.master')
@push('styles')
 
@endpush
@section('content')
<main role="main" class="container">
  <div class="row evenHeadText">
    <div class="col-md-4 galleryText">
      <h3>Tiger Street Football</h3>
      <p><b>&lsquo;&lsquo;</b>Tiger Street Football is our commitment to bring raw football talents in Myanmar and provide them with opportunities to Shine.
Welcome To Tiger Street Football Collection
<b>&rsquo;&rsquo;</b></p>
    </div>
    <div class="col-md-8">
      <div class="row">
        @foreach($gallery as $g)
          <div class="col-sm-4 gallery_show">
          <div class="text-center ">
          <a href="{{route('gallery_detial',$g->id)}}">
            <div class="imageText">
              <h5>{{$g->title}}</h5>
              
            </div>
            <div class="galleryImg">
             
                <img src="{{$g->getMediaUrl()}}" class="img-fluid" alt="Responsive image" width="100%" alt="...">
            </div> 
            </a> 
            </div>
          </div>
        @endforeach
        {{ $gallery->links() }}
      </div>
    </div>
  </div>
   
</main>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function($) { //noconflict wrapper
    var heights = $(".imageText p").map(function() {
        return $(this).height();
    }).get(),

    maxHeight = Math.max.apply(null, heights);

    $(".imageText p").height(maxHeight);
  });

</script>

@endpush
