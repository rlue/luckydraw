<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Tiger</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="gataomo-icon" href="{{ asset('images/logo.png') }}">
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
    <style type="text/css">
    	html,body,.container,.col-md-12.prom{
    		height: 100%;
  			margin: 0;
    	}
    	.col-md-12.prom {
		    text-align: center;
		    vertical-align: middle;
		}
		.textContent>p>strong{
		    font-size: 25px;
		    font-weight: bold;
		}
		.row.align-self-center.w-100 {
		    text-align: center;
		}
		.proContent{
			width: 100%;
		}
		.container{
			width:100% !important;
			max-width: 100% !important;
			margin:0px !important;
		}
    </style>
</head>
<body>
	<main role="main" class="container row align-self-center w-100 confrimContent">
		<div class="proContent d-flex h-100">
		    <div class="align-self-center w-100">
		        <div class="col-md-5 mx-auto promText_container" style="margin:auto;">           
	                <div class="textContent">
	                	<h2 class="lead "><strong>Tell us your age to continue</strong></h2>
	              <p>
Tiger Beer is committed to responsible drinking.Which is why we need to ensure that you are above the legal drinking age. And that you are legally permitted to view this site in the location you are in. This content is intended for those of legal drinking age, please do not share or forward to anyone underage.</p>
	                </div>
		            <div class="buttonContent">
		            <form action="{{url('/confirm_yes')}}" method="post">
		           @csrf
		            <div>
		            <input type="number" class="inputs"  name="request_day" placeholder="DD" min="1" max="31" maxlength="2" pattern="[0-9]*" value="{{old('request_day')}}" >
		            <input type="number" class="inputs" name="request_month" placeholder="MM" min="1" max="12" maxlength="2" pattern="[0-9]*" value="{{old('request_month')}}">
		            <input type="number" class="inputs" name="request_year" placeholder="YYYY"  maxlength="4" pattern="[0-9]*" value="{{old('request_year')}}"> 
		            </div>
		            @if(isset($message))
		            <span style="color:#ed8b00;font-weight: bold;font-size: 15px;line-height: 35px;">{{$message}}</span>
		            @endif
		           	<div class="form-group">
				        <button class="btn btn-tiger " id="btn_yes">UNCAGE</button>
				        
				     </div>
				     </form>
				    </div>
		        </div>
		    </div>
		</div>
	</main>
	<script type="text/javascript">
		$(document).ready(function() {
			

			$(".inputs").keyup(function () {
		    if (this.value.length == this.maxLength) {
		      var $next = $(this).next('.inputs');
		      if ($next.length)
		          $(this).next('.inputs').focus();
		      else
		          $(this).blur();
		    }
			});
			
		});

	</script>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148903826-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-148903826-1');
</script>
</body>
</html>




