@extends('layouts.master1')
@push('styles')
 <!-- CSS reset -->
<!-- superWheel -->
	<link rel="stylesheet" href="{{ asset('css/global.css') }}"> <!-- Resource style -->
	<link rel="stylesheet" href="{{ asset('css/spectrum.min.css') }}"> 
@endpush
@section('content')

<main role="main" class="container">
    <div class="row">

      <div class="col-md-6 galleryText">
      <h3>Welcome!</h3>
      <p><b>&lsquo;&lsquo;</b>Spin the wheel and get a chance to win the prizes. You can come and claim the prizes at Grand Final event.<b>&rsquo;&rsquo;</b></p>
      <div>
      	
      	@if(Auth::guard('customer')->check())
			@if(!$winning->isEmpty())
			<h3>You got</h3>
				@foreach($winning as $w)
					<p>{{$w->luckyitem->name}}</p>
					<p>{{$w->luckyitem->message}}</p>
				@endforeach
			@else
			<h3>Good  Luck</h3>
			@endif
		@endif
      </div>
    </div>
      <div class="col-sm-6"><main class="cd-main-content text-center">
		
		<div class="wheel-standard"></div>
		<div class="countClick">
		@if(Auth::guard('customer')->check())
			<p>{{$clickCount}} Left</p>
		@endif
		</div>
		<button type="button" class="btn btn-tiger wheel-standard-spin-button">Spin</button>
		
		
		
		
		
		
	</main> </div>

    </div>
</main>
@endsection
@push('scripts')
<script src="{{ asset('js/svg.js') }}"></script>
<script src="{{ asset('js/svg.filter.js') }}"></script>
<script src="{{ asset('js/layout.js') }}"></script> <!-- superWheel -->

<script src="{{ asset('js/jquery-3.4.0.min.js') }}" data-type="admin"></script>
   <script src="{{ asset('js/spectrum.min.js') }}" data-type="admin"></script>
    <script src="{{ asset('js/jszip.min.js') }}" data-type="admin"></script>
    <script src="{{ asset('js/jszip-utils.min.js') }}" data-type="admin"></script>
    <script src="{{ asset('js/filesaver.js') }}" data-type="admin"></script>
    <script src="{{ asset('js/params.js') }}" data-type="admin"></script>

@endpush
