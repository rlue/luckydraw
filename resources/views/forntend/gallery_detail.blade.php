@extends('layouts.master1')
@push('styles')
  <link href="https://vjs.zencdn.net/7.6.0/video-js.css" rel="stylesheet">

  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
  <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
@endpush
@section('content')
<main role="main" class="container" style="padding-top:20px;">
  <div class="row">
  	<div class="col-sm-12">
  		
  	</div>
    <div class="col-md-4 galleryText">
      <h3>{{$gallery->title}}</h3>
      <p>{{$gallery->description}}</p>
    </div>
    <div class="col-md-8">

    @if($gallery->getVideoUrl())
  		<video width="100%" autoplay loop muted playsinline controls>
          <source src="{{$gallery->getVideoUrl()}}" type="video/mp4;codecs='avc1.42E01E, mp4a.40.2'">
          <source src="{{$gallery->getVideoUrl()}}" type="video/ogg">
          Your browser does not support HTML5 video.
      </video>
      @else
      <img class="img-fluid" src="{{$gallery->getMediaUrl()}}" />
  		@endif

    </div>
  </div>
   
</main>
@endsection
@push('scripts')
<script src='https://vjs.zencdn.net/7.6.0/video.js'></script>
<script type=”text/javascript” src=”//cdn.jsdelivr.net/afterglow/latest/afterglow.min.js”></script>
@endpush