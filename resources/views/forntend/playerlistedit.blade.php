@extends('layouts.master')
@section('content')
<main role="main" class="container">
	<h3>Player Edit</h3>
	<hr>
	<div class="row">
		<div class="col-md-6 offset-md-2">
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
					    @foreach ($errors->all() as $error)
					      <li>{{ $error }}</li>
					    @endforeach
					</ul>
				</div><br/>
		    @endif
			<form method="POST" action="{{route('playerupdate',$player->id)}}" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
		        <div class="form-group row">
		            <label for="username" class="col-sm-4 col-form-label">Player Name<b class="highlight">***</b></label>
		            <div class="col-sm-8">
		                <input type="text" class="form-control" id="name" name="name" value="{{$player->name}}" required>
		            </div>
		        </div>
		        <div class="form-group row">
		            <label for="email" class="col-sm-4 col-form-label">Email<b class="highlight">*</b></label>
		            <div class="col-sm-8">
		                <input type="email" class="form-control" id="email" name="email" value="{{$player->email}}">
		            </div>
		        </div>
		        <div class="form-group row">
		            <label for="phone_no" class="col-sm-4 col-form-label">Phone No<b class="highlight">*</b></label>
		            <div class="col-sm-8">
		                <input type="text" class="form-control" id="phone_no" name="phone_no"value="{{$player->phone_no}}" required>
		            </div>
		        </div>
		        <div class="form-group row">
		            <label for="address" class="col-sm-4 col-form-label">NRC No<b class="highlight">*</b></label>
		            <div class="col-sm-8">
		                <input type="text" class="form-control" id="nrc_no" name="nrc_no"value="{{$player->nrc_no}}" required>
		            </div>
		        </div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label"></label>
					<div class="col-sm-8">
				    	<a href="{{route('playerlist')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
				   		<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
					</div>
				</div>
			</form>
		</div>	
	</div>
</main>

@endsection