@extends('layouts.master1')
@section('content')
<main role="main" class="container" style="margin-top: 5px;">
	<div class="row" style="margin:0px;">
		<!-- <div class="col-sm-12" > -->
			<div class="LoginContent">
				<figure><img src="{{ asset('images/tg_logo.png') }}"></figure>
				<div class="row headText" style="margin:20px 0px auto;">
					<h3>Register</h3>
					<p>Create your account</p>
					<div class="row" style="float: right;">
						<div class="col-xs-4" >
					      @if(Session::has('message'))
					        <div class="alert alert-success alert-dismissible fade show" role="alert">
					          {{ Session::get('message') }}
					          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					            <span aria-hidden="true">&times;</span>
					          </button>
					        </div>
					      @endif
					    </div>
					</div>
				</div>
				<div class="row" style="margin:0px;">
					<div class="col-md-12">	
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
								    @foreach ($errors->all() as $error)
								      <li>{{ $error }}</li>
								    @endforeach
								</ul>
							</div><br/>
					    @endif
					</div>
						<form class="form_login" action="{{ route('userReg.store') }}" method="post" >
							@csrf
							<input type="hidden" name="social_id" value="{{ isset($social_id) ? $social_id : ''}}" >
					        <div class="form-group row input">
					            <label for="username" class="col-sm-4 col-3 col-form-label">Username</label>
					            <div class="col-sm-8 col-9">
					                <input type="text" class="form-control" id="username" name="username" value="{{ isset($username) ? $username : ''}}" required>
					            </div>
					        </div>
					        <div class="form-group row input">
					            <label for="email" class="col-sm-4 col-3 col-form-label">Email</label>
					            <div class="col-sm-8 col-9">
					                <input type="email" class="form-control" id="email" name="email" value="{{ isset($email) ? $email : ''}}" required>
					            </div>
					        </div>
					        <div class="form-group row input">
					            <label for="password" class="col-sm-4 col-3 col-form-label">Password</label>
					            <div class="col-sm-8 col-9">
					                <input type="password" class="form-control" id="password" name="password" required>
					            </div>
					        </div>
					        <div class="form-group row input">
					            <label for="password_confirm" class="col-sm-6 col-4 col-form-label">Confirm Password</label>
					            <div class="col-sm-6 col-8">
					                <input type="password" class="form-control" id="password_confirm" name="password_confirm" required>
					            </div>
					        </div>
					         <div class="form-group row input">
					            <label for="phone_no" class="col-sm-4 col-3  col-form-label">Phone No</label>
					            <div class="col-sm-8 col-9">
					                <input type="text" class="form-control" id="phone_no" name="phone_no" value="{{old('phone_no')}}" required>
					            </div>
					        </div>
					        <div class="form-group row input">
					            <label for="address" class="col-sm-4 col-3  col-form-label">Address</label>
					            <div class="col-sm-8 col-9">
					            	<input type="text" class="form-control" id="Address" name="address" value="{{old('address')}}">
					            </div>
					        </div>
					        <div class="form-group row chk_acc_content">
					        	<input type="checkbox" name="chk_accept" required class="col-1">
					        	 <div class="col-11"><a href="{{route('term_condition')}}" style="color:#fff;" target="_black">I Accept terms and conditions & privacy policy</a></div>
					        </div>
				        	<div class="form-group row btnLogin" style="float:right; margin-top:15px;">
				        	 	<button type="submit" class="btn btn-tiger ">Sign Up</button>
				        	</div>
						</form>
					<div class="btnSocialContent">
						<h5>Login with Social Media</h5>
		                <a  href="{{route('fb_login')}}" class="btn btn-primary "><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a>
		            </div>	
				</div>
				</div>
			</div>
		<!-- </div> -->
	</div>
	<style type="text/css">
		.btnSocialContent {
    		margin-bottom: 20px;
		}
	
	</style>
</main>
@endsection