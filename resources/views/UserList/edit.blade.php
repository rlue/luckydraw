@extends('layouts.admin')
@section('content')
<div class="page-header">
    <div class="col-xs-6">
      <h1 class="page-title">Users</h1>
      <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="{{route('dashboard')}}">Home</a></li>
        <li class="active">Edit</li>
      </ol>     
    </div>
    <div class="col-xs-4">
      @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ Session::get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
    </div>
  </div>
  <div class="clearfix"></div>
   <div class="page-content" >
    <div class="panel">
      <div class="panel-body container-fluid" id="tbl-user">
        <div class="success-alert-area"> </div>
        <div class="clearfix sp-margin-sm"></div>
       <main role="main" class="container">
	<h3>User Registration Edit</h3>
	<hr>
	<div class="row">
		<div class="col-md-6 offset-md-2">
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
					    @foreach ($errors->all() as $error)
					      <li>{{ $error }}</li>
					    @endforeach
					</ul>
				</div><br/>
		    @endif
			<form method="POST" action="{{route('userlist.update',$user->id)}}"  >
                @method('PATCH')
		        <div class="form-group row">
		            <label for="username" class="col-sm-4 col-form-label">Username<b class="highlight">***</b></label>
		            <div class="col-sm-8">
		                <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{$user->username}}" required>
		            </div>
		        </div>
		        <div class="form-group row">
		            <label for="email" class="col-sm-4 col-form-label">Email<b class="highlight">***</b></label>
		            <div class="col-sm-8">
		                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$user->email}}" readonly>
		            </div>
		        </div>
		      
		        <div class="form-group row">
		            <label for="phone_no" class="col-sm-4 col-form-label">Phone No<b class="highlight">***</b></label>
		            <div class="col-sm-8">
		                <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Phone No" value="{{$user->phone_no}}" required>
		            </div>
		        </div>
		        <div class="form-group row">
		            <label for="address" class="col-sm-4 col-form-label">Address<b class="highlight">***</b></label>
		            <div class="col-sm-8">
		               <textarea class="form-control" rows="5" id="Address" name="address">{{$user->address}}</textarea>
		            </div>
		        </div>
		        <form action="{{ route('users.store', $user->id) }}">
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                     <div class="form-group row">
                     	<label class="col-sm-4 col-form-label"></label>
                     	<div class="col-sm-8">
	                        <a href="{{route('userlist.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
	                       <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
	                    </div>
                    </div>
                </form>
			</form>
		</div>	
	</div>
</main>
      </div>
    </div>
  </div>

@endsection