<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property ="og:video:type" content="application/x-shockwate-flash">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Tiger</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/navbar-top-fixed.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('remark/global/fonts/font-awesome/font-awesome.min.css?v2.1.0') }}">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="gataomo-icon" href="{{ asset('images/logo.png') }}">
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">
    <!-- add Map js -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
    
     <!-- add Map js -->
    
    <script>window.jQuery||document.write('<script src="{{ asset("js/jquery-slim.min.js") }}"><\/script>')</script>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TJNLMWD');</script>
<!-- End Google Tag Manager -->
<style type="text/css">
  nav.navbar.navbar-expand-md.navbar-dark.fixed-top.bg-dark {
    background: #11163238!important;
    color:#f6933f !important;
}
</style>
@stack('styles')
  </head>
  <body id="bdContent">
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJNLMWD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark ">
      <a class="navbar-brand" href="/home"><img src="{{ asset('images/banner.png') }}" class="logo_img"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
          
            <!-- <li class="nav-item {{ Route::currentRouteName() == 'footballTeamReg.index' ? 'active' : '' }}">
              <a class="nav-link " href="{{route('footballTeamReg.index')}}"><strong>Team Register</strong></a>
            </li> -->
          

          <li class="nav-item {{ Route::currentRouteName() == 'gallery' ? 'active' : '' }}">
            <a class="nav-link" href="{{route('gallery')}}"><strong>Gallery</strong></a>
          </li>
         <!--  <li class="nav-item {{ Route::currentRouteName() == 'promotions' ? 'active' : '' }}">
            <a class="nav-link" href="{{route('promotions')}}"><strong>Promotion</strong></a>
          </li> -->
          <li class="nav-item {{ Route::currentRouteName() == 'eventlist' ? 'active' : '' }}">
            <a class="nav-link " href="{{route('eventlist')}}"><strong>Event</strong></a>
          </li>
          
          @if(Auth::guard('customer')->check())
            <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="avatar avatar-online">{{Auth::guard('customer')->user()->username}}</span></a>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{route('playerlist')}}"><i class="ft-mail"></i>Player List</a>
<a class="dropdown-item" href="{{route('teamedit')}}"><i class="ft-mail"></i>Team Name Edit</a>
              <a class="dropdown-item" href="{{route('customer_logout')}}">Logout</a>
              </div>
            </li>
            @else
            <li class="nav-item">
              <a class="nav-link " href="{{route('customer_login')}}"><strong>Login</strong></a>
              </li>
              <li class="nav-item">

              <a class="nav-link " href="{{ url('userReg')}}"><strong>Register</strong></a>
              </li>
            @endif
          
        </ul>
      </div>
     
    </nav>
    <div class="main-content">
        <div class="main-content-inner">
          @yield('content')
        </div>
        <input type="hidden" id="hd_session_value" value="{{!empty(Session::get('CONFIRM')) ? Session::get('CONFIRM') : ''}}">
    </div>


<footer class="main_footer">
  <div class="container">
  <div class="row">
  <div class="col-sm-12">
    <div class="term_cond">
      <a href="#">Privacy Policy</a> |
      <a href="{{route('term_condition')}}">Terms & Conditions</a>
    </div>
  </div>
  <div class="col-sm-9">
      <div class="copyRight float-left">Tiger Beer Myanmar  ©  CopyRight 2019, All Right Reserved.</div>
    </div>
    <div class="col-sm-3">
      <div class="footer_contact float-right">
       
        Follow us on
         <a href="https://www.facebook.com/tigerbeerMM/" target="_black" class="fb_footer" ><img src="{{ asset('images/fb_footer.png') }}" /></a>
       
        
     
      </div>
    </div>
    
    
  </div>
  </div>
      
      
    </footer> 
   

  @stack('scripts')   
  <script type="text/javascript">
    $(document).ready(function() {
      var baseUrl = '{!! URL::to("/") !!}';
      var sess_value = $('#hd_session_value').val();
      if(sess_value != 'YES'){
        window.location.href = "{{URL::to('/')}}";
      }
    });
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148903826-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-148903826-1');
</script>

  </body>
</html>
