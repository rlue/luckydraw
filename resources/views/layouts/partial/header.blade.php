  <link rel="gataomo-icon" href="{{ asset('images/logo.png') }}">
  <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('remark/global/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/css/bootstrap-extend.min.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/base/assets/css/site.css') }}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/animsition/animsition.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/asscrollable/asScrollable.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/switchery/switchery.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/intro-js/introjs.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/slidepanel/slidePanel.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/flag-icon-css/flag-icon.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/waves/waves.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/select2/select2.css') }}" >
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/bootstrap-toggle/bootstrap-toggle.min.css') }}" >
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/jt-timepicker/jquery-timepicker.css') }}"></link>
  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('remark/global/fonts/material-design/material-design.min.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/fonts/brand-icons/brand-icons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/fonts/font-awesome/font-awesome.min.css?v2.1.0') }}">
  
  <!-- Datatables -->
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/datatables-bootstrap/dataTables.bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/datatables-responsive/dataTables.responsive.css') }}">
  <!-- Datepicker-->
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/summernote/summernote.css') }}">
  <link rel="stylesheet" href="{{ asset('remark/global/vendor/toastr/toastr.css') }}">

  <link rel="stylesheet" href="{{ asset('remark/global/vendor/tel-input/css/intlTelInput.css') }}">
  <!-- <link href="{{ asset('css/custom.css') }}" rel="stylesheet"> -->

  
  <!-- Custom CSS -->
  

  @stack('styles')
  <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="{{ asset('remark/global/vendor/jquery/jquery.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/modernizr/modernizr.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/breakpoints/breakpoints.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/summernote/summernote.js') }}"></script>

  <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">

  <script>
   
  Breakpoints();
  </script>