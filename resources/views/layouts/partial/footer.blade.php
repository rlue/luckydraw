 <footer class="site-footer">
    <div class="site-footer-legal">© 
    <?= date('Y', time()) . ' - ' . date('Y', strtotime('+1 year')); ?>
    </div>
    <div class="site-footer-right">
     <!--  Developed by <i class="red-600 icon zmdi zmdi-code"></i> <a href="https://itechmm.com">iTech Myanmar Pte Ltd</a> -->
    </div>
  </footer>
  <!-- Core  -->
 
  <script src="{{ asset('remark/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
  <!-- add Map js -->
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
  <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
  
     <!-- add Map js -->
    <script src="{{ asset('remark/global/vendor/popper-js/umd/popper.min.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/animsition/animsition.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/asscroll/jquery-asScroll.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/asscrollable/jquery.asScrollable.all.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/waves/waves.js') }}"></script>
  <!-- Plugins -->
  <script src="{{ asset('remark/global/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/chartist-js/chartist.min.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/select2/select2.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/jt-timepicker/jquery.timepicker.min.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/toastr/toastr.js') }}"></script>
   
  <!-- Scripts -->
  <script src="{{ asset('remark/global/js/core.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/site.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/sections/menu.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/sections/menubar.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/sections/gridmenu.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/sections/sidebar.js') }}"></script>
  <script src="{{ asset('remark/global/js/configs/config-colors.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/configs/config-tour.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/asscrollable.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/animsition.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/slidepanel.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/switchery.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/tabs.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/jquery-placeholder.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/input-group-file.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/formatter-js.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/summernote.js') }}"></script>
  <!-- Datatables -->
  <script src="{{ asset('remark/global/vendor/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
  <!-- Penel -->
  <script src="{{ asset('remark/global/js/components/panel.js') }}"></script>

 <!--  <script src="{{ asset('remark/global/vendor/jquery-validator/jquery.validate.js') }}"></script> -->

  <script src="{{ asset('remark/global/vendor/tel-input/js/intlTelInput.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/tel-input/js/utils.js') }}"></script>
  <!-- Custom Js -->
  
  
  
  <script> 
    (function(document, window, $) {
      'use strict';
      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    window.baseUrl = '{!! URL::to("/") !!}';
  </script>