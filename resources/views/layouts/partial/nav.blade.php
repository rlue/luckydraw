   
    <div class="site-menubar">
      <div class="site-menubar-body">
        <div>
          <div>
            <ul class="site-menu" data-plugin="menu">
              <li class="site-menu-category">General</li>
              <li class="site-menu-item active">
                <a class="animsition-link" href="{{route('dashboard')}}">
                 <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                  <span class="site-menu-title">Dashboard</span>
                </a>
              </li>              
              <li class="site-menu-category " >Elements</li>
              <li class="site-menu-item has-sub {{ Request::is('categories') ? 'open' : '' }}">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                  <span class="site-menu-title">Data Manage </span>
                  <span class="site-menu-arrow"></span>
                </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item {{ Request::is('users') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{route('users.index')}}">
                      <span class="site-menu-title">User</span>
                    </a>
                  </li>
                  <li class="site-menu-item {{ Request::is('userlist') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{route('userlist.index')}}">
                      <span class="site-menu-title">Customer List</span>
                    </a>
                  </li>
                  <li class="site-menu-item {{ Request::is('winning') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{route('winning.index')}}">
                      <span class="site-menu-title">Winning List</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="site-menu-item has-sub {{ Request::is('luckyitems') ? 'open' : '' }}">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                  <span class="site-menu-title">LuckyDraw Manage </span>
                  <span class="site-menu-arrow"></span>
                </a>
                <ul class="site-menu-sub">                  
                  <li class="site-menu-item {{ Request::is('luckyitems') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{route('luckyitems.index')}}">
                      <span class="site-menu-title">LuckyItem</span>
                    </a>
                  </li>
                  <li class="site-menu-item {{ Request::is('settings') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{url('/backend/settings')}}">
                      <span class="site-menu-title">Setting</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="site-menu-item has-sub {{ Request::is('galleries') ? 'open' : '' }}">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                    <span class="site-menu-title">Gallery Manage </span>
                    <span class="site-menu-arrow"></span>
                  </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item {{ Request::is('galleries') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{route('galleries.index')}}">
                      <span class="site-menu-title">Gallery</span>
                    </a>
                  </li>
                </ul>
              </li>
              <!-- <li class="site-menu-item has-sub {{ Request::is('videos') ? 'open' : '' }}">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                    <span class="site-menu-title">Video Manage </span>
                    <span class="site-menu-arrow"></span>
                  </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item {{ Request::is('videos') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{route('videos.index')}}">
                      <span class="site-menu-title">Video</span>
                    </a>
                  </li>
                </ul>
              </li> -->
               <li class="site-menu-item has-sub {{ Request::is('sliders') ? 'open' : '' }}">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                    <span class="site-menu-title">Slider Manage </span>
                    <span class="site-menu-arrow"></span>
                  </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item {{ Request::is('sliders') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{route('sliders.index')}}">
                      <span class="site-menu-title">Slider</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="site-menu-item has-sub {{Request::is('map') ? 'open' : ''}}">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                  <span class="site-menu-title">Event Manage </span>
                  <span class="site-menu-arrow"></span>
                </a>
                <ul class="site-menu-sub">                
                  <li class="site-menu-item {{Request::is('map') ? 'active' : ''}}">
                    <a class="animsition-link" href="{{route('map.index')}}">
                      <span class="site-menu-title">Event List</span>
                    </a>
                  </li>
                </ul>
              </li>
               <li class="site-menu-item has-sub {{Request::is('teamList') ? 'open' : ''}}">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                  <span class="site-menu-title">Team Manage </span>
                  <span class="site-menu-arrow"></span>
                </a>
                <ul class="site-menu-sub">                
                  <li class="site-menu-item {{Request::is('teamList') ? 'active' : ''}}">
                    <a class="animsition-link" href="{{route('teamList.index')}}">
                      <span class="site-menu-title">Team List</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="site-menu-item has-sub {{ Request::is('matches') ? 'open' : '' }}">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                    <span class="site-menu-title">Matches </span>
                    <span class="site-menu-arrow"></span>
                  </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item {{ Request::is('matches') ? 'active' : '' }}">
                    <a class="animsition-link" href="{{route('matches.index')}}">
                      <span class="site-menu-title">Matches</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
            </div>
        </div>
      </div>
    
      <div class="site-menubar-footer">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
          data-original-title="Settings">
          <span class="icon md-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
          <span class="icon md-eye-off" aria-hidden="true"></span>
        </a>
        <a href="{{route('logout')}}" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
          <span class="icon md-power" aria-hidden="true"></span>
        </a>
      </div>
      </div>    
      <div class="site-gridmenu">
      <div>
        <div>
          <ul>
            <li>
              <a href="apps/mailbox/mailbox.html">
                <i class="icon wb-envelope"></i>
                <span>Mailbox</span>
              </a>
            </li>
            <li>
              <a href="apps/calendar/calendar.html">
                <i class="icon wb-calendar"></i>
                <span>Calendar</span>
              </a>
            </li>
            <li>
              <a href="apps/contacts/contacts.html">
                <i class="icon wb-user"></i>
                <span>Contacts</span>
              </a>
            </li>
            <li>
              <a href="apps/media/overview.html">
                <i class="icon wb-camera"></i>
                <span>Media</span>
              </a>
            </li>
            <li>
              <a href="apps/documents/categories.html">
                <i class="icon wb-order"></i>
                <span>Documents</span>
              </a>
            </li>
            <li>
              <a href="apps/projects/projects.html">
                <i class="icon wb-image"></i>
                <span>Project</span>
              </a>
            </li>
            <li>
              <a href="apps/forum/forum.html">
                <i class="icon wb-chat-group"></i>
                <span>Forum</span>
              </a>
            </li>
            <li>
              <a href="index.html">
                <i class="icon wb-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
