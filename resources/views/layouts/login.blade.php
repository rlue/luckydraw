
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login | Tiger Beer</title>
    
   @include('layouts.partial.header')
        <link rel="stylesheet" href="{{ asset('css/login.min.css') }}">
    
   
  </head>
  <body class="animsition page-login layout-full page-dark" >
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <!-- Page -->
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out" id="login">
      <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
        <div class="brand">
          <img class="brand-img" src="../../images/logo.png" alt="...">
          <h2 class="brand-text">Tiger Football Street</h2>
        </div>
        
        <form method="post" action="{{ route('login') }}">
        @csrf
          <div class="form-group">
            <label class="sr-only" for="inputEmail">Email</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
          </div>
          <div class="form-group">
            <label class="sr-only" for="inputPassword">Password</label>
             <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
          </div>
          <div class="form-group clearfix">
            <!-- <div class="checkbox-custom checkbox-inline checkbox-primary float-left">
              <input type="checkbox" id="inputCheckbox" name="remember">
              <label for="inputCheckbox">Remember me</label>
            </div> -->
            
          </div>
          <button type="submit" class="btn btn-primary btn-block">Sign in</button>
        </form>
        

        <footer class="page-copyright page-copyright-inverse">
          <!-- <p>WEBSITE BY Creation iTech Myanmar</p> -->
          <p>© <?= date('Y', time()) . ' - ' . date('Y', strtotime('+1 year')); ?>. All RIGHT RESERVED.</p>
          
        </footer>
      </div>
    </div>
    <!-- End Page -->


   <footer class="site-footer">
    <div class="site-footer-legal">© 
    <?= date('Y', time()) . ' - ' . date('Y', strtotime('+1 year')); ?>
    </div>
    <div class="site-footer-right">
     <!--  Developed by <i class="red-600 icon zmdi zmdi-code"></i> <a href="http://techcode.com.sg">iTech Myanmar Pte Ltd</a> -->
    </div>
  </footer>
  <!-- Core  -->
   <script src="{{ asset('remark/global/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
    
    <script src="{{ asset('remark/global/vendor/popper-js/umd/popper.min.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/animsition/animsition.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/asscroll/jquery-asScroll.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/asscrollable/jquery.asScrollable.all.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/waves/waves.js') }}"></script>
  <!-- Plugins -->
  <script src="{{ asset('remark/global/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/chartist-js/chartist.min.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/select2/select2.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/jt-timepicker/jquery.timepicker.min.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/toastr/toastr.js') }}"></script>
   
  <!-- Scripts -->
  <script src="{{ asset('remark/global/js/core.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/site.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/sections/menu.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/sections/menubar.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/sections/gridmenu.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/sections/sidebar.js') }}"></script>
  <script src="{{ asset('remark/global/js/configs/config-colors.js') }}"></script>
  <script src="{{ asset('remark/base/assets/js/configs/config-tour.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/asscrollable.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/animsition.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/slidepanel.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/switchery.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/tabs.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/jquery-placeholder.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/input-group-file.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/formatter-js.js') }}"></script>
  <script src="{{ asset('remark/global/js/components/summernote.js') }}"></script>
  <!-- Datatables -->
  <script src="{{ asset('remark/global/vendor/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
  <!-- Penel -->
  <script src="{{ asset('remark/global/js/components/panel.js') }}"></script>

 <!--  <script src="{{ asset('remark/global/vendor/jquery-validator/jquery.validate.js') }}"></script> -->

  <script src="{{ asset('remark/global/vendor/tel-input/js/intlTelInput.js') }}"></script>
  <script src="{{ asset('remark/global/vendor/tel-input/js/utils.js') }}"></script>
  <!-- Custom Js -->
  <!-- <script src="{{ asset('js/custom.js') }}"></script>-->

  
  <script> 
    (function(document, window, $) {
      'use strict';
      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    window.baseUrl = '{!! URL::to("/") !!}';
  </script>
    
  </body>
</html>
