@extends('layouts.master1')
@section('content')
<main role="main" class="container bootstrap snippet">
	<div class="row">
		<h3>Football Team Registration</h3>		
		<div class="row" style="float: right;">
			<div class="col-xs-4" >
		      @if(Session::has('message'))
		        <div class="alert alert-success alert-dismissible fade show" role="alert">
		          {{ Session::get('message') }}
		          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		      @endif
		    </div>
		</div>
		
	</div>
	<hr>
	<div class="row">
		<div class="col-md-7 offset-md-2" style="float:left;">	
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
					    @foreach ($errors->all() as $error)
					      <li>{{ $error }}</li>
					    @endforeach
					</ul>
				</div><br/>
		    @endif

		</div>
		<div class="col-md-7 offset-md-2 errorContent" style="float:left;">

		</div>
	</div>
	<div class="col-md-8 offset-md-2 team_reg">	
	@if($check)
		<h4>You have been created Team </h4>
		<a href="{{route('playerlist')}}" class="btn btn-tiger">View Team</a>
	@else
		<form action="{{ route('footballTeamReg.store') }}" method="post" id="teamRegForm">
			@csrf
			<div class="row">
				<div class="col-md-6">
					<div class="form-group row">
		            	<label for="username" class="col-sm-4 col-form-label">Team Name </label>
			            <div class="col-sm-7">
			                <input type="text" class="form-control" id="teamname" name="teamname" placeholder="Team Name" value="{{old('teamname')}}" >
			            </div>
		        	</div>
				</div>	
				<div class="col-md-6">
			        <div class="form-group row">
			            <label for="region" class="col-sm-4 col-form-label">Region </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="teamname" name="region" placeholder="Region" value="{{old('region')}}" >
			            </div>
			        </div>
			    </div>
			</div>
			<br/>
		    <ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
					<a class="nav-link  active" href="#captain" role="tab" data-toggle="tab" aria-selected="true">Captain</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#player1" role="tab" data-toggle="tab">Player 1</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#player2" role="tab" data-toggle="tab">Player 2</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#player3" role="tab" data-toggle="tab">Player 3</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#player4" role="tab" data-toggle="tab">Player 4</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#player5" role="tab" data-toggle="tab">Player 5</a>
				</li>
			</ul>
			<div class="tab-content" style="margin-top: 25px;">
				<div role="tabpanel" class="tab-pane active" id="captain">
					<div class="form-group row">
			            <label for="name" class="col-sm-4 col-form-label">Name</label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="name" name="captainName" placeholder="Username" value="{{old('captainName')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="email" class="col-sm-4 col-form-label">Email</label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="email" name="captainemail" placeholder="Email" value="{{old('captainemail')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="phone_no" class="col-sm-4 col-form-label">Phone No</label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="phone_no" name="captainphone_no" placeholder="Phone No" value="{{old('captainphone_no')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="nrc_no" class="col-sm-4 col-form-label">NRC No</label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="nrc_no" name="captainnrc_no" placeholder="NRC No" value="{{old('captainnrc_no')}}" >
			            </div>
			    	</div>
			    	<div class="form-group row">
		            	<div class="col-sm-10 offset-sm-4">
			    	 		<a class="btn btn-tiger btnNext" style="color: #fff;">Next</a>
			    	 	</div>
			    	</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="player1">
			        <div class="form-group row">
			            <label for="username" class="col-sm-4 col-form-label">Name </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="name" name="player1Name" placeholder="Username" value="{{old('player1Name')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="email" class="col-sm-4 col-form-label">Email </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="email" name="player1email" placeholder="Email" value="{{old('player1email')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="phone_no" class="col-sm-4 col-form-label">Phone No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="phone_no" name="player1phone_no" placeholder="Phone No" value="{{old('player1phone_no')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="nrc_no" class="col-sm-4 col-form-label">NRC No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="nrc_no" name="player1nrc_no" placeholder="NRC No" value="{{old('player1nrc_no')}}">
			            </div>
			    	</div>
			    	<div class="form-group row">
		            	<div class="col-sm-10 offset-sm-4">
			    	 		<a class="btn btn-tiger btnNext" style="color: #fff;">Next</a>
			    	 		<a class="btn btn-tiger btnPrevious" style="color: #fff;">Previous</a>
			    	 	</div>
			    	</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="player2">
					<div class="form-group row">
			            <label for="username" class="col-sm-4 col-form-label">Name </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="name" name="player2Name" placeholder="Username" value="{{old('player2Name')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="email" class="col-sm-4 col-form-label">Email </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="email" name="player2email" placeholder="Email" value="{{old('player2email')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="phone_no" class="col-sm-4 col-form-label">Phone No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="phone_no" name="player2phone_no" placeholder="Phone No" value="{{old('player2phone_no')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="nrc_no" class="col-sm-4 col-form-label">NRC No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="nrc_no" name="player2nrc_no" placeholder="NRC No" value="{{old('player2nrc_no')}}" >
			            </div>
			    	</div>
			    	<div class="form-group row">
		            	<div class="col-sm-10 offset-sm-4">
			    	 		<a class="btn btn-tiger btnNext" style="color: #fff;">Next</a>
			    	 		<a class="btn btn-tiger btnPrevious" style="color: #fff;">Previous</a>
			    	 	</div>
			    	</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="player3">
					<div class="form-group row">
			            <label for="username" class="col-sm-4 col-form-label">Name </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="name" name="player3Name" placeholder="Username" value="{{old('player3Name')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="email" class="col-sm-4 col-form-label">Email </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="email" name="player3email" placeholder="Email" value="{{old('player3email')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="phone_no" class="col-sm-4 col-form-label">Phone No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="phone_no" name="player3phone_no" placeholder="Phone No" value="{{old('player3phone_no')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="nrc_no" class="col-sm-4 col-form-label">NRC No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="nrc_no" name="player3nrc_no" placeholder="NRC No" value="{{old('player3nrc_no')}}">
			            </div>
			    	</div>
			    	<div class="form-group row">
		            	<div class="col-sm-10 offset-sm-4">
			    	 		<a class="btn btn-tiger btnNext" style="color: #fff;">Next</a>
			    	 		<a class="btn btn-tiger btnPrevious" style="color: #fff;">Previous</a>
			    	 	</div>
			    	</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="player4">
					<div class="form-group row">
			            <label for="username" class="col-sm-4 col-form-label">Name </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="name" name="player4Name" placeholder="Username" value="{{old('player4Name')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="email" class="col-sm-4 col-form-label">Email </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="email" name="player4email" placeholder="Email" value="{{old('player4email')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="phone_no" class="col-sm-4 col-form-label">Phone No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="phone_no" name="player4phone_no" placeholder="Phone No" value="{{old('player4phone_no')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="nrc_no" class="col-sm-4 col-form-label">NRC No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="nrc_no" name="player4nrc_no" placeholder="NRC No" value="{{old('player4nrc_no')}}">
			            </div>
			    	</div>
			    	<div class="form-group row">
		            	<div class="col-sm-10 offset-sm-4">
			    	 		<a class="btn btn-tiger btnNext" style="color: #fff;">Next</a>
			    	 		<a class="btn btn-tiger btnPrevious" style="color: #fff;">Previous</a>
			    	 	</div>
			    	</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="player5">
					<div class="form-group row">
			            <label for="username" class="col-sm-4 col-form-label">Name </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="name" name="player5Name" placeholder="Username" value="{{old('player5Name')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="email" class="col-sm-4 col-form-label">Email </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="email" name="player5email" placeholder="Email" value="{{old('player5email')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="phone_no" class="col-sm-4 col-form-label">Phone No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="phone_no" name="player5phone_no" placeholder="Phone No" value="{{old('player5phone_no')}}" >
			            </div>
			        </div>
			        <div class="form-group row">
			            <label for="nrc_no" class="col-sm-4 col-form-label">NRC No </label>
			            <div class="col-sm-8">
			                <input type="text" class="form-control" id="nrc_no" name="player5nrc_no" placeholder="NRC No" value="{{old('player5nrc_no')}}">
			            </div>
			    	</div>
			    	<div class="form-group row">
		            	<div class="col-sm-10 offset-sm-4">
			    	 		<a class="btn btn-tiger btnPrevious" style="color: #fff;">Previous</a>
			    	 		<button type="submit" class="btn btn-tiger" id="signup_team" style="color:#fff;">Sign Up</button>
			    	 	</div>
			    	</div>
				</div>


				
		    </div>
		</form>
	@endif	
	    
	</div>	
</main>
@endsection
@push('scripts')
<script>
	// 
	var errMsg = "";
	$('.nav-tabs li.nav-item').addClass('disabled');
    $('.nav-tabs li.nav-item').click(function(event){
        if ($(this).hasClass('disabled')) {
            return false;
        }
    });
	
	$('.btnNext').click(function() {
		$('.errorContent').empty();
		var validationMessage = '';
        $.each($(this).closest('.tab-pane').find('input[type="text"]'), function () {
            if ($(this).val() == '') 
            validationMessage += "Please fill " + $(this).closest('.form-group').find('label').html().replace('*', '') + "!\n<br/>";           
        });
        if (validationMessage != '')
            $('.errorContent').html('<div class="alert alert-danger">'+validationMessage+'</div>');
        else
        	$('.nav-tabs .active').parent().next('li').removeClass('disabled');
            $('.nav-tabs .active').parent().next('li').find('a').trigger('click');

  	});
  	$('.btnPrevious').click(function() {
  		$('.nav-tabs .active').parent().prev('li').removeClass('disabled');
    	$('.nav-tabs .active').parent().prev('li').find('a').trigger('click');
  	});
</script>
@endpush