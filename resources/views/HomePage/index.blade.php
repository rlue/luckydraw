@extends('layouts.master')
@section('content')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
            <?php $i = 0; ?>
            @foreach($slide as $s)
              @if($i == 0)
              
              <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>" class="active"></li>
              @else
          
              <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>"></li>
              @endif
             
              <?php $i++; ?>
             @endforeach
  </ol>
          <div class="carousel-inner">
            <?php $l = 0; ?>
            @foreach($slide as $s)
            @if($l == 0)
              
              <div class="carousel-item active">
                <img class="lastest_news img-fluid"  alt="Responsive image" src="{{$s->getMediaUrl()}}">
                <div class="carousel-caption ">
                <h3>{{$s->title}}</h3>
                <p><a href="{{$s->url}}" class="btn btn-tiger" >{{$s->btn_name}}</a></p>
              </div>
                  
              </div>
              
              @else
              
              <div class="carousel-item">
                <img class="lastest_news img-fluid" src="{{$s->getMediaUrl()}}">
                  <div class="carousel-caption ">
                      <h3>{{$s->title}}</h3>
                      <p><a href="{{$s->url}}" class="btn btn-tiger" >{{$s->btn_name}}</a></p>
              </div>
              </div>
              
              @endif
              <?php $l++; ?>
             @endforeach
              
            </div>
  
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div id="homePage">
<div class="container">

<div class="row">
<div class="col-sm-3"></div>
@foreach($grand as $m)

  <div class="col-sm-6 ">
  <h4 class="match_title match_show">{{$m->event->title}}</h4>
  <p>{{$m->title}}
  <span class="float-right">{{date('d-m-Y',strtotime($m->event_date))}}</span>
  </p>
    <table class="table">
    @foreach($m->matchdetail($m->id) as $list)
      <tr class="table-active">
        <td>{{$list->homeTeam}}</td>
        <td>{{$list->result}}</td>
        <td>{{$list->awayTeam}}</td>
        <td>{{$list->match_time}}</td>
      </tr>
    @endforeach
    </table>
  </div>
    @endforeach
    <div class="col-sm-3"></div>
</div>
<div class="row" >
@foreach($mandalay as $m)
  <div class="col-sm-6">
  <h4 class="match_title">{{$m->event->title}}</h4>
  <p>{{$m->title}}
  <span class="float-right">{{date('d-m-Y',strtotime($m->event_date))}}</span>
  </p>
    <table class="table">
    @foreach($m->matchdetail($m->id) as $list)
      <tr class="table-active">
        <td>{{$list->homeTeam}}</td>
        <td>{{$list->result}}</td>
        <td>{{$list->awayTeam}}</td>
        <td>{{$list->match_time}}</td>
      </tr>
    @endforeach
    </table>
  </div>
    @endforeach
    @foreach($yangon as $m)
  <div class="col-sm-6">
  <h4 class="match_title">{{$m->event->title}}</h4>
  <p>{{$m->title}}
  <span class="float-right">{{date('d-m-Y',strtotime($m->event_date))}}</span>
  </p>
    <table class="table">
    @foreach($m->matchdetail($m->id) as $list)
      <tr class="table-active">
        <td>{{$list->homeTeam}}</td>
        <td>{{$list->result}}</td>
        <td>{{$list->awayTeam}}</td>
        <td>{{$list->match_time}}</td>
      </tr>
    @endforeach
    </table>
  </div>
    @endforeach
  </div>
</div>
</div>
@endsection
@push('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      $('.carousel').carousel();
    });
  </script>
@endpush