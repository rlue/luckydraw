@extends('layouts.admin')
@section('content')
  <div class="page-header">
    <div class="col-xs-6">
      <h1 class="page-title">Team List</h1>
      <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="{{route('dashboard')}}">Home</a></li>
        <li class="active">Team List</li>
      </ol>     
    </div>
    <div class="col-xs-4">
      @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ Session::get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="page-content" >
    <div class="panel">
      <div class="panel-body container-fluid" id="tbl-user">
        <div class="success-alert-area"> </div>
        <div class="clearfix sp-margin-sm"></div>
        <table class="table table-hover dataTable table-striped width-full dtr-inline" id='userListTable' >
          <thead>
            <tr>
              <th>No</th>  
              <th>Team Name</th>
              <th>Region</th>
              <th class="action-col">Action</th>
            </tr>
          </thead>
          <tbody> 

          </tbody>
        </table> 
      </div>
    </div>
  </div>
@endsection
@push('scripts')
<script>
$(function() {
  var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
   this.tbl = $('#userListTable').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('teamList.getData') !!}',
      columns: [
        { data: 'id', name:'id'  },
        { data: 'name',name: 'name' },
        { data: 'region',name: 'region' },
        {data: 'action',name: 'action', orderable: false, searchable: false},
      ],
  });
});
</script>
@endpush