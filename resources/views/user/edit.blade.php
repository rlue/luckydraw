@extends('layouts.admin')

@section('content')
    

   <div class="page-header">
  <div class="col-xs-6">
    <h1 class="page-title">Users</h1>
    <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{{route('dashboard')}}">Home</a></li>
      <li class="active">Users</li>
    </ol>     
  </div>
  <div class="col-xs-6 text-right">
   
    
   
  </div>
</div>
<div class="clearfix"></div>
<div class="page-content" >
  <div class="panel">
    <div class="panel-body container-fluid" id="tbl-user">
      <div class="success-alert-area"> </div>
      <div class="clearfix sp-margin-sm"></div>
      <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('users.update',$user->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update User</h4>
                                        
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Name</label>
                                            <input class="form-control" type="text"  name="name" value="{{$user->name}}" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Email</label>
                                            <input class="form-control" type="email"  name="email" value="{{$user->email}}" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email-input" class="col-form-label">Password</label>
                                            <input class="form-control" type="password"  name="password" placeholder="Password">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="inputPassword" class="">Confirm Password</label>
                                            <input type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password">
                                        </div>
                                        
                                       <form action="{{ route('users.store', $user->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('users.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
    </div>
  </div>
</div>
@endsection






