
@extends('layouts.admin')

@section('content')
    
  <div class="page-header">
  <div class="col-xs-6">
    <h1 class="page-title">Setting</h1>
    <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{{route('dashboard')}}">Home</a></li>
      <li class="active">Setting</li>
    </ol>     
  </div>
  <div class="col-xs-4">
       @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <i class="fa fa-remove"></i>
                                </button>
                            </div>
                        @endif
  </div>
  <div class="col-xs-2 text-right">
   
    
   
  </div>
</div>
<div class="clearfix"></div>
<div class="page-content" >
  <div class="panel">
    <div class="panel-body container-fluid" id="tbl-user">
      <div class="success-alert-area"> </div>
      <div class="clearfix sp-margin-sm"></div>
      <form method="POST" action="{{route('settings.store')}}" enctype="multipart/form-data" >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Winning Control</h4>
                                        
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Winning </label>
                                            <input type="hidden" name="settingid" value="@if(isset($setting->id)){{$setting->id}}@endif" />
                                            <div class="form-check">
                                              <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="setting_value" value="1" @if(isset($setting->setting_value) AND $setting->setting_value == 1) {{"checked"}} @endif> On
                                              </label>
                                            </div>
                                            <div class="form-check">
                                              <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="setting_value" value="0" @if(isset($setting->setting_value) AND $setting->setting_value == 0) {{"checked"}} @endif> Off
                                              </label>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                           
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
    </div>
  </div>
</div>
@endsection


@push('scripts')
<script>
$(function() {
    var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
     this.tbl = $('#sliderTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('sliders.getData') !!}',
        columns: [
            { data: 'id', name:'id'  },
            { data: 'title', name: 'title' },
            { data: 'url', name: 'url' },
            { data: 'action', name: 'action', orderable: false, searchable: false},
            
        ],
        
    });

  
    $('#sliderTable').on('click', '.sub-delete', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var checked = confirm("Sure You want to delete it");
    if(checked){
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "DELETE",
          url  : baseUrl + '/backend/sliders/' + id,
          data : {},
          success: function(data){
            var result = $.parseJSON(data);
            if(result['status'] == 'success'){
                me.tbl.ajax.reload( null, false );
            }else{
                alert(result['message']);        
            }
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in Lucky Items view. Please contact to administrator');
          }
        });  
    }else{
        return false;
    }
        
  });
   
});
  

</script>
@endpush