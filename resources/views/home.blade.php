@extends('layouts.admin')

@section('content')







<div class="clearfix"></div>
<div class="page-content">
  <div class="row">
       <div class=" col-md-3 col-lg-3 col-xs-6 col-sm-6">
    
     <div class="widget widget-shadow" id="widgetLineareaOne">
       <div class="widget-content padding-30 bg-blue-700">
        
        <div class="counter counter-md counter-inverse">
        <div class="counter-label text-capitalize" ><h4 id="dash_one">Today Winning</h4></div>
          <div class="counter-number-group">
            <span class="counter-number" id="total_properties">{{$itemcount}}</span>
          </div>
          
          <div class="counter-label text-present" id="out_invoice_total">&nbsp;</div>
        </div>
       </div>
     </div>
   
   </div>
   <div class=" col-md-3 col-lg-3 col-xs-6 col-sm-6">
    
     <div class="widget widget-shadow" id="widgetLineareatow">
       <div class="widget-content padding-30 bg-orange-700">
        
        <div class="counter counter-md counter-inverse">
        <div class="counter-label text-capitalize" ><h4 id="dash_two">Football Left Count</h4></div>
          <div class="counter-number-group">
          
            <span class="counter-number" id="total_properties">{{$footballcount}}</span>
          </div>
          
          <div class="counter-label text-present" id="out_invoice_total">&nbsp;</div>
        </div>
       </div>
     </div>
   
   </div>
   <div class=" col-md-3 col-lg-3 col-xs-6 col-sm-6">
    
     <div class="widget widget-shadow" id="widgetLineareathree">
       <div class="widget-content padding-30 bg-red-700">
        
        <div class="counter counter-md counter-inverse">
        <div class="counter-label text-capitalize" ><h4 id="dash_three">T-Shirt count Left</h4></div>
          <div class="counter-number-group">
            <span class="counter-number" id="total_properties">{{$tshirtcount}}</span>
          </div>
          
          <div class="counter-label text-present" id="out_invoice_total">&nbsp;</div>
        </div>
       </div>
     </div>
   
   </div>
   <div class=" col-md-3 col-lg-3 col-xs-6 col-sm-6">
    
     <div class="widget widget-shadow" id="widgetLineareaOne">
       <div class="widget-content padding-30 bg-green-700">
        
        <div class="counter counter-md counter-inverse">
        <div class="counter-label text-capitalize" ><h4 id="dash_one">Total Winning</h4></div>
          <div class="counter-number-group">
            <span class="counter-number" id="total_properties">{{$total}}</span>
          </div>
          
          <div class="counter-label text-present" id="out_invoice_total">&nbsp;</div>
        </div>
       </div>
     </div>
   
   </div>
  </div>
  <
</div>

@stop

@push('scripts')


</script>
@endpush
